<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Targetsales2022;use App\Models\Targetsales2021;use App\Models\Targetsales2020;

class TargetsalesController extends Controller
{
    public function index($year){
        if ($year == 2022) {
            $data = Targetsales2022::all();
        }elseif ($year == 2021) {
            $data = Targetsales2021::all();
        }elseif ($year == 2020) {
            $data = Targetsales2020::all();
        }
        return view('sales.target', [
            'data'=>$data,
            'year'=>$year,
        ]);
    }
}
