<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Datarev;

class MainrevController extends Controller
{
    public function index(){
        $data = Datarev::select('piddmt','kategori', 'bulan', 'sitename')->get();
        return view('revenue.main', [
            'data'  => $data,
        ]);
    }
}
