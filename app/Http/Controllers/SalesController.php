<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\Targetsales2022;use App\Models\Hasilsales2022;use App\Models\Datasales2022;use App\Models\Hasilsales2021;
use App\Models\Tenant;

use Carbon\Carbon;

class SalesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::User()->jobdesc;
        $month = Carbon::now()->format('M');
        $loop = Carbon::parse('1'.$month)->month;
        $sumbef = 0;
        $sumaft = 0;
        $target = 0;
        $co = Datasales2022::where('kategori', 'Carry Over')->count();
        $ns = Datasales2022::where('kategori', 'New Sales')->count();
        $area1 = Datasales2022::where('Area', 'Area 1')->count();
        $area2 = Datasales2022::where('Area', 'Area 2')->count();
        $area3 = Datasales2022::where('Area', 'Area 3')->count();
        $area4 = Datasales2022::where('Area', 'Area 4')->count();
        $b2s = Datasales2022::where('sow', 'New Site')->count();
        $colo = Datasales2022::where('sow', 'Colo')->count();
        $rese = Datasales2022::where('sow', 'Reseller')->count();
        $tsel = Datasales2022::where('tenant', 'TSEL')->count();
        $tlkm = Datasales2022::where('tenant', 'TELKOM')->count();
        $isat = Datasales2022::where('tenant', 'ISAT')->count();
        $h3i = Datasales2022::where('tenant', 'H3I')->count();
        $smart = Datasales2022::where('tenant', 'SMART')->count();
        $xl = Datasales2022::where('tenant', 'XL')->count();
        $others = Datasales2022::where('tenant', 'Others')->count();
        
        if($user == 'Sales' || $user == 'all'){
            for ($i=1;$i<=$loop;$i++){
                $sumbef = $sumbef + Hasilsales2022::sum($i);
                $sumaft = $sumaft + Hasilsales2021::sum($i);
                $target = $target + Targetsales2022::sum(Carbon::create()->day(1)->month($i)->format('M'));
            }
        }
        // dd($tenantnames);
        return view('sales.dashboard', [
            'sumbef'    =>  $sumbef,
            'sumaft'    =>  $sumaft,
            'target'    =>  $target,
            'co'        =>  $co,
            'ns'        =>  $ns,
            'area1'     =>  $area1,
            'area2'     =>  $area2,
            'area3'     =>  $area3,
            'area4'     =>  $area4,
            'b2s'       =>  $b2s,
            'colo'      =>  $colo,
            'rese'      =>  $rese,
            'tsel'      =>  $tsel,
            'tlkm'      =>  $tlkm,
            'isat'      =>  $isat,
            'h3i'       =>  $h3i,
            'smart'     =>  $smart,
            'xl'        =>  $xl,
            'others'    =>  $others,
        ]);
    }
    public function details($segmen, $segval){
        $hasil = Datasales2022::where($segmen, $segval)->get();
        dd($sumaft);
        return view('sales.dashdet', [
            'segmen'    =>  $segmen,
            'segval'    =>  $segval,
            'hasil'     =>  $hasil,
        ]);
    }
}
