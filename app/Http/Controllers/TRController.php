<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Models\Datatr;use App\Models\Targettr2022;

class TRController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $loop = Carbon::parse(Carbon::now()->format('M'))->month;
        $loop2 = Carbon::now()->format('Y');
        $sumtotalaset = 0;
        $ytdtrnew = 0;
        $ytdtrold = 0;
        $targettr = 0;
        $netadd = 0;
        $existing = 0;
        $fy2021 = Datatr::where('tahun', '<', 2022)->whereNotNull('jumtenant')->count();
        $rural = Datatr::whereNotNull('jumtenant')->where('demografi', 'rural')->count();
        $urban = Datatr::whereNotNull('jumtenant')->where('demografi', 'urban')->count();
        $suburban = Datatr::whereNotNull('jumtenant')->where('demografi', 'sub urban')->count();
        $dismantle = Datatr::where('jumtenant', 0)->count();
        $satutenant = Datatr::where('jumtenant', 1)->count();
        $duatenant = Datatr::where('jumtenant', 2)->count();
        $tigatenant = Datatr::where('jumtenant', 3)->count();
        $empattenant = Datatr::where('jumtenant', 4)->count();
        $limatenant = Datatr::where('jumtenant', 5)->count();
        $enamtenant = Datatr::where('jumtenant', 6)->count();
        $sumtotaltenant = Datatr::count();
        for ($i=2009;$i<=$loop2;$i++){
            $asetperyear = Datatr::where('tahun', $i)->whereNotNull('jumtenant')->count();
            $sumtotalaset = $sumtotalaset + Datatr::where('tahun', $i)->whereNotNull('jumtenant')->count();
        }
        for ($i=1;$i<=$loop;$i++){
            $ytdtrnew = $ytdtrnew + Datatr::where('tahun', $loop2)->where('bulan', (Carbon::create()->day(1)->month($i)->format('M')))->whereNotNull('jumtenant')->count();
            $ytdtrold = $ytdtrold + Datatr::where('tahun', ((Carbon::create()->day(1)->year($loop2)->format('Y'))-1))->where('bulan', (Carbon::create()->day(1)->month($i)->format('M')))->whereNotNull('jumtenant')->count();
            $targettr = $targettr + Targettr2022::sum((Carbon::create()->day(1)->month($i)->format('M')));
            $netadd = $netadd + Datatr::where('netadd', 'net add')->where('tahun', $loop2)->where('bulan', (Carbon::create()->day(1)->month($i)->format('M')))->count();
            $existing = $existing + Datatr::where('netadd', 'existing')->where('tahun', $loop2)->where('bulan', (Carbon::create()->day(1)->month($i)->format('M')))->count();
        }
        // dd($fy2021);
        return view('tratio.dashboard', [
            'sumtotalaset'      =>      $sumtotalaset,
            'sumtotaltenant'    =>      $sumtotaltenant,
            'ytdtrnew'          =>      $ytdtrnew,
            'ytdtrold'          =>      $ytdtrold,
            'targettr'          =>      $targettr,
            'netadd'            =>      $netadd,
            'existing'          =>      $existing,
            'rural'             =>      $rural,
            'urban'             =>      $urban,
            'suburban'          =>      $suburban,
            'dismantle'         =>      $dismantle,
            'satutenant'        =>      $satutenant,
            'duatenant'         =>      $duatenant,
            'tigatenant'        =>      $tigatenant,
            'empattenant'       =>      $empattenant,
            'limatenant'        =>      $limatenant,
            'enamtenant'        =>      $enamtenant,
        ]);
    }
}
