<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Datarev2022;use App\Models\Datarev2021;
use App\Models\Hasilrev2022;use App\Models\Hasilrev2021;
use App\Models\Targetrev2022;

use Carbon\Carbon;

class RevController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $loop = Carbon::parse(Carbon::now()->format('M'))->month;
        $loop2 = Carbon::now()->format('Y');

        $sumtotalrevnew = 0;
        $sumtotalrevold = 0;
        $sumtargetrev   = 0;
        $ytdanchorrec = 0;
        $ytdsectenantrec = 0;
        $ytdresellerrec = 0;
        $ytdprojectrec = 0;
        $ytdanchoradd = 0;
        $ytdsectenantadd = 0;
        $ytdreselleradd = 0;
        $ytdprojectadd = 0;
        $charttg1 = [];
        $charttg2 = [];
        $charttg3 = [];
        $chartbulan = [];
        for ($i=1;$i<=$loop;$i++){
            $chartbulana = Carbon::create()->day(1)->month($i)->format('M');
            $sumtotalrevnew = $sumtotalrevnew + Hasilrev2022::sum($i)*1000000;
            $sumtotalrevold = $sumtotalrevold + Hasilrev2021::sum($i)*1000000;
            $sumtargetrev   = $sumtargetrev + Targetrev2022::sum((Carbon::create()->day(1)->month($i)->format('M')))*1000000;
            $ytdanchorrec = $ytdanchorrec + Hasilrev2022::where('portofolio1', 'recurring')->where('portofolio2', 'Anchor Tenant')->sum($i)*1000000;
            $ytdsectenantrec = $ytdsectenantrec + Hasilrev2022::where('portofolio1', 'recurring')->where('portofolio2', '2nd Tenant')->sum($i)*1000000;
            $ytdresellerrec = $ytdresellerrec + Hasilrev2022::where('portofolio1', 'recurring')->where('portofolio2', 'Reseller')->sum($i)*1000000;
            $ytdprojectrec = $ytdprojectrec + Hasilrev2022::where('portofolio1', 'recurring')->where('portofolio2', 'Project Solution')->sum($i)*1000000;
            $ytdanchoradd = $ytdanchoradd + Hasilrev2022::where('portofolio1', 'net add')->where('portofolio2', 'Anchor Tenant')->sum($i)*1000000;
            $ytdsectenantadd = $ytdsectenantadd + Hasilrev2022::where('portofolio1', 'net add')->where('portofolio2', '2nd Tenant')->sum($i)*1000000;
            $ytdreselleradd = $ytdreselleradd + Hasilrev2022::where('portofolio1', 'net add')->where('portofolio2', 'Reseller')->sum($i)*1000000;
            $ytdprojectadd = $ytdprojectadd + Hasilrev2022::where('portofolio1', 'net add')->where('portofolio2', 'Project Solution')->sum($i)*1000000;
            $charttg1a = Datarev2022::where('segmen', 'Sales 1')->sum((Carbon::create()->day(1)->month($i)->format('M')));
            $charttg2a = Datarev2022::where('segmen', 'Sales 2')->sum((Carbon::create()->day(1)->month($i)->format('M')));
            $charttg3a = Datarev2022::where('segmen', 'Sales 3')->sum((Carbon::create()->day(1)->month($i)->format('M')));
            array_push($charttg1, $charttg1a);
            array_push($charttg2, $charttg2a);
            array_push($charttg3, $charttg3a);
            array_push($chartbulan, $chartbulana);
        }
        $hasilbulanrev = json_encode($chartbulan);
        $hasilcharttg1 = json_encode($charttg1);
        $hasilcharttg2 = json_encode($charttg2);
        $hasilcharttg3 = json_encode($charttg3);
        // dd($sumtargetrev);
        return view('revenue.dashboard', [
            'sumtotalrevnew' => $sumtotalrevnew,
            'sumtotalrevold' => $sumtotalrevold,
            'sumtargetrev' => $sumtargetrev,
            'ytdanchorrec' => $ytdanchorrec,
            'ytdsectenantrec' => $ytdsectenantrec,
            'ytdresellerrec' => $ytdresellerrec,
            'ytdprojectrec' => $ytdprojectrec,
            'ytdanchoradd' => $ytdanchoradd,
            'ytdsectenantadd' => $ytdsectenantadd,
            'ytdreselleradd' => $ytdreselleradd,
            'ytdprojectadd' => $ytdprojectadd,
            'hasilcharttg1' => $hasilcharttg1,
            'hasilcharttg2' => $hasilcharttg2,
            'hasilcharttg3' => $hasilcharttg3,
            'hasilbulanrev' => $hasilbulanrev,
        ]);
    }
}
