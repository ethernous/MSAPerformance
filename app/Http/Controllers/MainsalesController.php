<?php

namespace App\Http\Controllers;

use App\Models\Datasales2022;use App\Models\Datasales2021;use App\Models\Datasales2020;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class MainsalesController extends Controller
{
    public function main($year)
    {
        if ($year == '2020'){
            $data = Datasales2020::all();
        } elseif ($year == '2021'){
            $data = Datasales2021::all();
        } elseif ($year == '2022'){
            $data = Datasales2022::all();
        } 
        return view('sales.main', [
            'data' =>$data,
            'year' =>$year,
        ]);
    }
    public function detail($year, $id){
        if ($year == '2022'){
            $detail = DB::table('datasale2022')
            ->where('datasale2022.piddmt', '=', $id)->get();
        } elseif ($year == '2021'){
            $detail = DB::table('datasale2021')
            ->where('datasale2021.piddmt', '=', $id)->get();
        } elseif ($year == '2020'){
            $detail = DB::table('datasale2020')
            ->where('datasale2020.piddmt', '=', $id)->get();
        }
        return view('sales.maindetail', ['detail' =>$detail]);
    }
}
