<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Models\Datatr;use App\Models\Targettr2022;use App\Models\Hasiltrall;use App\Models\Hasiltrnew;

class TrrepController extends Controller
{
    public function index(){
        return view('tratio.trselect');
    }
    public function cari(Request $request){
        $year = $request->year;
        $month = $request->month;
        
        if ($year == 'no' || $year < 2022){

        } elseif($year != 'no' && $month == 'no'){

        } elseif($year == '2022' && $month != 'no'){
            $loop = Carbon::parse('1'.$month)->month;
            $loop2 = Carbon::parse($year)->year;
            //var chart
            $chartaset = [];
            $charttenant = [];
            $chartlabel = []; //for year
            $labelcharts = []; //for month
            $mtdtrnewchart = [];
            $mtdtroldchart = [];
            $mtdtartr = [];
            $chartnetadd = [];
            $chartexisting = [];
            $chartcolo = [];
            $chartmacro = [];
            $charttotalaset = [];
            $charttotaltenant = [];
            //var loop
            $sumtotalaset = 0;
            $sumtotaltenant = 0;
            $ytdtrnew = 0;
            $ytdtrold = 0;
            $targettr = 0;
            $netadd = 0;
            $existing = 0;
            $colo = 0;
            $macro = 0;
            //var
            $totalaset = Datatr::count();
            $dismantle = Datatr::where('jumtenant', 0)->count();
            $satutenant = Datatr::where('jumtenant', 1)->count();
            $duatenant = Datatr::where('jumtenant', 2)->count();
            $tigatenant = Datatr::where('jumtenant', 3)->count();
            $empattenant = Datatr::where('jumtenant', 4)->count();
            $limatenant = Datatr::where('jumtenant', 5)->count();
            $enamtenant = Datatr::where('jumtenant', 6)->count();
            $a1 = Datatr::where('area', 'Area 1')->count();
            $a2 = Datatr::where('area', 'Area 2')->count();
            $a3 = Datatr::where('area', 'Area 3')->count();
            $a4 = Datatr::where('area', 'Area 4')->count();
            $rural = Datatr::whereNotNull('jumtenant')->where('demografi', 'rural')->count();
            $urban = Datatr::whereNotNull('jumtenant')->where('demografi', 'urban')->count();
            $suburban = Datatr::whereNotNull('jumtenant')->where('demografi', 'sub urban')->count();
            $mar50jt = Datatr::where('market', '0-50 jt')->count();
            $mar100jt = Datatr::where('market', '50-100 jt')->count();
            $mar150jt = Datatr::where('market', '100-150 jt')->count();
            $mar200jt = Datatr::where('market', '150-200 jt')->count();
            $mara200jt  = Datatr::where('market', '> 200 jt')->count();
            $nyfibre = Datatr::where('fiber', 'NY Fibre')->count();
            $fotelkom = Datatr::where('fiber', 'Already FO Telkom')->count();
            $fy2022 = Datatr::where('tahun', 2022)->whereNotNull('jumtenant')->count();
            //var total aset
            for ($i=2009;$i<=$loop2;$i++){
                $asetperyear = Datatr::where('tahun', $i)->whereNotNull('jumtenant')->count();
                $tenantperyear = Datatr::where('tahun', $i)->count();
                $sumtotalaset = $sumtotalaset + Datatr::where('tahun', $i)->whereNotNull('jumtenant')->count();
                $sumtotaltenant = $sumtotaltenant + Datatr::where('tahun', $i)->count();
                $labelaset = Carbon::create()->day(1)->year($i)->format('Y');
                array_push($chartaset, $asetperyear);
                array_push($chartlabel, $labelaset);
                array_push($charttenant, $tenantperyear);
                array_push($charttotalaset, $sumtotalaset);
                array_push($charttotaltenant, $sumtotaltenant);
            }
            $hasilchartaset = json_encode($chartaset);
            $hasillabelaset = json_encode($chartlabel);
            $hasilcharttenant = json_encode($charttenant);
            $hasiltotalchart = json_encode($charttotalaset);
            $hasiltotaltenant = json_encode($charttotaltenant);
            //var monthly
            for ($i=1;$i<=$loop;$i++){
                $labelchart = Carbon::create()->day(1)->month($i)->format('M');
                $ytdtrnew = $ytdtrnew + Datatr::where('tahun', $year)->where('bulan', (Carbon::create()->day(1)->month($i)->format('M')))->whereNotNull('jumtenant')->count();
                $ytdtrold = $ytdtrold + Datatr::where('tahun', ((Carbon::create()->day(1)->year($year)->format('Y'))-1))->where('bulan', (Carbon::create()->day(1)->month($i)->format('M')))->whereNotNull('jumtenant')->count();
                $targettr = $targettr + Targettr2022::sum((Carbon::create()->day(1)->month($i)->format('M')));
                $netadd = $netadd + Datatr::where('netadd', 'net add')->where('tahun', $year)->where('bulan', (Carbon::create()->day(1)->month($i)->format('M')))->count();
                $existing = $existing + Datatr::where('netadd', 'existing')->where('tahun', $year)->where('bulan', (Carbon::create()->day(1)->month($i)->format('M')))->count();
                $colo =  $colo + Datatr::where('sow', 'colo')->where('tahun', $year)->where('bulan', (Carbon::create()->day(1)->month($i)->format('M')))->count();
                $macro = $macro + Datatr::where('sow', 'macro')->where('tahun', $year)->where('bulan', (Carbon::create()->day(1)->month($i)->format('M')))->count();
                array_push($labelcharts, $labelchart);
                array_push($mtdtrnewchart, $ytdtrnew);
                array_push($mtdtroldchart, $ytdtrold);
                array_push($mtdtartr, $targettr);
                array_push($chartnetadd, $netadd);
                array_push($chartexisting, $existing);
                array_push($chartcolo, $colo);
                array_push($chartmacro, $macro);
            }
            $hasillabelchart = json_encode($labelcharts);
            $hasilmtdtrnew = json_encode($mtdtrnewchart);
            $hasilmtdtrold = json_encode($mtdtroldchart);
            $hasiltargettr = json_encode($mtdtartr);
            $hasilchartnetadd = json_encode($chartnetadd);
            $hasilchartexisting = json_encode($chartexisting);
            $hasilchartcolo = json_encode($chartcolo);
            $hasilchartmacro = json_encode($chartmacro);
            // $coba = Datatr::groupBy('siteiddmt')->selectRaw('count(*) as total, siteiddmt')->where('tahun', $year)->where('bulan', 'mar');
        } else {

        }
        // dd($sumtotalaset, $fy2022);
        return view('tratio.trresult', [
            'year' => $year,
            'month' => $month,
            'sumtotalaset' => $sumtotalaset,
            'hasilchartaset' => $hasilchartaset,
            'hasillabelaset' => $hasillabelaset,
            'hasilcharttenant' => $hasilcharttenant,
            'totalaset' => $totalaset,
            'dismantle' => $dismantle,
            'satutenant' => $satutenant,
            'duatenant' => $duatenant,
            'tigatenant' => $tigatenant,
            'empattenant' => $empattenant,
            'limatenant' => $limatenant,
            'enamtenant' => $enamtenant,
            'a1' => $a1,
            'a2' => $a2,
            'a3' => $a3,
            'a4' => $a4,
            'rural' => $rural,
            'urban' => $urban,
            'suburban' => $suburban,
            'mar50jt' => $mar50jt,
            'mar100jt' => $mar100jt, 
            'mar150jt' => $mar150jt, 
            'mar200jt' => $mar200jt, 
            'mara200jt' => $mara200jt,
            'nyfibre' => $nyfibre,
            'fotelkom' => $fotelkom,
            'hasillabelchart' => $hasillabelchart,
            'hasilmtdtrnew' => $hasilmtdtrnew,
            'hasilmtdtrold' => $hasilmtdtrold,
            'hasiltargettr' => $hasiltargettr,
            'hasilchartnetadd' => $hasilchartnetadd,
            'hasilchartexisting' => $hasilchartexisting,
            'hasilchartcolo' => $hasilchartcolo,
            'hasilchartmacro' => $hasilchartmacro,
            'hasiltotalchart' => $hasiltotalchart,
            'hasiltotaltenant' => $hasiltotaltenant,
        ]);
    }
}
