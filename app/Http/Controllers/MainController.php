<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function main()
    {
        $data = Level0::all();
        return view('main', ['data' =>$data]);
    }
    public function detail($id){
        $detail = DB::table('level0')
        ->join('level1', 'level0.piddmt', '=', 'level1.piddmt')
        ->join('level2', 'level1.piddmt', '=', 'level2.piddmt')
        ->join('level3', 'level2.piddmt', '=', 'level3.piddmt')
        ->where('level1.piddmt', '=', $id)->get();
        // $pid = $detail[0];
        // dd($pid);
        return view('detail', ['detail' =>$detail]);
    }
}
