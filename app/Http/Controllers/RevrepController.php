<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Models\Datarev2022;use App\Models\Datarev2021;use App\Models\Hasilrev2021;use App\Models\Hasilrev2022;use App\Models\Targetrev2022;

class RevrepController extends Controller
{
    public function index(){
        return view('revenue.revselect');
    }
    public function cari(Request $request){
        $year = $request->year;
        $month = $request->month;
        if ($year == 2022 && $month !='no'){
            $loop = Carbon::parse('1'.$month)->month;
            $chartbulan = [];
            $chartrev = [];
            $chartrav = [];
            $charttar = [];
            $chartrec = [];
            $chartadd = [];
            $chartanc = [];
            $chart2nd = [];
            $chartres = [];
            $chartpro = [];
            $chartsel = [];
            $charttel = [];
            $chartxl = [];
            $chartsat = [];
            $chartsf = [];
            $charth3i = [];
            $chartoth = [];
            $chartbak = [];
            $charttg1 = [];
            $charttg2 = [];
            $charttg3 = [];
            $ytdanchor = 0;
            $ytdsectenant = 0;
            $ytdreseller = 0;
            $ytdproject = 0;
            $ytdanchorrec = 0;
            $ytdsectenantrec = 0;
            $ytdresellerrec = 0;
            $ytdprojectrec = 0;
            $ytdanchoradd = 0;
            $ytdsectenantadd = 0;
            $ytdreselleradd = 0;
            $ytdprojectadd = 0;
            $ytdtaranchor = 0;
            $ytdtarsectenant = 0;
            $ytdtarreseller = 0; 
            $ytdtarproject = 0;
            $ytdachanchor = 0;
            $ytdachsectenant = 0;
            $ytdachreseller = 0;
            $ytdachproject = 0;
            $ytdachms =0;
            $ytdachdb =0;
            $ytdtelkomsel =0;
            $ytdtelkom =0;
            $ytdxl =0;
            $ytdisat =0;
            $ytdsf =0;
            $ytdh3i =0;
            $ytdbakti =0;
            $ytdothers =0;
            $ytdtelkomselanchor =0;
            $ytdtelkomanchor =0;
            $ytdxlanchor =0;
            $ytdisatanchor =0;
            $ytdsfanchor =0;
            $ytdh3ianchor =0;
            $ytdbaktianchor =0;
            $ytdothersanchor =0;
            $ytdtelkomselanchora =0;
            $ytdtelkomanchora =0;
            $ytdxlanchora =0;
            $ytdisatanchora =0;
            $ytdsfanchora =0;
            $ytdh3ianchora =0;
            $ytdbaktianchora =0;
            $ytdothersanchora =0;
            $ytdtelkomsel2nd =0;
            $ytdtelkom2nd =0;
            $ytdxl2nd =0;
            $ytdisat2nd =0;
            $ytdsf2nd =0;
            $ytdh3i2nd =0;
            $ytdbakti2nd =0;
            $ytdothers2nd =0;
            $ytdtelkomsel2nda =0;
            $ytdtelkom2nda =0;
            $ytdxl2nda =0;
            $ytdisat2nda =0;
            $ytdsf2nda =0;
            $ytdh3i2nda =0;
            $ytdbakti2nda =0;
            $ytdothers2nda =0;
            $ytdtelkomselres =0;
            $ytdtelkomres =0;
            $ytdxlres =0;
            $ytdisatres =0;
            $ytdsfres =0;
            $ytdh3ires =0;
            $ytdbaktires =0;
            $ytdothersres =0;
            $ytdtelkomselresa =0;
            $ytdtelkomresa =0;
            $ytdxlresa =0;
            $ytdisatresa =0;
            $ytdsfresa =0;
            $ytdh3iresa =0;
            $ytdbaktiresa =0;
            $ytdothersresa =0;
            $ytdtelkomselpro =0;
            $ytdtelkompro =0;
            $ytdxlpro =0;
            $ytdisatpro =0;
            $ytdsfpro =0;
            $ytdh3ipro =0;
            $ytdbaktipro =0;
            $ytdotherspro =0;
            $ytdtelkomselproa =0;
            $ytdtelkomproa =0;
            $ytdxlproa =0;
            $ytdisatproa =0;
            $ytdsfproa =0;
            $ytdh3iproa =0;
            $ytdbaktiproa =0;
            $ytdothersproa =0;
            $anchorrec = Datarev2022::where('kategori', 'recurring')->where('portofolio2', 'Anchor Tenant')->sum($month);
            $sectenantrec = Datarev2022::where('kategori', 'recurring')->where('portofolio2', '2nd Tenant')->sum($month);
            $resellerrec = Datarev2022::where('kategori', 'recurring')->where('portofolio2', 'Reseller')->sum($month);
            $projectrec = Datarev2022::where('kategori', 'recurring')->where('portofolio2', 'Project')->sum($month);
            $anchoradd = Datarev2022::where('kategori', 'net add')->where('portofolio2', 'Anchor Tenant')->sum($month);
            $sectenantadd = Datarev2022::where('kategori', 'net add')->where('portofolio2', '2nd Tenant')->sum($month);
            $reselleradd = Datarev2022::where('kategori', 'net add')->where('portofolio2', 'Reseller')->sum($month);
            $projectadd = Datarev2022::where('kategori', 'net add')->where('portofolio2', 'Project')->sum($month);
            $anchor = $anchorrec + $anchoradd;
            $sectenant = $sectenantrec + $sectenantadd;
            $reseller = $resellerrec + $reselleradd;
            $project = $projectadd + $projectrec;
            $anchorreca = Datarev2021::where('kategori', 'recurring')->where('portofolio2', 'Anchor Tenant')->sum($month);
            $sectenantreca = Datarev2021::where('kategori', 'recurring')->where('portofolio2', '2nd Tenant')->sum($month);
            $resellerreca = Datarev2021::where('kategori', 'recurring')->where('portofolio2', 'Reseller')->sum($month);
            $projectreca = Datarev2021::where('kategori', 'recurring')->where('portofolio2', 'Project')->sum($month);
            $anchoradda = Datarev2021::where('kategori', 'net add')->where('portofolio2', 'Anchor Tenant')->sum($month);
            $sectenantadda = Datarev2021::where('kategori', 'net add')->where('portofolio2', '2nd Tenant')->sum($month);
            $reselleradda = Datarev2021::where('kategori', 'net add')->where('portofolio2', 'Reseller')->sum($month);
            $projectadda = Datarev2021::where('kategori', 'net add')->where('portofolio2', 'Project')->sum($month);
            $anchora = $anchorreca + $anchoradda;
            $sectenanta = $sectenantreca + $sectenantadda;
            $resellera = $resellerreca + $reselleradda;
            $projecta = $projectadda + $projectreca;
            $taranchorrec = Targetrev2022::where('portofolio1', 'recurring')->where('portofolio2', 'Anchor Tenant')->sum($month)*1000000;
            $tarsectenantrec = Targetrev2022::where('portofolio1', 'recurring')->where('portofolio2', '2nd Tenant')->sum($month)*1000000;
            $tarresellerrec = Targetrev2022::where('portofolio1', 'recurring')->where('portofolio2', 'Reseller')->sum($month)*1000000;
            $tarprojectrec = Targetrev2022::where('portofolio1', 'recurring')->where('portofolio2', 'Project Solution')->sum($month)*1000000;
            $taranchoradd = Targetrev2022::where('portofolio1', 'net add')->where('portofolio2', 'Anchor Tenant')->sum($month)*1000000;
            $tarsectenantadd = Targetrev2022::where('portofolio1', 'net add')->where('portofolio2', '2nd Tenant')->sum($month)*1000000;
            $tarreselleradd = Targetrev2022::where('portofolio1', 'net add')->where('portofolio2', 'Reseller')->sum($month)*1000000;
            $tarprojectadd = Targetrev2022::where('portofolio1', 'net add')->where('portofolio2', 'Project Solution')->sum($month)*1000000;
            $taranchor = $taranchorrec + $taranchoradd;
            $tarsectenant = $tarsectenantrec + $tarsectenantadd;
            $tarreseller = $tarresellerrec + $tarreselleradd;
            $tarproject = $tarprojectrec  + $tarprojectadd;
            $share = $anchor + $sectenant + $reseller + $project;
            $telkomselanchor = Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'Tsel')->sum($month);
            $telkomanchor = Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'Telkom')->sum($month);
            $xlanchor = Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'XL')->sum($month);
            $isatanchor = Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'ISAT')->sum($month);
            $sfanchor = Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'Smartfren/IBS')->sum($month);
            $h3ianchor = Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'H3I')->sum($month);
            $baktianchor = Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'Bakti')->sum($month);
            $othersanchor = Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'Others')->sum($month);
            $telkomsel2nd = Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'Tsel')->sum($month);
            $telkom2nd = Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'Telkom')->sum($month);
            $xl2nd = Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'XL')->sum($month);
            $isat2nd = Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'ISAT')->sum($month);
            $sf2nd = Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'Smartfren/IBS')->sum($month);
            $h3i2nd = Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'H3I')->sum($month);
            $bakti2nd = Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'Bakti')->sum($month);
            $others2nd = Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'Others')->sum($month);
            $telkomselres = Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'Tsel')->sum($month);
            $telkomres = Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'Telkom')->sum($month);
            $xlres = Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'XL')->sum($month);
            $isatres = Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'ISAT')->sum($month);
            $sfres = Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'Smartfren/IBS')->sum($month);
            $h3ires = Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'H3I')->sum($month);
            $baktires = Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'Bakti')->sum($month);
            $othersres = Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'Others')->sum($month);
            $telkomselpro = Datarev2022::where('portofolio2', 'Project')->where('tenant', 'Tsel')->sum($month);
            $telkompro = Datarev2022::where('portofolio2', 'Project')->where('tenant', 'Telkom')->sum($month);
            $xlpro = Datarev2022::where('portofolio2', 'Project')->where('tenant', 'XL')->sum($month);
            $isatpro = Datarev2022::where('portofolio2', 'Project')->where('tenant', 'ISAT')->sum($month);
            $sfpro = Datarev2022::where('portofolio2', 'Project')->where('tenant', 'Smartfren/IBS')->sum($month);
            $h3ipro = Datarev2022::where('portofolio2', 'Project')->where('tenant', 'H3I')->sum($month);
            $baktipro = Datarev2022::where('portofolio2', 'Project')->where('tenant', 'Bakti')->sum($month);
            $otherspro = Datarev2022::where('portofolio2', 'Project')->where('tenant', 'Others')->sum($month);
            for ($i=1;$i<=$loop;$i++){
                $ytdanchorrec = $ytdanchorrec + Hasilrev2022::where('portofolio1', 'recurring')->where('portofolio2', 'Anchor Tenant')->sum($i)*1000000;
                $ytdsectenantrec = $ytdsectenantrec + Hasilrev2022::where('portofolio1', 'recurring')->where('portofolio2', '2nd Tenant')->sum($i)*1000000;
                $ytdresellerrec = $ytdresellerrec + Hasilrev2022::where('portofolio1', 'recurring')->where('portofolio2', 'Reseller')->sum($i)*1000000;
                $ytdprojectrec = $ytdprojectrec + Hasilrev2022::where('portofolio1', 'recurring')->where('portofolio2', 'Project Solution')->sum($i)*1000000;
                $ytdanchoradd = $ytdanchoradd + Hasilrev2022::where('portofolio1', 'net add')->where('portofolio2', 'Anchor Tenant')->sum($i)*1000000;
                $ytdsectenantadd = $ytdsectenantadd + Hasilrev2022::where('portofolio1', 'net add')->where('portofolio2', '2nd Tenant')->sum($i)*1000000;
                $ytdreselleradd = $ytdreselleradd + Hasilrev2022::where('portofolio1', 'net add')->where('portofolio2', 'Reseller')->sum($i)*1000000;
                $ytdprojectadd = $ytdprojectadd + Hasilrev2022::where('portofolio1', 'net add')->where('portofolio2', 'Project Solution')->sum($i)*1000000;
                $ytdtaranchor = $ytdtaranchor + Targetrev2022::where('portofolio2', 'Anchor Tenant')->sum((Carbon::create()->day(1)->month($i)->format('M')))*1000000;
                $ytdtarsectenant = $ytdtarsectenant + Targetrev2022::where('portofolio2', '2nd Tenant')->sum((Carbon::create()->day(1)->month($i)->format('M')))*1000000;
                $ytdtarreseller = $ytdtarreseller +  Targetrev2022::where('portofolio2', 'Reseller')->sum((Carbon::create()->day(1)->month($i)->format('M')))*1000000;
                $ytdtarproject = $ytdtarproject + Targetrev2022::where('portofolio2', 'Project Solution')->sum((Carbon::create()->day(1)->month($i)->format('M')))*1000000;
                $ytdachanchor = $ytdachanchor  + Hasilrev2021::where('portofolio2', 'Anchor Tenant')->sum($i)*1000000;
                $ytdachsectenant = $ytdachsectenant + Hasilrev2021::where('portofolio2', '2nd Tenant')->sum($i)*1000000;
                $ytdachreseller = $ytdachreseller +  Hasilrev2021::where('portofolio2', 'Reseller')->sum($i)*1000000;
                $ytdachproject = $ytdachproject + Hasilrev2021::where('portofolio2', 'Project Solution')->sum($i)*1000000;
                $ytdachms = $ytdachms + Hasilrev2021::where('portofolio2', 'Managed Services')->sum($i)*1000000;
                $ytdachdb = $ytdachdb + Hasilrev2021::where('portofolio2', 'Digital Business')->sum($i)*1000000;
                $ytdtelkomsel = $ytdtelkomsel + Datarev2022::where('tenant', 'TSEL')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdtelkom = $ytdtelkom + Datarev2022::where('tenant', 'Telkom')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdxl = $ytdxl + Datarev2022::where('tenant', 'XL')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdisat = $ytdisat + Datarev2022::where('tenant', 'ISAT')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdsf = $ytdsf + Datarev2022::where('tenant', 'Smartfren/IBS')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdh3i = $ytdh3i + Datarev2022::where('tenant', 'H3I')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdbakti = $ytdbakti + Datarev2022::where('tenant', 'Bakti')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdothers = $ytdothers + Datarev2022::where('tenant', 'Others')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                //Anchor Tenant 2022
                $ytdtelkomselanchor = $ytdtelkomselanchor + Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'Tsel')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdtelkomanchor = $ytdtelkomanchor + Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'Telkom')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdxlanchor = $ytdxlanchor + Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'XL')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdisatanchor = $ytdisatanchor + Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'ISAT')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdsfanchor = $ytdsfanchor + Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'Smartfren/IBS')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdh3ianchor = $ytdh3ianchor + Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'H3I')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdbaktianchor = $ytdbaktianchor + Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'Bakti')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdothersanchor = $ytdothersanchor + Datarev2022::where('portofolio2', 'Anchor Tenant')->where('tenant', 'Others')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                // Anchor Tenant 2021
                $ytdtelkomselanchora = $ytdtelkomselanchora + Datarev2021::where('portofolio2', 'Anchor')->where('tenant', 'Telkomsel')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdtelkomanchora = $ytdtelkomanchora + Datarev2021::where('portofolio2', 'Anchor')->where('tenant', 'Telkom')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdxlanchora = $ytdxlanchora + Datarev2021::where('portofolio2', 'Anchor')->where('tenant', 'XL')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdisatanchora = $ytdisatanchora + Datarev2021::where('portofolio2', 'Anchor')->where('tenant', 'ISAT')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdsfanchora = $ytdsfanchora + Datarev2021::where('portofolio2', 'Anchor')->where('tenant', 'Smart/IBS')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdh3ianchora = $ytdh3ianchora + Datarev2021::where('portofolio2', 'Anchor')->where('tenant', 'H3I')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdbaktianchora = $ytdbaktianchora + Datarev2021::where('portofolio2', 'Anchor')->where('tenant', 'Bakti')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdothersanchora = $ytdothersanchora + Datarev2021::where('portofolio2', 'Anchor')->where('tenant', 'Others')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                // 2nd Tenant 2022
                $ytdtelkomsel2nd = $ytdtelkomsel2nd + Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'Tsel')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdtelkom2nd = $ytdtelkom2nd + Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'Telkom')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdxl2nd = $ytdxl2nd + Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'XL')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdisat2nd = $ytdisat2nd + Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'ISAT')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdsf2nd = $ytdsf2nd + Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'Smartfren/IBS')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdh3i2nd = $ytdh3i2nd + Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'H3I')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdbakti2nd = $ytdbakti2nd + Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'Bakti')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdothers2nd = $ytdothers2nd + Datarev2022::where('portofolio2', '2nd Tenant')->where('tenant', 'Others')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                // 2nd Tenant 2021
                $ytdtelkomsel2nda = $ytdtelkomsel2nda + Datarev2021::where('portofolio2', '2nd Tenant')->where('tenant', 'Telkomsel')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdtelkom2nda = $ytdtelkom2nda + Datarev2021::where('portofolio2', '2nd Tenant')->where('tenant', 'Telkom')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdxl2nda = $ytdxl2nda + Datarev2021::where('portofolio2', '2nd Tenant')->where('tenant', 'XL')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdisat2nda = $ytdisat2nda + Datarev2021::where('portofolio2', '2nd Tenant')->where('tenant', 'ISAT')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdsf2nda = $ytdsf2nda + Datarev2021::where('portofolio2', '2nd Tenant')->where('tenant', 'Smart/IBS')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdh3i2nda = $ytdh3i2nda + Datarev2021::where('portofolio2', '2nd Tenant')->where('tenant', 'H3I')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdbakti2nda = $ytdbakti2nda + Datarev2021::where('portofolio2', '2nd Tenant')->where('tenant', 'Bakti')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdothers2nda = $ytdothers2nda + Datarev2021::where('portofolio2', '2nd Tenant')->where('tenant', 'Others')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                // Reseller 2022
                $ytdtelkomselres = $ytdtelkomselres + Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'Tsel')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdtelkomres = $ytdtelkomres + Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'Telkom')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdxlres = $ytdxlres + Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'XL')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdisatres = $ytdisatres + Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'ISAT')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdsfres = $ytdsfres + Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'Smartfren/IBS')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdh3ires = $ytdh3ires + Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'H3I')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdbaktires = $ytdbaktires + Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'Bakti')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdothersres = $ytdothersres + Datarev2022::where('portofolio2', 'Reseller')->where('tenant', 'Others')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                // Reseller 2021
                $ytdtelkomselresa = $ytdtelkomselresa + Datarev2021::where('portofolio2', 'Reseller')->where('tenant', 'Telkomsel')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdtelkomresa = $ytdtelkomresa + Datarev2021::where('portofolio2', 'Reseller')->where('tenant', 'Telkom')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdxlresa = $ytdxlresa + Datarev2021::where('portofolio2', 'Reseller')->where('tenant', 'XL')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdisatresa = $ytdisatresa + Datarev2021::where('portofolio2', 'Reseller')->where('tenant', 'ISAT')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdsfresa = $ytdsfresa + Datarev2021::where('portofolio2', 'Reseller')->where('tenant', 'Smart/IBS')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdh3iresa = $ytdh3iresa + Datarev2021::where('portofolio2', 'Reseller')->where('tenant', 'H3I')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdbaktiresa = $ytdbaktiresa + Datarev2021::where('portofolio2', 'Reseller')->where('tenant', 'Bakti')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdothersresa = $ytdothersresa + Datarev2021::where('portofolio2', 'Reseller')->where('tenant', 'Others')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                // Project 2022
                $ytdtelkomselpro = $ytdtelkomselpro + Datarev2022::where('portofolio2', 'Project')->where('tenant', 'Tsel')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdtelkompro = $ytdtelkompro + Datarev2022::where('portofolio2', 'Project')->where('tenant', 'Telkom')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdxlpro = $ytdxlpro + Datarev2022::where('portofolio2', 'Project')->where('tenant', 'XL')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdisatpro = $ytdisatpro + Datarev2022::where('portofolio2', 'Project')->where('tenant', 'ISAT')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdsfpro = $ytdsfpro + Datarev2022::where('portofolio2', 'Project')->where('tenant', 'Smartfren/IBS')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdh3ipro = $ytdh3ipro + Datarev2022::where('portofolio2', 'Project')->where('tenant', 'H3I')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdbaktipro = $ytdbaktipro + Datarev2022::where('portofolio2', 'Project')->where('tenant', 'Bakti')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdotherspro = $ytdotherspro + Datarev2022::where('portofolio2', 'Project')->where('tenant', 'Others')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                // Project 2021
                $ytdtelkomselproa = $ytdtelkomselproa + Datarev2021::where('portofolio2', 'Project')->where('tenant', 'Telkomsel')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdtelkomproa = $ytdtelkomproa + Datarev2021::where('portofolio2', 'Project')->where('tenant', 'Telkom')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdxlproa = $ytdxlproa + Datarev2021::where('portofolio2', 'Project')->where('tenant', 'XL')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdisatproa = $ytdisatproa + Datarev2021::where('portofolio2', 'Project')->where('tenant', 'ISAT')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdsfproa = $ytdsfproa + Datarev2021::where('portofolio2', 'Project')->where('tenant', 'Smart/IBS')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdh3iproa = $ytdh3iproa + Datarev2021::where('portofolio2', 'Project')->where('tenant', 'H3I')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdbaktiproa = $ytdbaktiproa + Datarev2021::where('portofolio2', 'Project')->where('tenant', 'Bakti')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $ytdothersproa = $ytdothersproa + Datarev2021::where('portofolio2', 'Project')->where('tenant', 'Others')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                //chart
                $chartbulana = Carbon::create()->day(1)->month($i)->format('M');
                $chartreva = Hasilrev2022::sum($i)*1000;
                $chartrava = Hasilrev2021::sum($i)*1000;
                $charttara = Targetrev2022::sum((Carbon::create()->day(1)->month($i)->format('M')))*1000;
                $chartreca = Hasilrev2022::where('portofolio1', 'recurring')->sum($i)*1000;
                $chartadda = Hasilrev2022::where('portofolio1', 'net add')->sum($i)*1000;
                $chartanca = Datarev2022::where('portofolio2', 'Anchor Tenant')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $chart2nda = Datarev2022::where('portofolio2', '2nd Tenant')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $chartresa = Datarev2022::where('portofolio2', 'Reseller')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $chartproa = Datarev2022::where('portofolio2', 'Project')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $chartsela = Datarev2022::where('tenant', 'Tsel')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $charttela = Datarev2022::where('tenant', 'Telkom')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $chartxla = Datarev2022::where('tenant', 'XL')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $chartsata = Datarev2022::where('tenant', 'Isat')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $chartsfa = Datarev2022::where('tenant', 'Smartfren/IBS')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $charth3ia = Datarev2022::where('tenant', 'H3I')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $chartotha = Datarev2022::where('tenant', 'Others')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $chartbaka = Datarev2022::where('tenant', 'Bakti')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $charttg1a = Datarev2022::where('segmen', 'Sales 1')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $charttg2a = Datarev2022::where('segmen', 'Sales 2')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                $charttg3a = Datarev2022::where('segmen', 'Sales 3')->sum((Carbon::create()->day(1)->month($i)->format('M')));
                array_push($chartbulan, $chartbulana);
                array_push($chartrev, $chartreva);
                array_push($chartrav, $chartrava);
                array_push($charttar, $charttara);
                array_push($chartrec, $chartreca);
                array_push($chartadd, $chartadda);
                array_push($chartanc, $chartanca);
                array_push($chart2nd, $chart2nda);
                array_push($chartres, $chartresa);
                array_push($chartpro, $chartproa);
                array_push($chartsel, $chartsela);
                array_push($charttel, $charttela);
                array_push($chartxl, $chartxla);
                array_push($chartsat, $chartsata);
                array_push($chartsf, $chartsfa);
                array_push($charth3i, $charth3ia);
                array_push($chartoth, $chartotha);
                array_push($chartbak, $chartbaka);
                array_push($charttg1, $charttg1a);
                array_push($charttg2, $charttg2a);
                array_push($charttg3, $charttg3a);
            }
            $ytdanchor = $ytdanchorrec + $ytdanchoradd;
            $ytdsectenant = $ytdsectenantrec + $ytdsectenantadd;
            $ytdreseller = $ytdresellerrec + $ytdreselleradd;
            $ytdproject = $ytdprojectrec + $ytdprojectadd;
            $ytdtenanttotal = $ytdtelkomsel + $ytdtelkom + $ytdxl + $ytdisat + $ytdh3i + $ytdsf + $ytdbakti + $ytdothers;
            $hasilbulanrev = json_encode($chartbulan);
            $hasilchartrev = json_encode($chartrev);
            $hasilchartrav = json_encode($chartrav);
            $hasilcharttar = json_encode($charttar);
            $hasilchartrec = json_encode($chartrec);
            $hasilchartadd = json_encode($chartadd);
            $hasilchartanc = json_encode($chartanc);
            $hasilchart2nd = json_encode($chart2nd);
            $hasilchartres = json_encode($chartres);
            $hasilchartpro = json_encode($chartpro);
            $hasilchartsel = json_encode($chartsel);
            $hasilcharttel = json_encode($charttel);
            $hasilchartxl = json_encode($chartxl);
            $hasilchartsat = json_encode($chartsat);
            $hasilchartsf = json_encode($chartsf);
            $hasilcharth3i = json_encode($charth3i);
            $hasilchartoth = json_encode($chartoth);
            $hasilchartbak = json_encode($chartbak);
            $hasilcharttg1 = json_encode($charttg1);
            $hasilcharttg2 = json_encode($charttg2);
            $hasilcharttg3 = json_encode($charttg3);
            // $achmtdanchor = $anchor;
            // dd($hasilbulanrev);
           
        }
        return view('revenue.revreport', [
            'year' => $year,
            'month' => $month,
            'ytdanchor' => $ytdanchor,
            'ytdsectenant' => $ytdsectenant,
            'ytdreseller' => $ytdreseller,
            'ytdproject' => $ytdproject,
            'ytdanchorrec' => $ytdanchorrec,
            'ytdsectenantrec' => $ytdsectenantrec,
            'ytdresellerrec' => $ytdresellerrec,
            'ytdprojectrec' => $ytdprojectrec,
            'ytdanchoradd' => $ytdanchoradd,
            'ytdsectenantadd' => $ytdsectenantadd,
            'ytdreselleradd' => $ytdreselleradd,
            'ytdprojectadd' => $ytdprojectadd,
            'ytdtaranchor' => $ytdtaranchor,
            'ytdtarsectenant' => $ytdtarsectenant,
            'ytdtarreseller' => $ytdtarreseller, 
            'ytdtarproject' => $ytdtarproject,
            'ytdachanchor' => $ytdachanchor,
            'ytdachsectenant' => $ytdachsectenant,
            'ytdachreseller' => $ytdachreseller,
            'ytdachproject' => $ytdachproject,
            'ytdachms' => $ytdachms,
            'ytdachdb' => $ytdachdb,
            'anchorrec' => $anchorrec,
            'sectenantrec' => $sectenantrec,
            'resellerrec' => $resellerrec,
            'projectrec' => $projectrec,
            'anchoradd' => $anchoradd,
            'sectenantadd' => $sectenantadd,
            'reselleradd' => $reselleradd,
            'projectadd' => $projectadd,
            'anchor' => $anchor,
            'sectenant' => $sectenant,
            'reseller' => $reseller,
            'project' => $project,
            'taranchorrec' => $taranchorrec,
            'tarsectenantrec' => $tarsectenantrec,
            'tarresellerrec' => $tarresellerrec,
            'tarprojectrec' => $tarprojectrec,
            'taranchoradd' => $taranchoradd,
            'tarsectenantadd' => $tarsectenantadd,
            'tarreselleradd' => $tarreselleradd,
            'tarprojectadd' => $tarprojectadd,
            'taranchor' => $taranchor,
            'tarsectenant' => $tarsectenant,
            'tarreseller' => $tarreseller,
            'tarproject' => $tarproject,
            'share' => $share,
            'ytdtelkomsel' => $ytdtelkomsel,
            'ytdtelkom' => $ytdtelkom,
            'ytdxl' => $ytdxl,
            'ytdisat' => $ytdisat,
            'ytdsf' => $ytdsf,
            'ytdh3i' => $ytdh3i,
            'ytdbakti' => $ytdbakti,
            'ytdothers' => $ytdothers,
            'ytdtenanttotal' => $ytdtenanttotal,
            'telkomselanchor' => $telkomselanchor,
            'telkomanchor' => $telkomanchor,
            'xlanchor' => $xlanchor,
            'isatanchor' => $isatanchor,
            'sfanchor' => $sfanchor,
            'h3ianchor' => $h3ianchor,
            'baktianchor' => $baktianchor,
            'othersanchor' => $othersanchor,
            'ytdtelkomselanchor' => $ytdtelkomselanchor,
            'ytdtelkomanchor' => $ytdtelkomanchor,
            'ytdxlanchor' => $ytdxlanchor,
            'ytdisatanchor' => $ytdisatanchor,
            'ytdsfanchor' => $ytdsfanchor,
            'ytdh3ianchor' => $ytdh3ianchor,
            'ytdbaktianchor' => $ytdbaktianchor,
            'ytdothersanchor' => $ytdothersanchor,
            'ytdtelkomselanchora' => $ytdtelkomselanchora,
            'ytdtelkomanchora' => $ytdtelkomanchora,
            'ytdxlanchora' => $ytdxlanchora,
            'ytdisatanchora' => $ytdisatanchora,
            'ytdsfanchora' => $ytdsfanchora,
            'ytdh3ianchora' => $ytdh3ianchora,
            'ytdbaktianchora' => $ytdbaktianchora,
            'ytdothersanchora' => $ytdothersanchora,
            'telkomsel2nd' => $telkomsel2nd,
            'telkom2nd' => $telkom2nd,
            'xl2nd' => $xl2nd,
            'isat2nd' => $isat2nd,
            'sf2nd' => $sf2nd,
            'h3i2nd' => $h3i2nd,
            'bakti2nd' => $bakti2nd,
            'others2nd' => $others2nd,
            'ytdtelkomsel2nd' => $ytdtelkomsel2nd,
            'ytdtelkom2nd' => $ytdtelkom2nd,
            'ytdxl2nd' => $ytdxl2nd,
            'ytdisat2nd' => $ytdisat2nd,
            'ytdsf2nd' => $ytdsf2nd,
            'ytdh3i2nd' => $ytdh3i2nd,
            'ytdbakti2nd' => $ytdbakti2nd,
            'ytdothers2nd' => $ytdothers2nd,
            'ytdtelkomsel2nda' => $ytdtelkomsel2nda,
            'ytdtelkom2nda' => $ytdtelkom2nda,
            'ytdxl2nda' => $ytdxl2nda,
            'ytdisat2nda' => $ytdisat2nda,
            'ytdsf2nda' => $ytdsf2nda,
            'ytdh3i2nda' => $ytdh3i2nda,
            'ytdbakti2nda' => $ytdbakti2nda,
            'ytdothers2nda' => $ytdothers2nda,
            'telkomselres' => $telkomselres,
            'telkomres' => $telkomres,
            'xlres' => $xlres,
            'isatres' => $isatres,
            'sfres' => $sfres,
            'h3ires' => $h3ires,
            'baktires' => $baktires,
            'othersres' => $othersres,
            'ytdtelkomselres' => $ytdtelkomselres,
            'ytdtelkomres' => $ytdtelkomres,
            'ytdxlres' => $ytdxlres,
            'ytdisatres' => $ytdisatres,
            'ytdsfres' => $ytdsfres,
            'ytdh3ires' => $ytdh3ires,
            'ytdbaktires' => $ytdbaktires,
            'ytdothersres' => $ytdothersres,
            'ytdtelkomselresa' => $ytdtelkomselresa,
            'ytdtelkomresa' => $ytdtelkomresa,
            'ytdxlresa' => $ytdxlresa,
            'ytdisatresa' => $ytdisatresa,
            'ytdsfresa' => $ytdsfresa,
            'ytdh3iresa' => $ytdh3iresa,
            'ytdbaktiresa' => $ytdbaktiresa,
            'ytdothersresa' => $ytdothersresa,
            'telkomselpro' => $telkomselpro,
            'telkompro' => $telkompro,
            'xlpro' => $xlpro,
            'isatpro' => $isatpro,
            'sfpro' => $sfpro,
            'h3ipro' => $h3ipro,
            'baktipro' => $baktipro,
            'otherspro' => $otherspro,
            'ytdtelkomselpro' => $ytdtelkomselpro,
            'ytdtelkompro' => $ytdtelkompro,
            'ytdxlpro' => $ytdxlpro,
            'ytdisatpro' => $ytdisatpro,
            'ytdsfpro' => $ytdsfpro,
            'ytdh3ipro' => $ytdh3ipro,
            'ytdbaktipro' => $ytdbaktipro,
            'ytdotherspro' => $ytdotherspro,
            'ytdtelkomselproa' => $ytdtelkomselproa,
            'ytdtelkomproa' => $ytdtelkomproa,
            'ytdxlproa' => $ytdxlproa,
            'ytdisatproa' => $ytdisatproa,
            'ytdsfproa' => $ytdsfproa,
            'ytdh3iproa' => $ytdh3iproa,
            'ytdbaktiproa' => $ytdbaktiproa,
            'ytdothersproa' => $ytdothersproa,
            'hasilbulanrev' => $hasilbulanrev,
            'hasilchartrev' => $hasilchartrev,
            'hasilchartrav' => $hasilchartrav,
            'hasilcharttar' => $hasilcharttar,
            'hasilchartrec' => $hasilchartrec,
            'hasilchartadd' => $hasilchartadd,
            'hasilchartanc' => $hasilchartanc,
            'hasilchart2nd' => $hasilchart2nd,
            'hasilchartres' => $hasilchartres,
            'hasilchartpro' => $hasilchartpro,
            'hasilchartsel' => $hasilchartsel,
            'hasilcharttel' => $hasilcharttel,
            'hasilchartxl' => $hasilchartxl,
            'hasilchartsat' => $hasilchartsat,
            'hasilchartsf' => $hasilchartsf,
            'hasilcharth3i' => $hasilcharth3i,
            'hasilchartoth' => $hasilchartoth,
            'hasilchartbak' => $hasilchartbak,
            'hasilcharttg1' => $hasilcharttg1,
            'hasilcharttg2' => $hasilcharttg2,
            'hasilcharttg3' => $hasilcharttg3,
        ]);
    }
}
