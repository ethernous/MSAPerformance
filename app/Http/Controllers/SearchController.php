<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Datasales2020;
use App\Models\Datasales2021;
use App\Models\Datasales2022;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $dbase = $request->dbase;
        $year = $request->year;
        $cari = $request->cari;
        if($dbase == 'Sales' && $year == '2020'){
            $hasil = Datasales2020::where('piddmt', 'like', "%" . $cari . "%")->paginate();
        } elseif($dbase == 'Sales' && $year == '2021') {
            $hasil = Datasales2021::where('piddmt', 'like', "%" . $cari . "%")->paginate();
        } elseif($dbase == 'Sales' && $year == '2022'){
            $hasil = Datasales2022::where('piddmt', 'like', "%" . $cari . "%")->paginate();
        }
        // dd(request()->all());
        return view('search', [
            'dbase' => $dbase,
            'year' => $year,
            'cari' => $cari,
            'hasil' => $hasil,
        ]);
    }
}
