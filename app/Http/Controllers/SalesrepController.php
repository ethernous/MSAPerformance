<?php

namespace App\Http\Controllers;

use App\Models\Hasilsales2021;use App\Models\Hasilsales2020;use App\Models\Hasilsales2019;

use App\Models\Targetsales2022;use App\Models\Targetsales2021;use App\Models\Targetsales2020;

use App\Models\Datasales2022;use App\Models\Datasales2021;use App\Models\Datasales2020;

use Carbon\Carbon;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class SalesrepController extends Controller
{
    public function index(){
        return view('sales.salesselect');
    }
    public function cari(Request $request){
        $tahun = $request->year;
        $bulan = $request->month;
        $pekan = $request->week;
        $pekansales = $pekan.' '. $bulan;
        if ($tahun == 'no' && $bulan =='no' && $pekan =='no'){
            return view('sales.salesreport', [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'pekan' => $pekan,
            ]);
        }elseif($tahun != 'no' && $bulan =='no' && $pekan =='no'){
            $halaman = 0;
            return view('sales.salesreport', [
                'halaman' => $halaman,
            ]);
        }elseif($tahun == 2022 && $bulan !='no' && $pekan =='no'){
            $halaman = 1;
            $cek = Datasales2022::where('bulan', $bulan)->get();
            $hasilcek = $cek->isEmpty();
            $loop = Carbon::parse('1'.$bulan)->month;
            //Variabel Array
            $labelbln = [];
            $totsale = [];
            $tarsale = [];
            $reasale = [];
            $conow = [];
            $cobef = [];
            $nsnow = [];
            $nsbef = [];
            $b2snow = [];
            $b2sbef = [];
            $colonow = [];
            $colobef = [];
            $resenow = [];
            $resebef = [];
            $a1now = [];
            $a1bef = [];
            $a2now = [];
            $a2bef = [];
            $a3now = [];
            $a3bef = [];
            $a4now = [];
            $a4bef = [];
            //Variabel Loop
            $sumb2s = 0;
            $sumcolo = 0;
            $sumrese = 0;
            $cumtarb2s = 0;
            $cumtarcolo = 0;
            $cumtarres = 0;
            $cumcob2s=0;
            $cumcocolo=0;
            $cumcores=0;
            $cumnsb2s=0;
            $cumnscolo=0;
            $cumnsres=0;
            //Variabel Chart Donat
            $telkomsel=0;
            $telkom=0;
            $isat=0;
            $xl=0;
            $h3i=0;
            $sf=0;
            $oth=0;
            $telkomsela=0;
            $telkoma=0;
            $isata=0;
            $xla=0;
            $h3ia=0;
            $sfa=0;
            $otha = 0;
            //Loop YoY Kumulatif
            for ($i=1;$i<=$loop;$i++){
                $sumb2s = $sumb2s + Hasilsales2021::where('sow', 'Macro')->sum($i) + Hasilsales2021::where('sow', 'Micro')->sum($i);
                $sumcolo = $sumcolo + Hasilsales2021::where('sow', 'colo')->sum($i);
                $sumrese = $sumrese + Hasilsales2021::where('sow', 'reseller')->sum($i);
                $cumtarb2s = $cumtarb2s + Targetsales2022::where('sow', 'B2S')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarcolo = $cumtarcolo + Targetsales2022::where('sow', 'Colo')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarres = $cumtarres + Targetsales2022::where('sow', 'Reseller')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $totsalepro = Datasales2022::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $tarsalepro = Targetsales2022::select(Carbon::create()->day(1)->month($i)->format('M'))->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $reasalepro = Datasales2021::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $conowpro = Datasales2022::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cobefpro = Datasales2021::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsnowpro = Datasales2022::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsbefpro = Datasales2021::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $b2snowpro = Datasales2022::where('sow', 'New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $b2sbefpro = Datasales2021::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count() + Datasales2021::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colonowpro = Datasales2022::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colobefpro = Datasales2021::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resenowpro = Datasales2022::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resebefpro = Datasales2021::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1nowpro = Datasales2022::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1befpro = Datasales2021::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2nowpro = Datasales2022::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2befpro = Datasales2021::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3nowpro = Datasales2022::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3befpro = Datasales2021::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4nowpro = Datasales2022::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4befpro = Datasales2021::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $bulansales = Carbon::create()->day(1)->month($i)->format('M');
                $cumcob2s = $cumcob2s + Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcocolo = $cumcocolo + Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcores = $cumcores + Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsb2s = $cumnsb2s + Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnscolo = $cumnscolo + Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsres = $cumnsres + Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsel = $telkomsel + Datasales2022::where('tenant', 'TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkom = $telkom + Datasales2022::where('tenant', 'TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isat = $isat + Datasales2022::where('tenant', 'ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xl = $xl + Datasales2022::where('tenant', 'XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3i = $h3i + Datasales2022::where('tenant', 'H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sf = $sf + Datasales2022::where('tenant', 'SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $oth = $oth + Datasales2022::where('tenant', 'Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsela = $telkomsela + Datasales2022::where('kategori', 'New Sales')->where('tenant','TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkoma = $telkoma + Datasales2022::where('kategori', 'New Sales')->where('tenant','TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isata = $isata + Datasales2022::where('kategori', 'New Sales')->where('tenant','ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xla = $xla + Datasales2022::where('kategori', 'New Sales')->where('tenant','XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3ia = $h3ia + Datasales2022::where('kategori', 'New Sales')->where('tenant','H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sfa = $sfa + Datasales2022::where('kategori', 'New Sales')->where('tenant','SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $otha = $otha + Datasales2022::where('kategori', 'New Sales')->where('tenant','Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                array_push($labelbln, $bulansales);
                array_push($totsale, $totsalepro);
                array_push($tarsale, $tarsalepro);
                array_push($reasale, $reasalepro);
                array_push($conow, $conowpro);
                array_push($cobef, $cobefpro);
                array_push($nsnow, $nsnowpro);
                array_push($nsbef, $nsbefpro);
                array_push($b2snow,$b2snowpro);
                array_push($b2sbef,$b2sbefpro);
                array_push($colonow,$colonowpro);
                array_push($colobef,$colobefpro);
                array_push($resenow,$resenowpro);
                array_push($resebef,$resebefpro);
                array_push($a1now,$a1nowpro);
                array_push($a1bef,$a1befpro);
                array_push($a2now,$a2nowpro);
                array_push($a2bef,$a2befpro);
                array_push($a3now,$a3nowpro);
                array_push($a3bef,$a3befpro);
                array_push($a4now,$a4nowpro);
                array_push($a4bef,$a4befpro);
            }
            $labelbulan = json_encode($labelbln);
            $hasiltotsale = json_encode($totsale);
            $hasiltarsale = json_encode($tarsale);
            $hasilreasale = json_encode($reasale);
            $hasilconow = json_encode($conow);
            $hasilcobef = json_encode($cobef);
            $hasilnsnow = json_encode($nsnow);
            $hasilnsbef = json_encode($nsbef);
            $hasilb2snow = json_encode($b2snow);
            $hasilb2sbef = json_encode($b2sbef);
            $hasilcolonow = json_encode($colonow);
            $hasilcolobef = json_encode($colobef);
            $hasilresenow = json_encode($resenow);
            $hasilresebef = json_encode($resebef);
            $hasila1now = json_encode($a1now);
            $hasila1bef = json_encode($a1bef);
            $hasila2now = json_encode($a2now);
            $hasila2bef = json_encode($a2bef);
            $hasila3now = json_encode($a3now);
            $hasila3bef = json_encode($a3bef);
            $hasila4now = json_encode($a4now);
            $hasila4bef = json_encode($a4bef);
            $tarb2s = Targetsales2022::select($bulan)->where('sow', 'B2S')->sum($bulan);
            $tarcolo = Targetsales2022::select($bulan)->where('sow', 'Colo')->sum($bulan);
            $tarres = Targetsales2022::select($bulan)->where('sow', 'Reseller')->sum($bulan);
            $b2sco = Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('bulan', $bulan)->count();
            $coloco = Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', $bulan)->count();
            $reseco = Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', $bulan)->count();
            $b2sns = Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('bulan', $bulan)->count();
            $colons = Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', $bulan)->count();
            $resens = Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', $bulan)->count();
            $b2stotal = $b2sco + $b2sns;
            $colototal = $coloco + $colons;
            $resetotal = $reseco + $resens;
            $totalchart = $telkomsel + $telkom + $isat + $xl + $h3i + $sf + $oth;
            $totalcharta = $telkomsela + $telkoma + $isata + $xla + $h3ia + $sfa + $otha;
            // dd($tarb2s, $bulan, $loop);
            return view('sales.salesreport', [
                'halaman' => $halaman,
                'labelbulan' => $labelbulan,
                'tahun' => $tahun,
                'bulan' => $bulan,
                'pekan' => $pekan,
                'b2sco' => $b2sco,
                'macroco' => $b2sco,
                'microco' => $b2sco,
                'coloco' => $coloco,
                'reseco' => $reseco,
                'b2sns' => $b2sns,
                'colons' => $colons,
                'resens' => $resens,
                'b2stotal' => $b2stotal,
                'colototal' => $colototal,
                'resetotal' => $resetotal,
                'tarb2s' => $tarb2s,
                'tarcolo' => $tarcolo,
                'tarres' => $tarres,
                'sumb2s' => $sumb2s,
                'sumcolo' => $sumcolo,
                'sumrese' => $sumrese,
                'cumtarb2s' => $cumtarb2s,
                'cumtarcolo' => $cumtarcolo,
                'cumtarres' => $cumtarres,
                'cumcob2s'  => $cumcob2s,
                'cumcocolo'  => $cumcocolo,
                'cumcores'  => $cumcores,
                'cumnsb2s'  => $cumnsb2s,
                'cumnscolo'  => $cumnscolo,
                'cumnsres'  => $cumnsres,
                'telkomsel' => $telkomsel,
                'telkom' => $telkom,
                'isat' => $isat,
                'xl' => $xl,
                'h3i' => $h3i,
                'sf' => $sf,
                'oth' => $oth,
                'totalchart' => $totalchart,
                'telkomsela' => $telkomsela,
                'telkoma' => $telkoma,
                'isata' => $isata,
                'xla' => $xla,
                'h3ia' => $h3ia,
                'sfa' => $sfa,
                'otha' => $otha,
                'totalcharta' => $totalcharta,
                'hasilcek' => $hasilcek,
                'totsalepro' => $totsalepro,
                'tarsalepro' => $tarsalepro,
                'reasalepro' => $reasalepro,
                'conowpro' => $conowpro,
                'cobefpro' => $cobefpro,
                'nsnowpro' => $nsnowpro,
                'nsbefpro' => $nsbefpro,
                'b2snowpro' => $b2snowpro,
                'b2sbefpro' => $b2sbefpro,
                'colonowpro' => $colonowpro,
                'colobefpro' => $colobefpro,
                'resenowpro' => $resenowpro,
                'resebefpro' => $resebefpro,
                'a1nowpro' => $a1nowpro,
                'a1befpro' => $a1befpro,
                'a2nowpro' => $a2nowpro,
                'a2befpro' => $a2befpro,
                'a3nowpro' => $a3nowpro,
                'a3befpro' => $a3befpro,
                'a4nowpro' => $a4nowpro,
                'a4befpro' => $a4befpro,
                'hasiltotsale' => $hasiltotsale,
                'hasiltarsale' => $hasiltarsale,
                'hasilreasale' => $hasilreasale,
                'hasilconow' => $hasilconow,
                'hasilcobef' => $hasilcobef,
                'hasilnsnow' => $hasilnsnow,
                'hasilnsbef' => $hasilnsbef,
                'hasilb2snow' => $hasilb2snow,
                'hasilb2sbef' => $hasilb2sbef,
                'hasilcolonow' => $hasilcolonow,
                'hasilcolobef' => $hasilcolobef,
                'hasilresenow' => $hasilresenow,
                'hasilresebef' => $hasilresebef,
                'hasila1now' => $hasila1now,
                'hasila1bef' => $hasila1bef,
                'hasila2now' => $hasila2now,
                'hasila2bef' => $hasila2bef,
                'hasila3now' => $hasila3now,
                'hasila3bef' => $hasila3bef,
                'hasila4now' => $hasila4now,
                'hasila4bef' => $hasila4bef,
            ]);
        }elseif($tahun == 2022 && $bulan !='no' && $pekan =='W1'){
            $halaman = 1;
            $cek = Datasales2022::where('bulan', $bulan)->get();
            $hasilcek = $cek->isEmpty();
            $loop = Carbon::parse('1'.$bulan)->month;
            //Variabel Array
            $labelbln = [];
            $totsale = [];
            $tarsale = [];
            $reasale = [];
            $conow = [];
            $cobef = [];
            $nsnow = [];
            $nsbef = [];
            $b2snow = [];
            $b2sbef = [];
            $colonow = [];
            $colobef = [];
            $resenow = [];
            $resebef = [];
            $a1now = [];
            $a1bef = [];
            $a2now = [];
            $a2bef = [];
            $a3now = [];
            $a3bef = [];
            $a4now = [];
            $a4bef = [];
            //Variabel Loop
            $sumb2s = 0;
            $sumcolo = 0;
            $sumrese = 0;
            $cumtarb2s = 0;
            $cumtarcolo = 0;
            $cumtarres = 0;
            $cumcob2s=0;
            $cumcocolo=0;
            $cumcores=0;
            $cumnsb2s=0;
            $cumnscolo=0;
            $cumnsres=0;
            //Variabel Chart Donat
            $telkomsel=0;
            $telkom=0;
            $isat=0;
            $xl=0;
            $h3i=0;
            $sf=0;
            $oth=0;
            $telkomsela=0;
            $telkoma=0;
            $isata=0;
            $xla=0;
            $h3ia=0;
            $sfa=0;
            $otha = 0;
            //Loop YoY Kumulatif
            for ($i=1;$i<=$loop;$i++){
                $sumb2s = $sumb2s + Hasilsales2021::where('sow', 'Macro')->sum($i) + Hasilsales2021::where('sow', 'Micro')->sum($i);
                $sumcolo = $sumcolo + Hasilsales2021::where('sow', 'colo')->sum($i);
                $sumrese = $sumrese + Hasilsales2021::where('sow', 'reseller')->sum($i);
                $cumtarb2s = $cumtarb2s + Targetsales2022::where('sow', 'B2S')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarcolo = $cumtarcolo + Targetsales2022::where('sow', 'Colo')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarres = $cumtarres + Targetsales2022::where('sow', 'Reseller')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $totsalepro = Datasales2022::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $tarsalepro = Targetsales2022::select(Carbon::create()->day(1)->month($i)->format('M'))->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $reasalepro = Datasales2021::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $conowpro = Datasales2022::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cobefpro = Datasales2021::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsnowpro = Datasales2022::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsbefpro = Datasales2021::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $b2snowpro = Datasales2022::where('sow', 'New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $b2sbefpro = Datasales2021::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count() + Datasales2021::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colonowpro = Datasales2022::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colobefpro = Datasales2021::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resenowpro = Datasales2022::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resebefpro = Datasales2021::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1nowpro = Datasales2022::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1befpro = Datasales2021::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2nowpro = Datasales2022::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2befpro = Datasales2021::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3nowpro = Datasales2022::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3befpro = Datasales2021::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4nowpro = Datasales2022::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4befpro = Datasales2021::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $bulansales = Carbon::create()->day(1)->month($i)->format('M');
                $cumcob2s = $cumcob2s + Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcocolo = $cumcocolo + Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcores = $cumcores + Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsb2s = $cumnsb2s + Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnscolo = $cumnscolo + Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsres = $cumnsres + Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsel = $telkomsel + Datasales2022::where('tenant', 'TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkom = $telkom + Datasales2022::where('tenant', 'TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isat = $isat + Datasales2022::where('tenant', 'ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xl = $xl + Datasales2022::where('tenant', 'XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3i = $h3i + Datasales2022::where('tenant', 'H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sf = $sf + Datasales2022::where('tenant', 'SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $oth = $oth + Datasales2022::where('tenant', 'Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsela = $telkomsela + Datasales2022::where('kategori', 'New Sales')->where('tenant','TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkoma = $telkoma + Datasales2022::where('kategori', 'New Sales')->where('tenant','TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isata = $isata + Datasales2022::where('kategori', 'New Sales')->where('tenant','ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xla = $xla + Datasales2022::where('kategori', 'New Sales')->where('tenant','XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3ia = $h3ia + Datasales2022::where('kategori', 'New Sales')->where('tenant','H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sfa = $sfa + Datasales2022::where('kategori', 'New Sales')->where('tenant','SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $otha = $otha + Datasales2022::where('kategori', 'New Sales')->where('tenant','Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                array_push($labelbln, $bulansales);
                array_push($totsale, $totsalepro);
                array_push($tarsale, $tarsalepro);
                array_push($reasale, $reasalepro);
                array_push($conow, $conowpro);
                array_push($cobef, $cobefpro);
                array_push($nsnow, $nsnowpro);
                array_push($nsbef, $nsbefpro);
                array_push($b2snow,$b2snowpro);
                array_push($b2sbef,$b2sbefpro);
                array_push($colonow,$colonowpro);
                array_push($colobef,$colobefpro);
                array_push($resenow,$resenowpro);
                array_push($resebef,$resebefpro);
                array_push($a1now,$a1nowpro);
                array_push($a1bef,$a1befpro);
                array_push($a2now,$a2nowpro);
                array_push($a2bef,$a2befpro);
                array_push($a3now,$a3nowpro);
                array_push($a3bef,$a3befpro);
                array_push($a4now,$a4nowpro);
                array_push($a4bef,$a4befpro);
            }
            $labelbulan = json_encode($labelbln);
            $hasiltotsale = json_encode($totsale);
            $hasiltarsale = json_encode($tarsale);
            $hasilreasale = json_encode($reasale);
            $hasilconow = json_encode($conow);
            $hasilcobef = json_encode($cobef);
            $hasilnsnow = json_encode($nsnow);
            $hasilnsbef = json_encode($nsbef);
            $hasilb2snow = json_encode($b2snow);
            $hasilb2sbef = json_encode($b2sbef);
            $hasilcolonow = json_encode($colonow);
            $hasilcolobef = json_encode($colobef);
            $hasilresenow = json_encode($resenow);
            $hasilresebef = json_encode($resebef);
            $hasila1now = json_encode($a1now);
            $hasila1bef = json_encode($a1bef);
            $hasila2now = json_encode($a2now);
            $hasila2bef = json_encode($a2bef);
            $hasila3now = json_encode($a3now);
            $hasila3bef = json_encode($a3bef);
            $hasila4now = json_encode($a4now);
            $hasila4bef = json_encode($a4bef);
            $tarb2s = Targetsales2022::select($bulan)->where('sow', 'B2S')->sum($bulan);
            $tarcolo = Targetsales2022::select($bulan)->where('sow', 'Colo')->sum($bulan);
            $tarres = Targetsales2022::select($bulan)->where('sow', 'Reseller')->sum($bulan);
            $b2sco = Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $coloco = Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $reseco = Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $b2sns = Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $colons = Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $resens = Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $b2stotal = $b2sco + $b2sns;
            $colototal = $coloco + $colons;
            $resetotal = $reseco + $resens;
            $totalchart = $telkomsel + $telkom + $isat + $xl + $h3i + $sf + $oth;
            $totalcharta = $telkomsela + $telkoma + $isata + $xla + $h3ia + $sfa + $otha;
            // dd($sumb2s, $sumcolo);
            return view('sales.salesreport', [
                'halaman' => $halaman,
                'labelbulan' => $labelbulan,
                'tahun' => $tahun,
                'bulan' => $bulan,
                'pekan' => $pekan,
                'b2sco' => $b2sco,
                'macroco' => $b2sco,
                'microco' => $b2sco,
                'coloco' => $coloco,
                'reseco' => $reseco,
                'b2sns' => $b2sns,
                'colons' => $colons,
                'resens' => $resens,
                'b2stotal' => $b2stotal,
                'colototal' => $colototal,
                'resetotal' => $resetotal,
                'tarb2s' => $tarb2s,
                'tarcolo' => $tarcolo,
                'tarres' => $tarres,
                'sumb2s' => $sumb2s,
                'sumcolo' => $sumcolo,
                'sumrese' => $sumrese,
                'cumtarb2s' => $cumtarb2s,
                'cumtarcolo' => $cumtarcolo,
                'cumtarres' => $cumtarres,
                'cumcob2s'  => $cumcob2s,
                'cumcocolo'  => $cumcocolo,
                'cumcores'  => $cumcores,
                'cumnsb2s'  => $cumnsb2s,
                'cumnscolo'  => $cumnscolo,
                'cumnsres'  => $cumnsres,
                'telkomsel' => $telkomsel,
                'telkom' => $telkom,
                'isat' => $isat,
                'xl' => $xl,
                'h3i' => $h3i,
                'sf' => $sf,
                'oth' => $oth,
                'totalchart' => $totalchart,
                'telkomsela' => $telkomsela,
                'telkoma' => $telkoma,
                'isata' => $isata,
                'xla' => $xla,
                'h3ia' => $h3ia,
                'sfa' => $sfa,
                'otha' => $otha,
                'totalcharta' => $totalcharta,
                'hasilcek' => $hasilcek,
                'totsalepro' => $totsalepro,
                'tarsalepro' => $tarsalepro,
                'reasalepro' => $reasalepro,
                'conowpro' => $conowpro,
                'cobefpro' => $cobefpro,
                'nsnowpro' => $nsnowpro,
                'nsbefpro' => $nsbefpro,
                'b2snowpro' => $b2snowpro,
                'b2sbefpro' => $b2sbefpro,
                'colonowpro' => $colonowpro,
                'colobefpro' => $colobefpro,
                'resenowpro' => $resenowpro,
                'resebefpro' => $resebefpro,
                'a1nowpro' => $a1nowpro,
                'a1befpro' => $a1befpro,
                'a2nowpro' => $a2nowpro,
                'a2befpro' => $a2befpro,
                'a3nowpro' => $a3nowpro,
                'a3befpro' => $a3befpro,
                'a4nowpro' => $a4nowpro,
                'a4befpro' => $a4befpro,
                'hasiltotsale' => $hasiltotsale,
                'hasiltarsale' => $hasiltarsale,
                'hasilreasale' => $hasilreasale,
                'hasilconow' => $hasilconow,
                'hasilcobef' => $hasilcobef,
                'hasilnsnow' => $hasilnsnow,
                'hasilnsbef' => $hasilnsbef,
                'hasilb2snow' => $hasilb2snow,
                'hasilb2sbef' => $hasilb2sbef,
                'hasilcolonow' => $hasilcolonow,
                'hasilcolobef' => $hasilcolobef,
                'hasilresenow' => $hasilresenow,
                'hasilresebef' => $hasilresebef,
                'hasila1now' => $hasila1now,
                'hasila1bef' => $hasila1bef,
                'hasila2now' => $hasila2now,
                'hasila2bef' => $hasila2bef,
                'hasila3now' => $hasila3now,
                'hasila3bef' => $hasila3bef,
                'hasila4now' => $hasila4now,
                'hasila4bef' => $hasila4bef,
            ]);
        }elseif($tahun == 2022 && $bulan !='no' && $pekan =='W2'){
            $halaman = 1;
            $cek = Datasales2022::where('bulan', $bulan)->get();
            $hasilcek = $cek->isEmpty();
            $loop = Carbon::parse('1'.$bulan)->month;
            //Variabel Array
            $labelbln = [];
            $totsale = [];
            $tarsale = [];
            $reasale = [];
            $conow = [];
            $cobef = [];
            $nsnow = [];
            $nsbef = [];
            $b2snow = [];
            $b2sbef = [];
            $colonow = [];
            $colobef = [];
            $resenow = [];
            $resebef = [];
            $a1now = [];
            $a1bef = [];
            $a2now = [];
            $a2bef = [];
            $a3now = [];
            $a3bef = [];
            $a4now = [];
            $a4bef = [];
            //Variabel Loop
            $sumb2s = 0;
            $sumcolo = 0;
            $sumrese = 0;
            $cumtarb2s = 0;
            $cumtarcolo = 0;
            $cumtarres = 0;
            $cumcob2s=0;
            $cumcocolo=0;
            $cumcores=0;
            $cumnsb2s=0;
            $cumnscolo=0;
            $cumnsres=0;
            //Variabel Chart Donat
            $telkomsel=0;
            $telkom=0;
            $isat=0;
            $xl=0;
            $h3i=0;
            $sf=0;
            $oth=0;
            $telkomsela=0;
            $telkoma=0;
            $isata=0;
            $xla=0;
            $h3ia=0;
            $sfa=0;
            $otha = 0;
            //Loop YoY Kumulatif
            for ($i=1;$i<=$loop;$i++){
                $sumb2s = $sumb2s + Hasilsales2021::where('sow', 'Macro')->sum($i) + Hasilsales2021::where('sow', 'Micro')->sum($i);
                $sumcolo = $sumcolo + Hasilsales2021::where('sow', 'colo')->sum($i);
                $sumrese = $sumrese + Hasilsales2021::where('sow', 'reseller')->sum($i);
                $cumtarb2s = $cumtarb2s + Targetsales2022::where('sow', 'B2S')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarcolo = $cumtarcolo + Targetsales2022::where('sow', 'Colo')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarres = $cumtarres + Targetsales2022::where('sow', 'Reseller')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $totsalepro = Datasales2022::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $tarsalepro = Targetsales2022::select(Carbon::create()->day(1)->month($i)->format('M'))->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $reasalepro = Datasales2021::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $conowpro = Datasales2022::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cobefpro = Datasales2021::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsnowpro = Datasales2022::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsbefpro = Datasales2021::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $b2snowpro = Datasales2022::where('sow', 'New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $b2sbefpro = Datasales2021::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count() + Datasales2021::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colonowpro = Datasales2022::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colobefpro = Datasales2021::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resenowpro = Datasales2022::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resebefpro = Datasales2021::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1nowpro = Datasales2022::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1befpro = Datasales2021::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2nowpro = Datasales2022::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2befpro = Datasales2021::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3nowpro = Datasales2022::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3befpro = Datasales2021::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4nowpro = Datasales2022::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4befpro = Datasales2021::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $bulansales = Carbon::create()->day(1)->month($i)->format('M');
                $cumcob2s = $cumcob2s + Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcocolo = $cumcocolo + Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcores = $cumcores + Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsb2s = $cumnsb2s + Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnscolo = $cumnscolo + Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsres = $cumnsres + Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsel = $telkomsel + Datasales2022::where('tenant', 'TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkom = $telkom + Datasales2022::where('tenant', 'TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isat = $isat + Datasales2022::where('tenant', 'ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xl = $xl + Datasales2022::where('tenant', 'XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3i = $h3i + Datasales2022::where('tenant', 'H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sf = $sf + Datasales2022::where('tenant', 'SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $oth = $oth + Datasales2022::where('tenant', 'Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsela = $telkomsela + Datasales2022::where('kategori', 'New Sales')->where('tenant','TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkoma = $telkoma + Datasales2022::where('kategori', 'New Sales')->where('tenant','TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isata = $isata + Datasales2022::where('kategori', 'New Sales')->where('tenant','ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xla = $xla + Datasales2022::where('kategori', 'New Sales')->where('tenant','XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3ia = $h3ia + Datasales2022::where('kategori', 'New Sales')->where('tenant','H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sfa = $sfa + Datasales2022::where('kategori', 'New Sales')->where('tenant','SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $otha = $otha + Datasales2022::where('kategori', 'New Sales')->where('tenant','Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                array_push($labelbln, $bulansales);
                array_push($totsale, $totsalepro);
                array_push($tarsale, $tarsalepro);
                array_push($reasale, $reasalepro);
                array_push($conow, $conowpro);
                array_push($cobef, $cobefpro);
                array_push($nsnow, $nsnowpro);
                array_push($nsbef, $nsbefpro);
                array_push($b2snow,$b2snowpro);
                array_push($b2sbef,$b2sbefpro);
                array_push($colonow,$colonowpro);
                array_push($colobef,$colobefpro);
                array_push($resenow,$resenowpro);
                array_push($resebef,$resebefpro);
                array_push($a1now,$a1nowpro);
                array_push($a1bef,$a1befpro);
                array_push($a2now,$a2nowpro);
                array_push($a2bef,$a2befpro);
                array_push($a3now,$a3nowpro);
                array_push($a3bef,$a3befpro);
                array_push($a4now,$a4nowpro);
                array_push($a4bef,$a4befpro);
            }
            $labelbulan = json_encode($labelbln);
            $hasiltotsale = json_encode($totsale);
            $hasiltarsale = json_encode($tarsale);
            $hasilreasale = json_encode($reasale);
            $hasilconow = json_encode($conow);
            $hasilcobef = json_encode($cobef);
            $hasilnsnow = json_encode($nsnow);
            $hasilnsbef = json_encode($nsbef);
            $hasilb2snow = json_encode($b2snow);
            $hasilb2sbef = json_encode($b2sbef);
            $hasilcolonow = json_encode($colonow);
            $hasilcolobef = json_encode($colobef);
            $hasilresenow = json_encode($resenow);
            $hasilresebef = json_encode($resebef);
            $hasila1now = json_encode($a1now);
            $hasila1bef = json_encode($a1bef);
            $hasila2now = json_encode($a2now);
            $hasila2bef = json_encode($a2bef);
            $hasila3now = json_encode($a3now);
            $hasila3bef = json_encode($a3bef);
            $hasila4now = json_encode($a4now);
            $hasila4bef = json_encode($a4bef);
            $tarb2s = Targetsales2022::select($bulan)->where('sow', 'B2S')->sum($bulan);
            $tarcolo = Targetsales2022::select($bulan)->where('sow', 'Colo')->sum($bulan);
            $tarres = Targetsales2022::select($bulan)->where('sow', 'Reseller')->sum($bulan);
            $b2sco = Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $coloco = Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $reseco = Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $b2sns = Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $colons = Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $resens = Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $b2stotal = $b2sco + $b2sns;
            $colototal = $coloco + $colons;
            $resetotal = $reseco + $resens;
            $totalchart = $telkomsel + $telkom + $isat + $xl + $h3i + $sf + $oth;
            $totalcharta = $telkomsela + $telkoma + $isata + $xla + $h3ia + $sfa + $otha;
            // dd($sumb2s, $sumcolo);
            return view('sales.salesreport', [
                'halaman' => $halaman,
                'labelbulan' => $labelbulan,
                'tahun' => $tahun,
                'bulan' => $bulan,
                'pekan' => $pekan,
                'b2sco' => $b2sco,
                'macroco' => $b2sco,
                'microco' => $b2sco,
                'coloco' => $coloco,
                'reseco' => $reseco,
                'b2sns' => $b2sns,
                'colons' => $colons,
                'resens' => $resens,
                'b2stotal' => $b2stotal,
                'colototal' => $colototal,
                'resetotal' => $resetotal,
                'tarb2s' => $tarb2s,
                'tarcolo' => $tarcolo,
                'tarres' => $tarres,
                'sumb2s' => $sumb2s,
                'sumcolo' => $sumcolo,
                'sumrese' => $sumrese,
                'cumtarb2s' => $cumtarb2s,
                'cumtarcolo' => $cumtarcolo,
                'cumtarres' => $cumtarres,
                'cumcob2s'  => $cumcob2s,
                'cumcocolo'  => $cumcocolo,
                'cumcores'  => $cumcores,
                'cumnsb2s'  => $cumnsb2s,
                'cumnscolo'  => $cumnscolo,
                'cumnsres'  => $cumnsres,
                'telkomsel' => $telkomsel,
                'telkom' => $telkom,
                'isat' => $isat,
                'xl' => $xl,
                'h3i' => $h3i,
                'sf' => $sf,
                'oth' => $oth,
                'totalchart' => $totalchart,
                'telkomsela' => $telkomsela,
                'telkoma' => $telkoma,
                'isata' => $isata,
                'xla' => $xla,
                'h3ia' => $h3ia,
                'sfa' => $sfa,
                'otha' => $otha,
                'totalcharta' => $totalcharta,
                'hasilcek' => $hasilcek,
                'totsalepro' => $totsalepro,
                'tarsalepro' => $tarsalepro,
                'reasalepro' => $reasalepro,
                'conowpro' => $conowpro,
                'cobefpro' => $cobefpro,
                'nsnowpro' => $nsnowpro,
                'nsbefpro' => $nsbefpro,
                'b2snowpro' => $b2snowpro,
                'b2sbefpro' => $b2sbefpro,
                'colonowpro' => $colonowpro,
                'colobefpro' => $colobefpro,
                'resenowpro' => $resenowpro,
                'resebefpro' => $resebefpro,
                'a1nowpro' => $a1nowpro,
                'a1befpro' => $a1befpro,
                'a2nowpro' => $a2nowpro,
                'a2befpro' => $a2befpro,
                'a3nowpro' => $a3nowpro,
                'a3befpro' => $a3befpro,
                'a4nowpro' => $a4nowpro,
                'a4befpro' => $a4befpro,
                'hasiltotsale' => $hasiltotsale,
                'hasiltarsale' => $hasiltarsale,
                'hasilreasale' => $hasilreasale,
                'hasilconow' => $hasilconow,
                'hasilcobef' => $hasilcobef,
                'hasilnsnow' => $hasilnsnow,
                'hasilnsbef' => $hasilnsbef,
                'hasilb2snow' => $hasilb2snow,
                'hasilb2sbef' => $hasilb2sbef,
                'hasilcolonow' => $hasilcolonow,
                'hasilcolobef' => $hasilcolobef,
                'hasilresenow' => $hasilresenow,
                'hasilresebef' => $hasilresebef,
                'hasila1now' => $hasila1now,
                'hasila1bef' => $hasila1bef,
                'hasila2now' => $hasila2now,
                'hasila2bef' => $hasila2bef,
                'hasila3now' => $hasila3now,
                'hasila3bef' => $hasila3bef,
                'hasila4now' => $hasila4now,
                'hasila4bef' => $hasila4bef,
            ]);

        }elseif($tahun == 2022 && $bulan !='no' && $pekan =='W3'){
            $halaman = 1;
            $cek = Datasales2022::where('bulan', $bulan)->get();
            $hasilcek = $cek->isEmpty();
            $loop = Carbon::parse('1'.$bulan)->month;
            //Variabel Array
            $labelbln = [];
            $totsale = [];
            $tarsale = [];
            $reasale = [];
            $conow = [];
            $cobef = [];
            $nsnow = [];
            $nsbef = [];
            $b2snow = [];
            $b2sbef = [];
            $colonow = [];
            $colobef = [];
            $resenow = [];
            $resebef = [];
            $a1now = [];
            $a1bef = [];
            $a2now = [];
            $a2bef = [];
            $a3now = [];
            $a3bef = [];
            $a4now = [];
            $a4bef = [];
            //Variabel Loop
            $sumb2s = 0;
            $sumcolo = 0;
            $sumrese = 0;
            $cumtarb2s = 0;
            $cumtarcolo = 0;
            $cumtarres = 0;
            $cumcob2s=0;
            $cumcocolo=0;
            $cumcores=0;
            $cumnsb2s=0;
            $cumnscolo=0;
            $cumnsres=0;
            //Variabel Chart Donat
            $telkomsel=0;
            $telkom=0;
            $isat=0;
            $xl=0;
            $h3i=0;
            $sf=0;
            $oth=0;
            $telkomsela=0;
            $telkoma=0;
            $isata=0;
            $xla=0;
            $h3ia=0;
            $sfa=0;
            $otha = 0;
            //Loop YoY Kumulatif
            for ($i=1;$i<=$loop;$i++){
                $sumb2s = $sumb2s + Hasilsales2021::where('sow', 'Macro')->sum($i) + Hasilsales2021::where('sow', 'Micro')->sum($i);
                $sumcolo = $sumcolo + Hasilsales2021::where('sow', 'colo')->sum($i);
                $sumrese = $sumrese + Hasilsales2021::where('sow', 'reseller')->sum($i);
                $cumtarb2s = $cumtarb2s + Targetsales2022::where('sow', 'B2S')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarcolo = $cumtarcolo + Targetsales2022::where('sow', 'Colo')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarres = $cumtarres + Targetsales2022::where('sow', 'Reseller')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $totsalepro = Datasales2022::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $tarsalepro = Targetsales2022::select(Carbon::create()->day(1)->month($i)->format('M'))->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $reasalepro = Datasales2021::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $conowpro = Datasales2022::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cobefpro = Datasales2021::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsnowpro = Datasales2022::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsbefpro = Datasales2021::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $b2snowpro = Datasales2022::where('sow', 'New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $b2sbefpro = Datasales2021::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count() + Datasales2021::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colonowpro = Datasales2022::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colobefpro = Datasales2021::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resenowpro = Datasales2022::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resebefpro = Datasales2021::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1nowpro = Datasales2022::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1befpro = Datasales2021::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2nowpro = Datasales2022::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2befpro = Datasales2021::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3nowpro = Datasales2022::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3befpro = Datasales2021::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4nowpro = Datasales2022::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4befpro = Datasales2021::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $bulansales = Carbon::create()->day(1)->month($i)->format('M');
                $cumcob2s = $cumcob2s + Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcocolo = $cumcocolo + Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcores = $cumcores + Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsb2s = $cumnsb2s + Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnscolo = $cumnscolo + Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsres = $cumnsres + Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsel = $telkomsel + Datasales2022::where('tenant', 'TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkom = $telkom + Datasales2022::where('tenant', 'TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isat = $isat + Datasales2022::where('tenant', 'ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xl = $xl + Datasales2022::where('tenant', 'XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3i = $h3i + Datasales2022::where('tenant', 'H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sf = $sf + Datasales2022::where('tenant', 'SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $oth = $oth + Datasales2022::where('tenant', 'Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsela = $telkomsela + Datasales2022::where('kategori', 'New Sales')->where('tenant','TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkoma = $telkoma + Datasales2022::where('kategori', 'New Sales')->where('tenant','TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isata = $isata + Datasales2022::where('kategori', 'New Sales')->where('tenant','ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xla = $xla + Datasales2022::where('kategori', 'New Sales')->where('tenant','XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3ia = $h3ia + Datasales2022::where('kategori', 'New Sales')->where('tenant','H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sfa = $sfa + Datasales2022::where('kategori', 'New Sales')->where('tenant','SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $otha = $otha + Datasales2022::where('kategori', 'New Sales')->where('tenant','Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                array_push($labelbln, $bulansales);
                array_push($totsale, $totsalepro);
                array_push($tarsale, $tarsalepro);
                array_push($reasale, $reasalepro);
                array_push($conow, $conowpro);
                array_push($cobef, $cobefpro);
                array_push($nsnow, $nsnowpro);
                array_push($nsbef, $nsbefpro);
                array_push($b2snow,$b2snowpro);
                array_push($b2sbef,$b2sbefpro);
                array_push($colonow,$colonowpro);
                array_push($colobef,$colobefpro);
                array_push($resenow,$resenowpro);
                array_push($resebef,$resebefpro);
                array_push($a1now,$a1nowpro);
                array_push($a1bef,$a1befpro);
                array_push($a2now,$a2nowpro);
                array_push($a2bef,$a2befpro);
                array_push($a3now,$a3nowpro);
                array_push($a3bef,$a3befpro);
                array_push($a4now,$a4nowpro);
                array_push($a4bef,$a4befpro);
            }
            $labelbulan = json_encode($labelbln);
            $hasiltotsale = json_encode($totsale);
            $hasiltarsale = json_encode($tarsale);
            $hasilreasale = json_encode($reasale);
            $hasilconow = json_encode($conow);
            $hasilcobef = json_encode($cobef);
            $hasilnsnow = json_encode($nsnow);
            $hasilnsbef = json_encode($nsbef);
            $hasilb2snow = json_encode($b2snow);
            $hasilb2sbef = json_encode($b2sbef);
            $hasilcolonow = json_encode($colonow);
            $hasilcolobef = json_encode($colobef);
            $hasilresenow = json_encode($resenow);
            $hasilresebef = json_encode($resebef);
            $hasila1now = json_encode($a1now);
            $hasila1bef = json_encode($a1bef);
            $hasila2now = json_encode($a2now);
            $hasila2bef = json_encode($a2bef);
            $hasila3now = json_encode($a3now);
            $hasila3bef = json_encode($a3bef);
            $hasila4now = json_encode($a4now);
            $hasila4bef = json_encode($a4bef);
            $tarb2s = Targetsales2022::select($bulan)->where('sow', 'B2S')->sum($bulan);
            $tarcolo = Targetsales2022::select($bulan)->where('sow', 'Colo')->sum($bulan);
            $tarres = Targetsales2022::select($bulan)->where('sow', 'Reseller')->sum($bulan);
            $b2sco = Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $coloco = Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $reseco = Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $b2sns = Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $colons = Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $resens = Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $b2stotal = $b2sco + $b2sns;
            $colototal = $coloco + $colons;
            $resetotal = $reseco + $resens;
            $totalchart = $telkomsel + $telkom + $isat + $xl + $h3i + $sf + $oth;
            $totalcharta = $telkomsela + $telkoma + $isata + $xla + $h3ia + $sfa + $otha;
            // dd($sumb2s, $sumcolo);
            return view('sales.salesreport', [
                'halaman' => $halaman,
                'labelbulan' => $labelbulan,
                'tahun' => $tahun,
                'bulan' => $bulan,
                'pekan' => $pekan,
                'b2sco' => $b2sco,
                'macroco' => $b2sco,
                'microco' => $b2sco,
                'coloco' => $coloco,
                'reseco' => $reseco,
                'b2sns' => $b2sns,
                'colons' => $colons,
                'resens' => $resens,
                'b2stotal' => $b2stotal,
                'colototal' => $colototal,
                'resetotal' => $resetotal,
                'tarb2s' => $tarb2s,
                'tarcolo' => $tarcolo,
                'tarres' => $tarres,
                'sumb2s' => $sumb2s,
                'sumcolo' => $sumcolo,
                'sumrese' => $sumrese,
                'cumtarb2s' => $cumtarb2s,
                'cumtarcolo' => $cumtarcolo,
                'cumtarres' => $cumtarres,
                'cumcob2s'  => $cumcob2s,
                'cumcocolo'  => $cumcocolo,
                'cumcores'  => $cumcores,
                'cumnsb2s'  => $cumnsb2s,
                'cumnscolo'  => $cumnscolo,
                'cumnsres'  => $cumnsres,
                'telkomsel' => $telkomsel,
                'telkom' => $telkom,
                'isat' => $isat,
                'xl' => $xl,
                'h3i' => $h3i,
                'sf' => $sf,
                'oth' => $oth,
                'totalchart' => $totalchart,
                'telkomsela' => $telkomsela,
                'telkoma' => $telkoma,
                'isata' => $isata,
                'xla' => $xla,
                'h3ia' => $h3ia,
                'sfa' => $sfa,
                'otha' => $otha,
                'totalcharta' => $totalcharta,
                'hasilcek' => $hasilcek,
                'totsalepro' => $totsalepro,
                'tarsalepro' => $tarsalepro,
                'reasalepro' => $reasalepro,
                'conowpro' => $conowpro,
                'cobefpro' => $cobefpro,
                'nsnowpro' => $nsnowpro,
                'nsbefpro' => $nsbefpro,
                'b2snowpro' => $b2snowpro,
                'b2sbefpro' => $b2sbefpro,
                'colonowpro' => $colonowpro,
                'colobefpro' => $colobefpro,
                'resenowpro' => $resenowpro,
                'resebefpro' => $resebefpro,
                'a1nowpro' => $a1nowpro,
                'a1befpro' => $a1befpro,
                'a2nowpro' => $a2nowpro,
                'a2befpro' => $a2befpro,
                'a3nowpro' => $a3nowpro,
                'a3befpro' => $a3befpro,
                'a4nowpro' => $a4nowpro,
                'a4befpro' => $a4befpro,
                'hasiltotsale' => $hasiltotsale,
                'hasiltarsale' => $hasiltarsale,
                'hasilreasale' => $hasilreasale,
                'hasilconow' => $hasilconow,
                'hasilcobef' => $hasilcobef,
                'hasilnsnow' => $hasilnsnow,
                'hasilnsbef' => $hasilnsbef,
                'hasilb2snow' => $hasilb2snow,
                'hasilb2sbef' => $hasilb2sbef,
                'hasilcolonow' => $hasilcolonow,
                'hasilcolobef' => $hasilcolobef,
                'hasilresenow' => $hasilresenow,
                'hasilresebef' => $hasilresebef,
                'hasila1now' => $hasila1now,
                'hasila1bef' => $hasila1bef,
                'hasila2now' => $hasila2now,
                'hasila2bef' => $hasila2bef,
                'hasila3now' => $hasila3now,
                'hasila3bef' => $hasila3bef,
                'hasila4now' => $hasila4now,
                'hasila4bef' => $hasila4bef,
            ]);
        }elseif($tahun == 2022 && $bulan !='no' && $pekan =='W4'){
            $halaman = 1;
            $cek = Datasales2022::where('bulan', $bulan)->get();
            $hasilcek = $cek->isEmpty();
            $loop = Carbon::parse('1'.$bulan)->month;
            //Variabel Array
            $labelbln = [];
            $totsale = [];
            $tarsale = [];
            $reasale = [];
            $conow = [];
            $cobef = [];
            $nsnow = [];
            $nsbef = [];
            $b2snow = [];
            $b2sbef = [];
            $colonow = [];
            $colobef = [];
            $resenow = [];
            $resebef = [];
            $a1now = [];
            $a1bef = [];
            $a2now = [];
            $a2bef = [];
            $a3now = [];
            $a3bef = [];
            $a4now = [];
            $a4bef = [];
            //Variabel Loop
            $sumb2s = 0;
            $sumcolo = 0;
            $sumrese = 0;
            $cumtarb2s = 0;
            $cumtarcolo = 0;
            $cumtarres = 0;
            $cumcob2s=0;
            $cumcocolo=0;
            $cumcores=0;
            $cumnsb2s=0;
            $cumnscolo=0;
            $cumnsres=0;
            //Variabel Chart Donat
            $telkomsel=0;
            $telkom=0;
            $isat=0;
            $xl=0;
            $h3i=0;
            $sf=0;
            $oth=0;
            $telkomsela=0;
            $telkoma=0;
            $isata=0;
            $xla=0;
            $h3ia=0;
            $sfa=0;
            $otha = 0;
            //Loop YoY Kumulatif
            for ($i=1;$i<=$loop;$i++){
                $sumb2s = $sumb2s + Hasilsales2021::where('sow', 'Macro')->sum($i) + Hasilsales2021::where('sow', 'Micro')->sum($i);
                $sumcolo = $sumcolo + Hasilsales2021::where('sow', 'colo')->sum($i);
                $sumrese = $sumrese + Hasilsales2021::where('sow', 'reseller')->sum($i);
                $cumtarb2s = $cumtarb2s + Targetsales2022::where('sow', 'B2S')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarcolo = $cumtarcolo + Targetsales2022::where('sow', 'Colo')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarres = $cumtarres + Targetsales2022::where('sow', 'Reseller')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $totsalepro = Datasales2022::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $tarsalepro = Targetsales2022::select(Carbon::create()->day(1)->month($i)->format('M'))->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $reasalepro = Datasales2021::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $conowpro = Datasales2022::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cobefpro = Datasales2021::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsnowpro = Datasales2022::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsbefpro = Datasales2021::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $b2snowpro = Datasales2022::where('sow', 'New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $b2sbefpro = Datasales2021::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count() + Datasales2021::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colonowpro = Datasales2022::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colobefpro = Datasales2021::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resenowpro = Datasales2022::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resebefpro = Datasales2021::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1nowpro = Datasales2022::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1befpro = Datasales2021::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2nowpro = Datasales2022::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2befpro = Datasales2021::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3nowpro = Datasales2022::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3befpro = Datasales2021::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4nowpro = Datasales2022::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4befpro = Datasales2021::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $bulansales = Carbon::create()->day(1)->month($i)->format('M');
                $cumcob2s = $cumcob2s + Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcocolo = $cumcocolo + Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcores = $cumcores + Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsb2s = $cumnsb2s + Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnscolo = $cumnscolo + Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsres = $cumnsres + Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsel = $telkomsel + Datasales2022::where('tenant', 'TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkom = $telkom + Datasales2022::where('tenant', 'TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isat = $isat + Datasales2022::where('tenant', 'ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xl = $xl + Datasales2022::where('tenant', 'XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3i = $h3i + Datasales2022::where('tenant', 'H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sf = $sf + Datasales2022::where('tenant', 'SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $oth = $oth + Datasales2022::where('tenant', 'Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsela = $telkomsela + Datasales2022::where('kategori', 'New Sales')->where('tenant','TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkoma = $telkoma + Datasales2022::where('kategori', 'New Sales')->where('tenant','TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isata = $isata + Datasales2022::where('kategori', 'New Sales')->where('tenant','ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xla = $xla + Datasales2022::where('kategori', 'New Sales')->where('tenant','XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3ia = $h3ia + Datasales2022::where('kategori', 'New Sales')->where('tenant','H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sfa = $sfa + Datasales2022::where('kategori', 'New Sales')->where('tenant','SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $otha = $otha + Datasales2022::where('kategori', 'New Sales')->where('tenant','Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                array_push($labelbln, $bulansales);
                array_push($totsale, $totsalepro);
                array_push($tarsale, $tarsalepro);
                array_push($reasale, $reasalepro);
                array_push($conow, $conowpro);
                array_push($cobef, $cobefpro);
                array_push($nsnow, $nsnowpro);
                array_push($nsbef, $nsbefpro);
                array_push($b2snow,$b2snowpro);
                array_push($b2sbef,$b2sbefpro);
                array_push($colonow,$colonowpro);
                array_push($colobef,$colobefpro);
                array_push($resenow,$resenowpro);
                array_push($resebef,$resebefpro);
                array_push($a1now,$a1nowpro);
                array_push($a1bef,$a1befpro);
                array_push($a2now,$a2nowpro);
                array_push($a2bef,$a2befpro);
                array_push($a3now,$a3nowpro);
                array_push($a3bef,$a3befpro);
                array_push($a4now,$a4nowpro);
                array_push($a4bef,$a4befpro);
            }
            $labelbulan = json_encode($labelbln);
            $hasiltotsale = json_encode($totsale);
            $hasiltarsale = json_encode($tarsale);
            $hasilreasale = json_encode($reasale);
            $hasilconow = json_encode($conow);
            $hasilcobef = json_encode($cobef);
            $hasilnsnow = json_encode($nsnow);
            $hasilnsbef = json_encode($nsbef);
            $hasilb2snow = json_encode($b2snow);
            $hasilb2sbef = json_encode($b2sbef);
            $hasilcolonow = json_encode($colonow);
            $hasilcolobef = json_encode($colobef);
            $hasilresenow = json_encode($resenow);
            $hasilresebef = json_encode($resebef);
            $hasila1now = json_encode($a1now);
            $hasila1bef = json_encode($a1bef);
            $hasila2now = json_encode($a2now);
            $hasila2bef = json_encode($a2bef);
            $hasila3now = json_encode($a3now);
            $hasila3bef = json_encode($a3bef);
            $hasila4now = json_encode($a4now);
            $hasila4bef = json_encode($a4bef);
            $tarb2s = Targetsales2022::select($bulan)->where('sow', 'B2S')->sum($bulan);
            $tarcolo = Targetsales2022::select($bulan)->where('sow', 'Colo')->sum($bulan);
            $tarres = Targetsales2022::select($bulan)->where('sow', 'Reseller')->sum($bulan);
            $b2sco = Datasales2022::where('kategori', 'Carry Over')->where('sow','New Site')->where('bulan', $bulan)->count();
            $coloco = Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', $bulan)->count();
            $reseco = Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', $bulan)->count();
            $b2sns = Datasales2022::where('kategori', 'New Sales')->where('sow','New Site')->where('bulan', $bulan)->count();
            $colons = Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', $bulan)->count();
            $resens = Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', $bulan)->count();
            $b2stotal = $b2sco + $b2sns;
            $colototal = $coloco + $colons;
            $resetotal = $reseco + $resens;
            $totalchart = $telkomsel + $telkom + $isat + $xl + $h3i + $sf + $oth;
            $totalcharta = $telkomsela + $telkoma + $isata + $xla + $h3ia + $sfa + $otha;
            // dd($sumb2s, $sumcolo);
            return view('sales.salesreport', [
                'halaman' => $halaman,
                'labelbulan' => $labelbulan,
                'tahun' => $tahun,
                'bulan' => $bulan,
                'pekan' => $pekan,
                'b2sco' => $b2sco,
                'macroco' => $b2sco,
                'microco' => $b2sco,
                'coloco' => $coloco,
                'reseco' => $reseco,
                'b2sns' => $b2sns,
                'colons' => $colons,
                'resens' => $resens,
                'b2stotal' => $b2stotal,
                'colototal' => $colototal,
                'resetotal' => $resetotal,
                'tarb2s' => $tarb2s,
                'tarcolo' => $tarcolo,
                'tarres' => $tarres,
                'sumb2s' => $sumb2s,
                'sumcolo' => $sumcolo,
                'sumrese' => $sumrese,
                'cumtarb2s' => $cumtarb2s,
                'cumtarcolo' => $cumtarcolo,
                'cumtarres' => $cumtarres,
                'cumcob2s'  => $cumcob2s,
                'cumcocolo'  => $cumcocolo,
                'cumcores'  => $cumcores,
                'cumnsb2s'  => $cumnsb2s,
                'cumnscolo'  => $cumnscolo,
                'cumnsres'  => $cumnsres,
                'telkomsel' => $telkomsel,
                'telkom' => $telkom,
                'isat' => $isat,
                'xl' => $xl,
                'h3i' => $h3i,
                'sf' => $sf,
                'oth' => $oth,
                'totalchart' => $totalchart,
                'telkomsela' => $telkomsela,
                'telkoma' => $telkoma,
                'isata' => $isata,
                'xla' => $xla,
                'h3ia' => $h3ia,
                'sfa' => $sfa,
                'otha' => $otha,
                'totalcharta' => $totalcharta,
                'hasilcek' => $hasilcek,
                'totsalepro' => $totsalepro,
                'tarsalepro' => $tarsalepro,
                'reasalepro' => $reasalepro,
                'conowpro' => $conowpro,
                'cobefpro' => $cobefpro,
                'nsnowpro' => $nsnowpro,
                'nsbefpro' => $nsbefpro,
                'b2snowpro' => $b2snowpro,
                'b2sbefpro' => $b2sbefpro,
                'colonowpro' => $colonowpro,
                'colobefpro' => $colobefpro,
                'resenowpro' => $resenowpro,
                'resebefpro' => $resebefpro,
                'a1nowpro' => $a1nowpro,
                'a1befpro' => $a1befpro,
                'a2nowpro' => $a2nowpro,
                'a2befpro' => $a2befpro,
                'a3nowpro' => $a3nowpro,
                'a3befpro' => $a3befpro,
                'a4nowpro' => $a4nowpro,
                'a4befpro' => $a4befpro,
                'hasiltotsale' => $hasiltotsale,
                'hasiltarsale' => $hasiltarsale,
                'hasilreasale' => $hasilreasale,
                'hasilconow' => $hasilconow,
                'hasilcobef' => $hasilcobef,
                'hasilnsnow' => $hasilnsnow,
                'hasilnsbef' => $hasilnsbef,
                'hasilb2snow' => $hasilb2snow,
                'hasilb2sbef' => $hasilb2sbef,
                'hasilcolonow' => $hasilcolonow,
                'hasilcolobef' => $hasilcolobef,
                'hasilresenow' => $hasilresenow,
                'hasilresebef' => $hasilresebef,
                'hasila1now' => $hasila1now,
                'hasila1bef' => $hasila1bef,
                'hasila2now' => $hasila2now,
                'hasila2bef' => $hasila2bef,
                'hasila3now' => $hasila3now,
                'hasila3bef' => $hasila3bef,
                'hasila4now' => $hasila4now,
                'hasila4bef' => $hasila4bef,
            ]);
        }elseif($tahun == 2021 && $bulan !='no' && $pekan =='no'){
            $halaman = 1;
            $cek = Datasales2022::where('bulan', $bulan)->get();
            $hasilcek = $cek->isEmpty();
            $loop = Carbon::parse('1'.$bulan)->month;
            // Variabel Array
            $labelbln = [];
            $totsale = [];
            $tarsale = [];
            $reasale = [];
            $conow = [];
            $cobef = [];
            $nsnow = [];
            $nsbef = [];
            $macronow = [];
            $macrobef = [];
            $micronow = [];
            $microbef = [];
            $colonow = [];
            $colobef = [];
            $resenow = [];
            $resebef = [];
            $a1now = [];
            $a1bef = [];
            $a2now = [];
            $a2bef = [];
            $a3now = [];
            $a3bef = [];
            $a4now = [];
            $a4bef = [];
            // Variabel Loop
            $summacro = 0;
            $summicro = 0;
            $sumcolo = 0;
            $sumrese = 0;
            $cumtarmacro = 0;
            $cumtarmicro = 0;
            $cumtarcolo = 0;
            $cumtarres = 0;
            $totsalepro = 0;
            $tarsalepro = 0;
            $reasalepro = 0;
            $conowpro = 0;
            $cobefpro = 0;
            $nsnowpro = 0;
            $nsbefpro = 0;
            $macronowpro = 0;
            $macrobefpro = 0;
            $micronowpro = 0;
            $microbefpro = 0;
            $colonowpro = 0;
            $colobefpro = 0;
            $resenowpro = 0;
            $resebefpro = 0;
            $a1nowpro = 0;
            $a1befpro = 0;
            $a2nowpro = 0;
            $a2befpro = 0;
            $a3nowpro = 0;
            $a3befpro = 0;
            $a4nowpro = 0;
            $a4befpro = 0;
            $cumcomacro =  0;
            $cumcomicro = 0;
            $cumcocolo = 0;
            $cumcores = 0;
            $cumnsmacro =  0;
            $cumnsmicro = 0;
            $cumnscolo = 0;
            $cumnsres = 0;
            // Variabel Chart
            $telkomsel=0;
            $telkom=0;
            $isat=0;
            $xl=0;
            $h3i=0;
            $sf=0;
            $sti=0;
            $oth=0;
            $telkomsela=0;
            $telkoma=0;
            $isata=0;
            $xla=0;
            $h3ia=0;
            $sfa=0;
            $stia=0;
            $otha = 0;
            for ($i=1;$i<=$loop;$i++){
                $summacro = $summacro + Hasilsales2020::where('sow', 'macro')->sum($i);
                $summicro = $summicro + Hasilsales2020::where('sow', 'micro')->sum($i);
                $sumcolo = $sumcolo + Hasilsales2020::where('sow', 'colo')->sum($i);
                $sumrese = $sumrese + Hasilsales2020::where('sow', 'reseller')->sum($i);
                $cumtarmacro = $cumtarmacro + Targetsales2021::where('sow', 'Macro')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarmicro = $cumtarmicro + Targetsales2021::where('sow', 'Micro')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarcolo = $cumtarcolo + Targetsales2021::where('sow', 'Colo')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarres = $cumtarres + Targetsales2021::where('sow', 'Reseller')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $totsalepro = Datasales2021::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $tarsalepro = Targetsales2021::select(Carbon::create()->day(1)->month($i)->format('M'))->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $reasalepro = Datasales2020::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $conowpro = Datasales2021::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cobefpro = Datasales2020::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsnowpro = Datasales2021::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsbefpro = Datasales2020::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $macronowpro = Datasales2021::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $macrobefpro = Datasales2020::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $micronowpro = Datasales2021::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $microbefpro = Datasales2020::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colonowpro = Datasales2021::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colobefpro = Datasales2020::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resenowpro = Datasales2021::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resebefpro = Datasales2020::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1nowpro = Datasales2021::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1befpro = Datasales2020::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2nowpro = Datasales2021::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2befpro = Datasales2020::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3nowpro = Datasales2021::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3befpro = Datasales2020::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4nowpro = Datasales2021::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4befpro = Datasales2020::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $bulansales = Carbon::create()->day(1)->month($i)->format('M');
                $cumcomacro =  $cumcomacro + Datasales2021::where('kategori', 'Carry Over')->where('sow','Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcomicro = $cumcomicro + Datasales2021::where('kategori', 'Carry Over')->where('sow','Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcocolo = $cumcocolo + Datasales2021::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcores = $cumcores + Datasales2021::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsmacro =  $cumnsmacro + Datasales2021::where('kategori', 'New Sales')->where('sow','Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsmicro = $cumnsmicro + Datasales2021::where('kategori', 'New Sales')->where('sow','Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnscolo = $cumnscolo + Datasales2021::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsres = $cumnsres + Datasales2021::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsel = $telkomsel + Datasales2021::where('tenant', 'TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkom = $telkom + Datasales2021::where('tenant', 'TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isat = $isat + Datasales2021::where('tenant', 'ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xl = $xl + Datasales2021::where('tenant', 'XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3i = $h3i + Datasales2021::where('tenant', 'H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sf = $sf + Datasales2021::where('tenant', 'SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sti = $sti + Datasales2021::where('tenant', 'STI')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $oth = $oth + Datasales2021::where('tenant', 'Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsela = $telkomsela + Datasales2021::where('kategori', 'New Sales')->where('tenant','TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkoma = $telkoma + Datasales2021::where('kategori', 'New Sales')->where('tenant','TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isata = $isata + Datasales2021::where('kategori', 'New Sales')->where('tenant','ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xla = $xla + Datasales2021::where('kategori', 'New Sales')->where('tenant','XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3ia = $h3ia + Datasales2021::where('kategori', 'New Sales')->where('tenant','H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sfa = $sfa + Datasales2021::where('kategori', 'New Sales')->where('tenant','SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $stia = $stia + Datasales2021::where('kategori', 'New Sales')->where('tenant','STI')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $otha = $otha + Datasales2021::where('kategori', 'New Sales')->where('tenant','Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                array_push($labelbln, $bulansales);
                array_push($totsale, $totsalepro);
                array_push($tarsale, $tarsalepro);
                array_push($reasale, $reasalepro);
                array_push($conow, $conowpro);
                array_push($cobef, $cobefpro);
                array_push($nsnow, $nsnowpro);
                array_push($nsbef, $nsbefpro);
                array_push($macronow, $macronowpro);
                array_push($macrobef, $macrobefpro);
                array_push($micronow, $micronowpro);
                array_push($microbef, $microbefpro);
                array_push($colonow,$colonowpro);
                array_push($colobef,$colobefpro);
                array_push($resenow,$resenowpro);
                array_push($resebef,$resebefpro);
                array_push($a1now,$a1nowpro);
                array_push($a1bef,$a1befpro);
                array_push($a2now,$a2nowpro);
                array_push($a2bef,$a2befpro);
                array_push($a3now,$a3nowpro);
                array_push($a3bef,$a3befpro);
                array_push($a4now,$a4nowpro);
                array_push($a4bef,$a4befpro);
            }
            $labelbulan = json_encode($labelbln);
            $hasiltotsale = json_encode($totsale);
            $hasiltarsale = json_encode($tarsale);
            $hasilreasale = json_encode($reasale);
            $hasilconow = json_encode($conow);
            $hasilcobef = json_encode($cobef);
            $hasilnsnow = json_encode($nsnow);
            $hasilnsbef = json_encode($nsbef);
            $hasilmacronow = json_encode($macronow);
            $hasilmacrobef = json_encode($macrobef);
            $hasilmicronow = json_encode($micronow);
            $hasilmicrobef = json_encode($microbef);
            $hasilcolonow = json_encode($colonow);
            $hasilcolobef = json_encode($colobef);
            $hasilresenow = json_encode($resenow);
            $hasilresebef = json_encode($resebef);
            $hasila1now = json_encode($a1now);
            $hasila1bef = json_encode($a1bef);
            $hasila2now = json_encode($a2now);
            $hasila2bef = json_encode($a2bef);
            $hasila3now = json_encode($a3now);
            $hasila3bef = json_encode($a3bef);
            $hasila4now = json_encode($a4now);
            $hasila4bef = json_encode($a4bef);
            $tarmacro = Targetsales2021::select($bulan)->where('sow', 'Macro')->sum($bulan);
            $tarmicro = Targetsales2021::select($bulan)->where('sow', 'Micro')->sum($bulan);
            $tarcolo = Targetsales2021::select($bulan)->where('sow', 'Colo')->sum($bulan);
            $tarres = Targetsales2021::select($bulan)->where('sow', 'Reseller')->sum($bulan);
            $macroco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Macro')->where('bulan', $bulan)->count();
            $microco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Micro')->where('bulan', $bulan)->count();
            $coloco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', $bulan)->count();
            $reseco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', $bulan)->count();
            $macrons = Datasales2021::where('kategori', 'New Sales')->where('sow','Macro')->where('bulan', $bulan)->count();
            $microns = Datasales2021::where('kategori', 'New Sales')->where('sow','Micro')->where('bulan', $bulan)->count();
            $colons = Datasales2021::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', $bulan)->count();
            $resens = Datasales2021::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', $bulan)->count();
            $macrototal = $macroco + $macrons;
            $micrototal = $microco + $microns;
            $colototal = $coloco + $colons;
            $resetotal = $reseco + $resens;
            $totalchart = $telkomsel + $telkom + $isat + $xl + $h3i + $sf + $sti + $oth;
            $totalcharta = $telkomsela + $telkoma + $isata + $xla + $h3ia + $sfa + $stia + $otha;
            
            return view('sales.salesreport', [
                'halaman' => $halaman,
                'labelbulan' => $labelbulan,
                'tahun' => $tahun,
                'bulan' => $bulan,
                'pekan' => $pekan,
                'macroco' => $macroco,
                'microco' => $microco,
                'coloco' => $coloco,
                'reseco' => $reseco,
                'macrons' => $macrons,
                'microns' => $microns,
                'colons' => $colons,
                'resens' => $resens,
                'macrototal' => $macrototal,
                'micrototal' => $micrototal,
                'colototal' => $colototal,
                'resetotal' => $resetotal,
                'tarmacro' => $tarmacro,
                'tarmicro' => $tarmicro,
                'tarcolo' => $tarcolo,
                'tarres' => $tarres,
                'summacro' => $summacro,
                'summicro' => $summicro,
                'sumcolo' => $sumcolo,
                'sumrese' => $sumrese,
                'cumtarmacro' => $cumtarmacro,
                'cumtarmicro' => $cumtarmicro,
                'cumtarcolo' => $cumtarcolo,
                'cumtarres' => $cumtarres,
                'cumcomacro' => $cumcomacro,
                'cumcomicro' => $cumcomicro,
                'cumcocolo'  => $cumcocolo,
                'cumcores'  => $cumcores,
                'cumnsmacro' => $cumnsmacro,
                'cumnsmicro' => $cumnsmicro,
                'cumnscolo'  => $cumnscolo,
                'cumnsres'  => $cumnsres,
                'telkomsel' => $telkomsel,
                'telkom' => $telkom,
                'isat' => $isat,
                'xl' => $xl,
                'h3i' => $h3i,
                'sf' => $sf,
                'sti' => $sti,
                'oth' => $oth,
                'totalchart' => $totalchart,
                'telkomsela' => $telkomsela,
                'telkoma' => $telkoma,
                'isata' => $isata,
                'xla' => $xla,
                'h3ia' => $h3ia,
                'sfa' => $sfa,
                'stia' => $stia,
                'otha' => $otha,
                'totalcharta' => $totalcharta,
                'hasilcek' => $hasilcek,
                'totsalepro' => $totsalepro,
                'tarsalepro' => $tarsalepro,
                'reasalepro' => $reasalepro,
                'conowpro' => $conowpro,
                'cobefpro' => $cobefpro,
                'nsnowpro' => $nsnowpro,
                'nsbefpro' => $nsbefpro,
                'macronowpro' => $macronowpro,
                'macrobefpro' => $macrobefpro,
                'micronowpro' => $micronowpro,
                'microbefpro' => $microbefpro,
                'colonowpro' => $colonowpro,
                'colobefpro' => $colobefpro,
                'resenowpro' => $resenowpro,
                'resebefpro' => $resebefpro,
                'a1nowpro' => $a1nowpro,
                'a1befpro' => $a1befpro,
                'a2nowpro' => $a2nowpro,
                'a2befpro' => $a2befpro,
                'a3nowpro' => $a3nowpro,
                'a3befpro' => $a3befpro,
                'a4nowpro' => $a4nowpro,
                'a4befpro' => $a4befpro,
                'hasiltotsale' => $hasiltotsale,
                'hasiltarsale' => $hasiltarsale,
                'hasilreasale' => $hasilreasale,
                'hasilconow' => $hasilconow,
                'hasilcobef' => $hasilcobef,
                'hasilnsnow' => $hasilnsnow,
                'hasilnsbef' => $hasilnsbef,
                'hasilmacronow' => $hasilmacronow,
                'hasilmacrobef' => $hasilmacrobef,
                'hasilmicronow' => $hasilmicronow,
                'hasilmicrobef' => $hasilmicrobef,
                'hasilcolonow' => $hasilcolonow,
                'hasilcolobef' => $hasilcolobef,
                'hasilresenow' => $hasilresenow,
                'hasilresebef' => $hasilresebef,
                'hasila1now' => $hasila1now,
                'hasila1bef' => $hasila1bef,
                'hasila2now' => $hasila2now,
                'hasila2bef' => $hasila2bef,
                'hasila3now' => $hasila3now,
                'hasila3bef' => $hasila3bef,
                'hasila4now' => $hasila4now,
                'hasila4bef' => $hasila4bef,
            ]);
        }elseif($tahun == 2021 && $bulan !='no' && $pekan =='W1'){
            $halaman = 1;
            $cek = Datasales2022::where('bulan', $bulan)->get();
            $hasilcek = $cek->isEmpty();
            $loop = Carbon::parse('1'.$bulan)->month;
            // Variabel Array
            $labelbln = [];
            $totsale = [];
            $tarsale = [];
            $reasale = [];
            $conow = [];
            $cobef = [];
            $nsnow = [];
            $nsbef = [];
            $macronow = [];
            $macrobef = [];
            $micronow = [];
            $microbef = [];
            $colonow = [];
            $colobef = [];
            $resenow = [];
            $resebef = [];
            $a1now = [];
            $a1bef = [];
            $a2now = [];
            $a2bef = [];
            $a3now = [];
            $a3bef = [];
            $a4now = [];
            $a4bef = [];
            // Variabel Loop
            $summacro = 0;
            $summicro = 0;
            $sumcolo = 0;
            $sumrese = 0;
            $cumtarmacro = 0;
            $cumtarmicro = 0;
            $cumtarcolo = 0;
            $cumtarres = 0;
            $totsalepro = 0;
            $tarsalepro = 0;
            $reasalepro = 0;
            $conowpro = 0;
            $cobefpro = 0;
            $nsnowpro = 0;
            $nsbefpro = 0;
            $macronowpro = 0;
            $macrobefpro = 0;
            $micronowpro = 0;
            $microbefpro = 0;
            $colonowpro = 0;
            $colobefpro = 0;
            $resenowpro = 0;
            $resebefpro = 0;
            $a1nowpro = 0;
            $a1befpro = 0;
            $a2nowpro = 0;
            $a2befpro = 0;
            $a3nowpro = 0;
            $a3befpro = 0;
            $a4nowpro = 0;
            $a4befpro = 0;
            $cumcomacro =  0;
            $cumcomicro = 0;
            $cumcocolo = 0;
            $cumcores = 0;
            $cumnsmacro =  0;
            $cumnsmicro = 0;
            $cumnscolo = 0;
            $cumnsres = 0;
            // Variabel Chart
            $telkomsel=0;
            $telkom=0;
            $isat=0;
            $xl=0;
            $h3i=0;
            $sf=0;
            $sti=0;
            $oth=0;
            $telkomsela=0;
            $telkoma=0;
            $isata=0;
            $xla=0;
            $h3ia=0;
            $sfa=0;
            $stia=0;
            $otha = 0;
            for ($i=1;$i<=$loop;$i++){
                $summacro = $summacro + Hasilsales2020::where('sow', 'macro')->sum($i);
                $summicro = $summicro + Hasilsales2020::where('sow', 'micro')->sum($i);
                $sumcolo = $sumcolo + Hasilsales2020::where('sow', 'colo')->sum($i);
                $sumrese = $sumrese + Hasilsales2020::where('sow', 'reseller')->sum($i);
                $cumtarmacro = $cumtarmacro + Targetsales2021::where('sow', 'Macro')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarmicro = $cumtarmicro + Targetsales2021::where('sow', 'Micro')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarcolo = $cumtarcolo + Targetsales2021::where('sow', 'Colo')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarres = $cumtarres + Targetsales2021::where('sow', 'Reseller')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $totsalepro = Datasales2021::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $tarsalepro = Targetsales2021::select(Carbon::create()->day(1)->month($i)->format('M'))->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $reasalepro = Datasales2020::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $conowpro = Datasales2021::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cobefpro = Datasales2020::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsnowpro = Datasales2021::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsbefpro = Datasales2020::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $macronowpro = Datasales2021::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $macrobefpro = Datasales2020::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $micronowpro = Datasales2021::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $microbefpro = Datasales2020::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colonowpro = Datasales2021::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colobefpro = Datasales2020::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resenowpro = Datasales2021::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resebefpro = Datasales2020::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1nowpro = Datasales2021::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1befpro = Datasales2020::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2nowpro = Datasales2021::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2befpro = Datasales2020::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3nowpro = Datasales2021::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3befpro = Datasales2020::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4nowpro = Datasales2021::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4befpro = Datasales2020::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $bulansales = Carbon::create()->day(1)->month($i)->format('M');
                $cumcomacro =  $cumcomacro + Datasales2021::where('kategori', 'Carry Over')->where('sow','Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcomicro = $cumcomicro + Datasales2021::where('kategori', 'Carry Over')->where('sow','Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcocolo = $cumcocolo + Datasales2021::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcores = $cumcores + Datasales2021::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsmacro =  $cumnsmacro + Datasales2021::where('kategori', 'New Sales')->where('sow','Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsmicro = $cumnsmicro + Datasales2021::where('kategori', 'New Sales')->where('sow','Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnscolo = $cumnscolo + Datasales2021::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsres = $cumnsres + Datasales2021::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsel = $telkomsel + Datasales2021::where('tenant', 'TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkom = $telkom + Datasales2021::where('tenant', 'TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isat = $isat + Datasales2021::where('tenant', 'ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xl = $xl + Datasales2021::where('tenant', 'XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3i = $h3i + Datasales2021::where('tenant', 'H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sf = $sf + Datasales2021::where('tenant', 'SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sti = $sti + Datasales2021::where('tenant', 'STI')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $oth = $oth + Datasales2021::where('tenant', 'Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsela = $telkomsela + Datasales2021::where('kategori', 'New Sales')->where('tenant','TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkoma = $telkoma + Datasales2021::where('kategori', 'New Sales')->where('tenant','TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isata = $isata + Datasales2021::where('kategori', 'New Sales')->where('tenant','ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xla = $xla + Datasales2021::where('kategori', 'New Sales')->where('tenant','XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3ia = $h3ia + Datasales2021::where('kategori', 'New Sales')->where('tenant','H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sfa = $sfa + Datasales2021::where('kategori', 'New Sales')->where('tenant','SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $stia = $stia + Datasales2021::where('kategori', 'New Sales')->where('tenant','STI')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $otha = $otha + Datasales2021::where('kategori', 'New Sales')->where('tenant','Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                array_push($labelbln, $bulansales);
                array_push($totsale, $totsalepro);
                array_push($tarsale, $tarsalepro);
                array_push($reasale, $reasalepro);
                array_push($conow, $conowpro);
                array_push($cobef, $cobefpro);
                array_push($nsnow, $nsnowpro);
                array_push($nsbef, $nsbefpro);
                array_push($macronow, $macronowpro);
                array_push($macrobef, $macrobefpro);
                array_push($micronow, $micronowpro);
                array_push($microbef, $microbefpro);
                array_push($colonow,$colonowpro);
                array_push($colobef,$colobefpro);
                array_push($resenow,$resenowpro);
                array_push($resebef,$resebefpro);
                array_push($a1now,$a1nowpro);
                array_push($a1bef,$a1befpro);
                array_push($a2now,$a2nowpro);
                array_push($a2bef,$a2befpro);
                array_push($a3now,$a3nowpro);
                array_push($a3bef,$a3befpro);
                array_push($a4now,$a4nowpro);
                array_push($a4bef,$a4befpro);
            }
            $labelbulan = json_encode($labelbln);
            $hasiltotsale = json_encode($totsale);
            $hasiltarsale = json_encode($tarsale);
            $hasilreasale = json_encode($reasale);
            $hasilconow = json_encode($conow);
            $hasilcobef = json_encode($cobef);
            $hasilnsnow = json_encode($nsnow);
            $hasilnsbef = json_encode($nsbef);
            $hasilmacronow = json_encode($macronow);
            $hasilmacrobef = json_encode($macrobef);
            $hasilmicronow = json_encode($micronow);
            $hasilmicrobef = json_encode($microbef);
            $hasilcolonow = json_encode($colonow);
            $hasilcolobef = json_encode($colobef);
            $hasilresenow = json_encode($resenow);
            $hasilresebef = json_encode($resebef);
            $hasila1now = json_encode($a1now);
            $hasila1bef = json_encode($a1bef);
            $hasila2now = json_encode($a2now);
            $hasila2bef = json_encode($a2bef);
            $hasila3now = json_encode($a3now);
            $hasila3bef = json_encode($a3bef);
            $hasila4now = json_encode($a4now);
            $hasila4bef = json_encode($a4bef);
            $tarmacro = Targetsales2021::select($bulan)->where('sow', 'Macro')->sum($bulan);
            $tarmicro = Targetsales2021::select($bulan)->where('sow', 'Micro')->sum($bulan);
            $tarcolo = Targetsales2021::select($bulan)->where('sow', 'Colo')->sum($bulan);
            $tarres = Targetsales2021::select($bulan)->where('sow', 'Reseller')->sum($bulan);
            $macroco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Macro')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $microco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Micro')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $coloco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Colo')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $reseco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Reseller')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $macrons = Datasales2021::where('kategori', 'New Sales')->where('sow','Macro')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $microns = Datasales2021::where('kategori', 'New Sales')->where('sow','Micro')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $colons = Datasales2021::where('kategori', 'New Sales')->where('sow','Colo')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $resens = Datasales2021::where('kategori', 'New Sales')->where('sow','Reseller')->where('achsales', 'like', "%" . $pekansales . "%")->count();
            $macrototal = $macroco + $macrons;
            $micrototal = $microco + $microns;
            $colototal = $coloco + $colons;
            $resetotal = $reseco + $resens;
            $totalchart = $telkomsel + $telkom + $isat + $xl + $h3i + $sf + $sti + $oth;
            $totalcharta = $telkomsela + $telkoma + $isata + $xla + $h3ia + $sfa + $stia + $otha;
            
            return view('sales.salesreport', [
                'halaman' => $halaman,
                'labelbulan' => $labelbulan,
                'tahun' => $tahun,
                'bulan' => $bulan,
                'pekan' => $pekan,
                'macroco' => $macroco,
                'microco' => $microco,
                'coloco' => $coloco,
                'reseco' => $reseco,
                'macrons' => $macrons,
                'microns' => $microns,
                'colons' => $colons,
                'resens' => $resens,
                'macrototal' => $macrototal,
                'micrototal' => $micrototal,
                'colototal' => $colototal,
                'resetotal' => $resetotal,
                'tarmacro' => $tarmacro,
                'tarmicro' => $tarmicro,
                'tarcolo' => $tarcolo,
                'tarres' => $tarres,
                'summacro' => $summacro,
                'summicro' => $summicro,
                'sumcolo' => $sumcolo,
                'sumrese' => $sumrese,
                'cumtarmacro' => $cumtarmacro,
                'cumtarmicro' => $cumtarmicro,
                'cumtarcolo' => $cumtarcolo,
                'cumtarres' => $cumtarres,
                'cumcomacro' => $cumcomacro,
                'cumcomicro' => $cumcomicro,
                'cumcocolo'  => $cumcocolo,
                'cumcores'  => $cumcores,
                'cumnsmacro' => $cumnsmacro,
                'cumnsmicro' => $cumnsmicro,
                'cumnscolo'  => $cumnscolo,
                'cumnsres'  => $cumnsres,
                'telkomsel' => $telkomsel,
                'telkom' => $telkom,
                'isat' => $isat,
                'xl' => $xl,
                'h3i' => $h3i,
                'sf' => $sf,
                'sti' => $sti,
                'oth' => $oth,
                'totalchart' => $totalchart,
                'telkomsela' => $telkomsela,
                'telkoma' => $telkoma,
                'isata' => $isata,
                'xla' => $xla,
                'h3ia' => $h3ia,
                'sfa' => $sfa,
                'stia' => $stia,
                'otha' => $otha,
                'totalcharta' => $totalcharta,
                'hasilcek' => $hasilcek,
                'totsalepro' => $totsalepro,
                'tarsalepro' => $tarsalepro,
                'reasalepro' => $reasalepro,
                'conowpro' => $conowpro,
                'cobefpro' => $cobefpro,
                'nsnowpro' => $nsnowpro,
                'nsbefpro' => $nsbefpro,
                'macronowpro' => $macronowpro,
                'macrobefpro' => $macrobefpro,
                'micronowpro' => $micronowpro,
                'microbefpro' => $microbefpro,
                'colonowpro' => $colonowpro,
                'colobefpro' => $colobefpro,
                'resenowpro' => $resenowpro,
                'resebefpro' => $resebefpro,
                'a1nowpro' => $a1nowpro,
                'a1befpro' => $a1befpro,
                'a2nowpro' => $a2nowpro,
                'a2befpro' => $a2befpro,
                'a3nowpro' => $a3nowpro,
                'a3befpro' => $a3befpro,
                'a4nowpro' => $a4nowpro,
                'a4befpro' => $a4befpro,
                'hasiltotsale' => $hasiltotsale,
                'hasiltarsale' => $hasiltarsale,
                'hasilreasale' => $hasilreasale,
                'hasilconow' => $hasilconow,
                'hasilcobef' => $hasilcobef,
                'hasilnsnow' => $hasilnsnow,
                'hasilnsbef' => $hasilnsbef,
                'hasilmacronow' => $hasilmacronow,
                'hasilmacrobef' => $hasilmacrobef,
                'hasilmicronow' => $hasilmicronow,
                'hasilmicrobef' => $hasilmicrobef,
                'hasilcolonow' => $hasilcolonow,
                'hasilcolobef' => $hasilcolobef,
                'hasilresenow' => $hasilresenow,
                'hasilresebef' => $hasilresebef,
                'hasila1now' => $hasila1now,
                'hasila1bef' => $hasila1bef,
                'hasila2now' => $hasila2now,
                'hasila2bef' => $hasila2bef,
                'hasila3now' => $hasila3now,
                'hasila3bef' => $hasila3bef,
                'hasila4now' => $hasila4now,
                'hasila4bef' => $hasila4bef,
            ]);
        }elseif($tahun == 2021 && $bulan !='no' && $pekan =='W2'){
            $halaman = 1;
            $cek = Datasales2022::where('bulan', $bulan)->get();
            $hasilcek = $cek->isEmpty();
            $loop = Carbon::parse('1'.$bulan)->month;
            // Variabel Array
            $labelbln = [];
            $totsale = [];
            $tarsale = [];
            $reasale = [];
            $conow = [];
            $cobef = [];
            $nsnow = [];
            $nsbef = [];
            $macronow = [];
            $macrobef = [];
            $micronow = [];
            $microbef = [];
            $colonow = [];
            $colobef = [];
            $resenow = [];
            $resebef = [];
            $a1now = [];
            $a1bef = [];
            $a2now = [];
            $a2bef = [];
            $a3now = [];
            $a3bef = [];
            $a4now = [];
            $a4bef = [];
            // Variabel Loop
            $summacro = 0;
            $summicro = 0;
            $sumcolo = 0;
            $sumrese = 0;
            $cumtarmacro = 0;
            $cumtarmicro = 0;
            $cumtarcolo = 0;
            $cumtarres = 0;
            $totsalepro = 0;
            $tarsalepro = 0;
            $reasalepro = 0;
            $conowpro = 0;
            $cobefpro = 0;
            $nsnowpro = 0;
            $nsbefpro = 0;
            $macronowpro = 0;
            $macrobefpro = 0;
            $micronowpro = 0;
            $microbefpro = 0;
            $colonowpro = 0;
            $colobefpro = 0;
            $resenowpro = 0;
            $resebefpro = 0;
            $a1nowpro = 0;
            $a1befpro = 0;
            $a2nowpro = 0;
            $a2befpro = 0;
            $a3nowpro = 0;
            $a3befpro = 0;
            $a4nowpro = 0;
            $a4befpro = 0;
            $cumcomacro =  0;
            $cumcomicro = 0;
            $cumcocolo = 0;
            $cumcores = 0;
            $cumnsmacro =  0;
            $cumnsmicro = 0;
            $cumnscolo = 0;
            $cumnsres = 0;
            // Variabel Chart
            $telkomsel=0;
            $telkom=0;
            $isat=0;
            $xl=0;
            $h3i=0;
            $sf=0;
            $sti=0;
            $oth=0;
            $telkomsela=0;
            $telkoma=0;
            $isata=0;
            $xla=0;
            $h3ia=0;
            $sfa=0;
            $stia=0;
            $otha = 0;
            for ($i=1;$i<=$loop;$i++){
                $summacro = $summacro + Hasilsales2020::where('sow', 'macro')->sum($i);
                $summicro = $summicro + Hasilsales2020::where('sow', 'micro')->sum($i);
                $sumcolo = $sumcolo + Hasilsales2020::where('sow', 'colo')->sum($i);
                $sumrese = $sumrese + Hasilsales2020::where('sow', 'reseller')->sum($i);
                $cumtarmacro = $cumtarmacro + Targetsales2021::where('sow', 'Macro')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarmicro = $cumtarmicro + Targetsales2021::where('sow', 'Micro')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarcolo = $cumtarcolo + Targetsales2021::where('sow', 'Colo')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarres = $cumtarres + Targetsales2021::where('sow', 'Reseller')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $totsalepro = Datasales2021::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $tarsalepro = Targetsales2021::select(Carbon::create()->day(1)->month($i)->format('M'))->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $reasalepro = Datasales2020::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $conowpro = Datasales2021::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cobefpro = Datasales2020::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsnowpro = Datasales2021::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsbefpro = Datasales2020::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $macronowpro = Datasales2021::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $macrobefpro = Datasales2020::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $micronowpro = Datasales2021::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $microbefpro = Datasales2020::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colonowpro = Datasales2021::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colobefpro = Datasales2020::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resenowpro = Datasales2021::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resebefpro = Datasales2020::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1nowpro = Datasales2021::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1befpro = Datasales2020::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2nowpro = Datasales2021::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2befpro = Datasales2020::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3nowpro = Datasales2021::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3befpro = Datasales2020::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4nowpro = Datasales2021::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4befpro = Datasales2020::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $bulansales = Carbon::create()->day(1)->month($i)->format('M');
                $cumcomacro =  $cumcomacro + Datasales2021::where('kategori', 'Carry Over')->where('sow','Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcomicro = $cumcomicro + Datasales2021::where('kategori', 'Carry Over')->where('sow','Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcocolo = $cumcocolo + Datasales2021::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcores = $cumcores + Datasales2021::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsmacro =  $cumnsmacro + Datasales2021::where('kategori', 'New Sales')->where('sow','Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsmicro = $cumnsmicro + Datasales2021::where('kategori', 'New Sales')->where('sow','Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnscolo = $cumnscolo + Datasales2021::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsres = $cumnsres + Datasales2021::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsel = $telkomsel + Datasales2021::where('tenant', 'TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkom = $telkom + Datasales2021::where('tenant', 'TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isat = $isat + Datasales2021::where('tenant', 'ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xl = $xl + Datasales2021::where('tenant', 'XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3i = $h3i + Datasales2021::where('tenant', 'H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sf = $sf + Datasales2021::where('tenant', 'SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sti = $sti + Datasales2021::where('tenant', 'STI')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $oth = $oth + Datasales2021::where('tenant', 'Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsela = $telkomsela + Datasales2021::where('kategori', 'New Sales')->where('tenant','TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkoma = $telkoma + Datasales2021::where('kategori', 'New Sales')->where('tenant','TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isata = $isata + Datasales2021::where('kategori', 'New Sales')->where('tenant','ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xla = $xla + Datasales2021::where('kategori', 'New Sales')->where('tenant','XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3ia = $h3ia + Datasales2021::where('kategori', 'New Sales')->where('tenant','H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sfa = $sfa + Datasales2021::where('kategori', 'New Sales')->where('tenant','SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $stia = $stia + Datasales2021::where('kategori', 'New Sales')->where('tenant','STI')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $otha = $otha + Datasales2021::where('kategori', 'New Sales')->where('tenant','Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                array_push($labelbln, $bulansales);
                array_push($totsale, $totsalepro);
                array_push($tarsale, $tarsalepro);
                array_push($reasale, $reasalepro);
                array_push($conow, $conowpro);
                array_push($cobef, $cobefpro);
                array_push($nsnow, $nsnowpro);
                array_push($nsbef, $nsbefpro);
                array_push($macronow, $macronowpro);
                array_push($macrobef, $macrobefpro);
                array_push($micronow, $micronowpro);
                array_push($microbef, $microbefpro);
                array_push($colonow,$colonowpro);
                array_push($colobef,$colobefpro);
                array_push($resenow,$resenowpro);
                array_push($resebef,$resebefpro);
                array_push($a1now,$a1nowpro);
                array_push($a1bef,$a1befpro);
                array_push($a2now,$a2nowpro);
                array_push($a2bef,$a2befpro);
                array_push($a3now,$a3nowpro);
                array_push($a3bef,$a3befpro);
                array_push($a4now,$a4nowpro);
                array_push($a4bef,$a4befpro);
            }
            $labelbulan = json_encode($labelbln);
            $hasiltotsale = json_encode($totsale);
            $hasiltarsale = json_encode($tarsale);
            $hasilreasale = json_encode($reasale);
            $hasilconow = json_encode($conow);
            $hasilcobef = json_encode($cobef);
            $hasilnsnow = json_encode($nsnow);
            $hasilnsbef = json_encode($nsbef);
            $hasilmacronow = json_encode($macronow);
            $hasilmacrobef = json_encode($macrobef);
            $hasilmicronow = json_encode($micronow);
            $hasilmicrobef = json_encode($microbef);
            $hasilcolonow = json_encode($colonow);
            $hasilcolobef = json_encode($colobef);
            $hasilresenow = json_encode($resenow);
            $hasilresebef = json_encode($resebef);
            $hasila1now = json_encode($a1now);
            $hasila1bef = json_encode($a1bef);
            $hasila2now = json_encode($a2now);
            $hasila2bef = json_encode($a2bef);
            $hasila3now = json_encode($a3now);
            $hasila3bef = json_encode($a3bef);
            $hasila4now = json_encode($a4now);
            $hasila4bef = json_encode($a4bef);
            $tarmacro = Targetsales2021::select($bulan)->where('sow', 'Macro')->sum($bulan);
            $tarmicro = Targetsales2021::select($bulan)->where('sow', 'Micro')->sum($bulan);
            $tarcolo = Targetsales2021::select($bulan)->where('sow', 'Colo')->sum($bulan);
            $tarres = Targetsales2021::select($bulan)->where('sow', 'Reseller')->sum($bulan);
            $macroco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Macro')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Macro')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $microco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Micro')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Micro')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $coloco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Colo')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $reseco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Reseller')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $macrons = Datasales2021::where('kategori', 'New Sales')->where('sow','Macro')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Macro')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $microns = Datasales2021::where('kategori', 'New Sales')->where('sow','Micro')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Micro')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $colons = Datasales2021::where('kategori', 'New Sales')->where('sow','Colo')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $resens = Datasales2021::where('kategori', 'New Sales')->where('sow','Reseller')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count();
            $macrototal = $macroco + $macrons;
            $micrototal = $microco + $microns;
            $colototal = $coloco + $colons;
            $resetotal = $reseco + $resens;
            $totalchart = $telkomsel + $telkom + $isat + $xl + $h3i + $sf + $sti + $oth;
            $totalcharta = $telkomsela + $telkoma + $isata + $xla + $h3ia + $sfa + $stia + $otha;
            
            return view('sales.salesreport', [
                'halaman' => $halaman,
                'labelbulan' => $labelbulan,
                'tahun' => $tahun,
                'bulan' => $bulan,
                'pekan' => $pekan,
                'macroco' => $macroco,
                'microco' => $microco,
                'coloco' => $coloco,
                'reseco' => $reseco,
                'macrons' => $macrons,
                'microns' => $microns,
                'colons' => $colons,
                'resens' => $resens,
                'macrototal' => $macrototal,
                'micrototal' => $micrototal,
                'colototal' => $colototal,
                'resetotal' => $resetotal,
                'tarmacro' => $tarmacro,
                'tarmicro' => $tarmicro,
                'tarcolo' => $tarcolo,
                'tarres' => $tarres,
                'summacro' => $summacro,
                'summicro' => $summicro,
                'sumcolo' => $sumcolo,
                'sumrese' => $sumrese,
                'cumtarmacro' => $cumtarmacro,
                'cumtarmicro' => $cumtarmicro,
                'cumtarcolo' => $cumtarcolo,
                'cumtarres' => $cumtarres,
                'cumcomacro' => $cumcomacro,
                'cumcomicro' => $cumcomicro,
                'cumcocolo'  => $cumcocolo,
                'cumcores'  => $cumcores,
                'cumnsmacro' => $cumnsmacro,
                'cumnsmicro' => $cumnsmicro,
                'cumnscolo'  => $cumnscolo,
                'cumnsres'  => $cumnsres,
                'telkomsel' => $telkomsel,
                'telkom' => $telkom,
                'isat' => $isat,
                'xl' => $xl,
                'h3i' => $h3i,
                'sf' => $sf,
                'sti' => $sti,
                'oth' => $oth,
                'totalchart' => $totalchart,
                'telkomsela' => $telkomsela,
                'telkoma' => $telkoma,
                'isata' => $isata,
                'xla' => $xla,
                'h3ia' => $h3ia,
                'sfa' => $sfa,
                'stia' => $stia,
                'otha' => $otha,
                'totalcharta' => $totalcharta,
                'hasilcek' => $hasilcek,
                'totsalepro' => $totsalepro,
                'tarsalepro' => $tarsalepro,
                'reasalepro' => $reasalepro,
                'conowpro' => $conowpro,
                'cobefpro' => $cobefpro,
                'nsnowpro' => $nsnowpro,
                'nsbefpro' => $nsbefpro,
                'macronowpro' => $macronowpro,
                'macrobefpro' => $macrobefpro,
                'micronowpro' => $micronowpro,
                'microbefpro' => $microbefpro,
                'colonowpro' => $colonowpro,
                'colobefpro' => $colobefpro,
                'resenowpro' => $resenowpro,
                'resebefpro' => $resebefpro,
                'a1nowpro' => $a1nowpro,
                'a1befpro' => $a1befpro,
                'a2nowpro' => $a2nowpro,
                'a2befpro' => $a2befpro,
                'a3nowpro' => $a3nowpro,
                'a3befpro' => $a3befpro,
                'a4nowpro' => $a4nowpro,
                'a4befpro' => $a4befpro,
                'hasiltotsale' => $hasiltotsale,
                'hasiltarsale' => $hasiltarsale,
                'hasilreasale' => $hasilreasale,
                'hasilconow' => $hasilconow,
                'hasilcobef' => $hasilcobef,
                'hasilnsnow' => $hasilnsnow,
                'hasilnsbef' => $hasilnsbef,
                'hasilmacronow' => $hasilmacronow,
                'hasilmacrobef' => $hasilmacrobef,
                'hasilmicronow' => $hasilmicronow,
                'hasilmicrobef' => $hasilmicrobef,
                'hasilcolonow' => $hasilcolonow,
                'hasilcolobef' => $hasilcolobef,
                'hasilresenow' => $hasilresenow,
                'hasilresebef' => $hasilresebef,
                'hasila1now' => $hasila1now,
                'hasila1bef' => $hasila1bef,
                'hasila2now' => $hasila2now,
                'hasila2bef' => $hasila2bef,
                'hasila3now' => $hasila3now,
                'hasila3bef' => $hasila3bef,
                'hasila4now' => $hasila4now,
                'hasila4bef' => $hasila4bef,
            ]);
        }elseif($tahun == 2021 && $bulan !='no' && $pekan =='W3'){
            $halaman = 1;
            $cek = Datasales2022::where('bulan', $bulan)->get();
            $hasilcek = $cek->isEmpty();
            $loop = Carbon::parse('1'.$bulan)->month;
            // Variabel Array
            $labelbln = [];
            $totsale = [];
            $tarsale = [];
            $reasale = [];
            $conow = [];
            $cobef = [];
            $nsnow = [];
            $nsbef = [];
            $macronow = [];
            $macrobef = [];
            $micronow = [];
            $microbef = [];
            $colonow = [];
            $colobef = [];
            $resenow = [];
            $resebef = [];
            $a1now = [];
            $a1bef = [];
            $a2now = [];
            $a2bef = [];
            $a3now = [];
            $a3bef = [];
            $a4now = [];
            $a4bef = [];
            // Variabel Loop
            $summacro = 0;
            $summicro = 0;
            $sumcolo = 0;
            $sumrese = 0;
            $cumtarmacro = 0;
            $cumtarmicro = 0;
            $cumtarcolo = 0;
            $cumtarres = 0;
            $totsalepro = 0;
            $tarsalepro = 0;
            $reasalepro = 0;
            $conowpro = 0;
            $cobefpro = 0;
            $nsnowpro = 0;
            $nsbefpro = 0;
            $macronowpro = 0;
            $macrobefpro = 0;
            $micronowpro = 0;
            $microbefpro = 0;
            $colonowpro = 0;
            $colobefpro = 0;
            $resenowpro = 0;
            $resebefpro = 0;
            $a1nowpro = 0;
            $a1befpro = 0;
            $a2nowpro = 0;
            $a2befpro = 0;
            $a3nowpro = 0;
            $a3befpro = 0;
            $a4nowpro = 0;
            $a4befpro = 0;
            $cumcomacro =  0;
            $cumcomicro = 0;
            $cumcocolo = 0;
            $cumcores = 0;
            $cumnsmacro =  0;
            $cumnsmicro = 0;
            $cumnscolo = 0;
            $cumnsres = 0;
            // Variabel Chart
            $telkomsel=0;
            $telkom=0;
            $isat=0;
            $xl=0;
            $h3i=0;
            $sf=0;
            $sti=0;
            $oth=0;
            $telkomsela=0;
            $telkoma=0;
            $isata=0;
            $xla=0;
            $h3ia=0;
            $sfa=0;
            $stia=0;
            $otha = 0;
            for ($i=1;$i<=$loop;$i++){
                $summacro = $summacro + Hasilsales2020::where('sow', 'macro')->sum($i);
                $summicro = $summicro + Hasilsales2020::where('sow', 'micro')->sum($i);
                $sumcolo = $sumcolo + Hasilsales2020::where('sow', 'colo')->sum($i);
                $sumrese = $sumrese + Hasilsales2020::where('sow', 'reseller')->sum($i);
                $cumtarmacro = $cumtarmacro + Targetsales2021::where('sow', 'Macro')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarmicro = $cumtarmicro + Targetsales2021::where('sow', 'Micro')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarcolo = $cumtarcolo + Targetsales2021::where('sow', 'Colo')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarres = $cumtarres + Targetsales2021::where('sow', 'Reseller')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $totsalepro = Datasales2021::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $tarsalepro = Targetsales2021::select(Carbon::create()->day(1)->month($i)->format('M'))->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $reasalepro = Datasales2020::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $conowpro = Datasales2021::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cobefpro = Datasales2020::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsnowpro = Datasales2021::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsbefpro = Datasales2020::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $macronowpro = Datasales2021::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $macrobefpro = Datasales2020::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $micronowpro = Datasales2021::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $microbefpro = Datasales2020::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colonowpro = Datasales2021::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colobefpro = Datasales2020::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resenowpro = Datasales2021::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resebefpro = Datasales2020::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1nowpro = Datasales2021::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1befpro = Datasales2020::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2nowpro = Datasales2021::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2befpro = Datasales2020::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3nowpro = Datasales2021::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3befpro = Datasales2020::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4nowpro = Datasales2021::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4befpro = Datasales2020::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $bulansales = Carbon::create()->day(1)->month($i)->format('M');
                $cumcomacro =  $cumcomacro + Datasales2021::where('kategori', 'Carry Over')->where('sow','Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcomicro = $cumcomicro + Datasales2021::where('kategori', 'Carry Over')->where('sow','Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcocolo = $cumcocolo + Datasales2021::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcores = $cumcores + Datasales2021::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsmacro =  $cumnsmacro + Datasales2021::where('kategori', 'New Sales')->where('sow','Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsmicro = $cumnsmicro + Datasales2021::where('kategori', 'New Sales')->where('sow','Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnscolo = $cumnscolo + Datasales2021::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsres = $cumnsres + Datasales2021::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsel = $telkomsel + Datasales2021::where('tenant', 'TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkom = $telkom + Datasales2021::where('tenant', 'TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isat = $isat + Datasales2021::where('tenant', 'ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xl = $xl + Datasales2021::where('tenant', 'XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3i = $h3i + Datasales2021::where('tenant', 'H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sf = $sf + Datasales2021::where('tenant', 'SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sti = $sti + Datasales2021::where('tenant', 'STI')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $oth = $oth + Datasales2021::where('tenant', 'Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsela = $telkomsela + Datasales2021::where('kategori', 'New Sales')->where('tenant','TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkoma = $telkoma + Datasales2021::where('kategori', 'New Sales')->where('tenant','TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isata = $isata + Datasales2021::where('kategori', 'New Sales')->where('tenant','ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xla = $xla + Datasales2021::where('kategori', 'New Sales')->where('tenant','XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3ia = $h3ia + Datasales2021::where('kategori', 'New Sales')->where('tenant','H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sfa = $sfa + Datasales2021::where('kategori', 'New Sales')->where('tenant','SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $stia = $stia + Datasales2021::where('kategori', 'New Sales')->where('tenant','STI')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $otha = $otha + Datasales2021::where('kategori', 'New Sales')->where('tenant','Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                array_push($labelbln, $bulansales);
                array_push($totsale, $totsalepro);
                array_push($tarsale, $tarsalepro);
                array_push($reasale, $reasalepro);
                array_push($conow, $conowpro);
                array_push($cobef, $cobefpro);
                array_push($nsnow, $nsnowpro);
                array_push($nsbef, $nsbefpro);
                array_push($macronow, $macronowpro);
                array_push($macrobef, $macrobefpro);
                array_push($micronow, $micronowpro);
                array_push($microbef, $microbefpro);
                array_push($colonow,$colonowpro);
                array_push($colobef,$colobefpro);
                array_push($resenow,$resenowpro);
                array_push($resebef,$resebefpro);
                array_push($a1now,$a1nowpro);
                array_push($a1bef,$a1befpro);
                array_push($a2now,$a2nowpro);
                array_push($a2bef,$a2befpro);
                array_push($a3now,$a3nowpro);
                array_push($a3bef,$a3befpro);
                array_push($a4now,$a4nowpro);
                array_push($a4bef,$a4befpro);
            }
            $labelbulan = json_encode($labelbln);
            $hasiltotsale = json_encode($totsale);
            $hasiltarsale = json_encode($tarsale);
            $hasilreasale = json_encode($reasale);
            $hasilconow = json_encode($conow);
            $hasilcobef = json_encode($cobef);
            $hasilnsnow = json_encode($nsnow);
            $hasilnsbef = json_encode($nsbef);
            $hasilmacronow = json_encode($macronow);
            $hasilmacrobef = json_encode($macrobef);
            $hasilmicronow = json_encode($micronow);
            $hasilmicrobef = json_encode($microbef);
            $hasilcolonow = json_encode($colonow);
            $hasilcolobef = json_encode($colobef);
            $hasilresenow = json_encode($resenow);
            $hasilresebef = json_encode($resebef);
            $hasila1now = json_encode($a1now);
            $hasila1bef = json_encode($a1bef);
            $hasila2now = json_encode($a2now);
            $hasila2bef = json_encode($a2bef);
            $hasila3now = json_encode($a3now);
            $hasila3bef = json_encode($a3bef);
            $hasila4now = json_encode($a4now);
            $hasila4bef = json_encode($a4bef);
            $tarmacro = Targetsales2021::select($bulan)->where('sow', 'Macro')->sum($bulan);
            $tarmicro = Targetsales2021::select($bulan)->where('sow', 'Micro')->sum($bulan);
            $tarcolo = Targetsales2021::select($bulan)->where('sow', 'Colo')->sum($bulan);
            $tarres = Targetsales2021::select($bulan)->where('sow', 'Reseller')->sum($bulan);
            $macroco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Macro')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Macro')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Macro')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $microco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Micro')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Micro')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Micro')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $coloco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Colo')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Colo')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $reseco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Reseller')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'Carry Over')->where('sow','Reseller')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $macrons = Datasales2021::where('kategori', 'New Sales')->where('sow','Macro')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Macro')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Macro')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $microns = Datasales2021::where('kategori', 'New Sales')->where('sow','Micro')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Micro')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Micro')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $colons = Datasales2021::where('kategori', 'New Sales')->where('sow','Colo')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Colo')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $resens = Datasales2021::where('kategori', 'New Sales')->where('sow','Reseller')->where('achsales', 'like', "%" . $pekansales . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('achsales', 'like', "%" . "W1 ". $bulan . "%")->count() + Datasales2022::where('kategori', 'New Sales')->where('sow','Reseller')->where('achsales', 'like', "%" . "W2 ". $bulan . "%")->count();
            $macrototal = $macroco + $macrons;
            $micrototal = $microco + $microns;
            $colototal = $coloco + $colons;
            $resetotal = $reseco + $resens;
            $totalchart = $telkomsel + $telkom + $isat + $xl + $h3i + $sf + $sti + $oth;
            $totalcharta = $telkomsela + $telkoma + $isata + $xla + $h3ia + $sfa + $stia + $otha;
            
            return view('sales.salesreport', [
                'halaman' => $halaman,
                'labelbulan' => $labelbulan,
                'tahun' => $tahun,
                'bulan' => $bulan,
                'pekan' => $pekan,
                'macroco' => $macroco,
                'microco' => $microco,
                'coloco' => $coloco,
                'reseco' => $reseco,
                'macrons' => $macrons,
                'microns' => $microns,
                'colons' => $colons,
                'resens' => $resens,
                'macrototal' => $macrototal,
                'micrototal' => $micrototal,
                'colototal' => $colototal,
                'resetotal' => $resetotal,
                'tarmacro' => $tarmacro,
                'tarmicro' => $tarmicro,
                'tarcolo' => $tarcolo,
                'tarres' => $tarres,
                'summacro' => $summacro,
                'summicro' => $summicro,
                'sumcolo' => $sumcolo,
                'sumrese' => $sumrese,
                'cumtarmacro' => $cumtarmacro,
                'cumtarmicro' => $cumtarmicro,
                'cumtarcolo' => $cumtarcolo,
                'cumtarres' => $cumtarres,
                'cumcomacro' => $cumcomacro,
                'cumcomicro' => $cumcomicro,
                'cumcocolo'  => $cumcocolo,
                'cumcores'  => $cumcores,
                'cumnsmacro' => $cumnsmacro,
                'cumnsmicro' => $cumnsmicro,
                'cumnscolo'  => $cumnscolo,
                'cumnsres'  => $cumnsres,
                'telkomsel' => $telkomsel,
                'telkom' => $telkom,
                'isat' => $isat,
                'xl' => $xl,
                'h3i' => $h3i,
                'sf' => $sf,
                'sti' => $sti,
                'oth' => $oth,
                'totalchart' => $totalchart,
                'telkomsela' => $telkomsela,
                'telkoma' => $telkoma,
                'isata' => $isata,
                'xla' => $xla,
                'h3ia' => $h3ia,
                'sfa' => $sfa,
                'stia' => $stia,
                'otha' => $otha,
                'totalcharta' => $totalcharta,
                'hasilcek' => $hasilcek,
                'totsalepro' => $totsalepro,
                'tarsalepro' => $tarsalepro,
                'reasalepro' => $reasalepro,
                'conowpro' => $conowpro,
                'cobefpro' => $cobefpro,
                'nsnowpro' => $nsnowpro,
                'nsbefpro' => $nsbefpro,
                'macronowpro' => $macronowpro,
                'macrobefpro' => $macrobefpro,
                'micronowpro' => $micronowpro,
                'microbefpro' => $microbefpro,
                'colonowpro' => $colonowpro,
                'colobefpro' => $colobefpro,
                'resenowpro' => $resenowpro,
                'resebefpro' => $resebefpro,
                'a1nowpro' => $a1nowpro,
                'a1befpro' => $a1befpro,
                'a2nowpro' => $a2nowpro,
                'a2befpro' => $a2befpro,
                'a3nowpro' => $a3nowpro,
                'a3befpro' => $a3befpro,
                'a4nowpro' => $a4nowpro,
                'a4befpro' => $a4befpro,
                'hasiltotsale' => $hasiltotsale,
                'hasiltarsale' => $hasiltarsale,
                'hasilreasale' => $hasilreasale,
                'hasilconow' => $hasilconow,
                'hasilcobef' => $hasilcobef,
                'hasilnsnow' => $hasilnsnow,
                'hasilnsbef' => $hasilnsbef,
                'hasilmacronow' => $hasilmacronow,
                'hasilmacrobef' => $hasilmacrobef,
                'hasilmicronow' => $hasilmicronow,
                'hasilmicrobef' => $hasilmicrobef,
                'hasilcolonow' => $hasilcolonow,
                'hasilcolobef' => $hasilcolobef,
                'hasilresenow' => $hasilresenow,
                'hasilresebef' => $hasilresebef,
                'hasila1now' => $hasila1now,
                'hasila1bef' => $hasila1bef,
                'hasila2now' => $hasila2now,
                'hasila2bef' => $hasila2bef,
                'hasila3now' => $hasila3now,
                'hasila3bef' => $hasila3bef,
                'hasila4now' => $hasila4now,
                'hasila4bef' => $hasila4bef,
            ]);
        }elseif($tahun == 2021 && $bulan !='no' && $pekan =='W4'){
            $halaman = 1;
            $cek = Datasales2022::where('bulan', $bulan)->get();
            $hasilcek = $cek->isEmpty();
            $loop = Carbon::parse('1'.$bulan)->month;
            // Variabel Array
            $labelbln = [];
            $totsale = [];
            $tarsale = [];
            $reasale = [];
            $conow = [];
            $cobef = [];
            $nsnow = [];
            $nsbef = [];
            $macronow = [];
            $macrobef = [];
            $micronow = [];
            $microbef = [];
            $colonow = [];
            $colobef = [];
            $resenow = [];
            $resebef = [];
            $a1now = [];
            $a1bef = [];
            $a2now = [];
            $a2bef = [];
            $a3now = [];
            $a3bef = [];
            $a4now = [];
            $a4bef = [];
            // Variabel Loop
            $summacro = 0;
            $summicro = 0;
            $sumcolo = 0;
            $sumrese = 0;
            $cumtarmacro = 0;
            $cumtarmicro = 0;
            $cumtarcolo = 0;
            $cumtarres = 0;
            $totsalepro = 0;
            $tarsalepro = 0;
            $reasalepro = 0;
            $conowpro = 0;
            $cobefpro = 0;
            $nsnowpro = 0;
            $nsbefpro = 0;
            $macronowpro = 0;
            $macrobefpro = 0;
            $micronowpro = 0;
            $microbefpro = 0;
            $colonowpro = 0;
            $colobefpro = 0;
            $resenowpro = 0;
            $resebefpro = 0;
            $a1nowpro = 0;
            $a1befpro = 0;
            $a2nowpro = 0;
            $a2befpro = 0;
            $a3nowpro = 0;
            $a3befpro = 0;
            $a4nowpro = 0;
            $a4befpro = 0;
            $cumcomacro =  0;
            $cumcomicro = 0;
            $cumcocolo = 0;
            $cumcores = 0;
            $cumnsmacro =  0;
            $cumnsmicro = 0;
            $cumnscolo = 0;
            $cumnsres = 0;
            // Variabel Chart
            $telkomsel=0;
            $telkom=0;
            $isat=0;
            $xl=0;
            $h3i=0;
            $sf=0;
            $sti=0;
            $oth=0;
            $telkomsela=0;
            $telkoma=0;
            $isata=0;
            $xla=0;
            $h3ia=0;
            $sfa=0;
            $stia=0;
            $otha = 0;
            for ($i=1;$i<=$loop;$i++){
                $summacro = $summacro + Hasilsales2020::where('sow', 'macro')->sum($i);
                $summicro = $summicro + Hasilsales2020::where('sow', 'micro')->sum($i);
                $sumcolo = $sumcolo + Hasilsales2020::where('sow', 'colo')->sum($i);
                $sumrese = $sumrese + Hasilsales2020::where('sow', 'reseller')->sum($i);
                $cumtarmacro = $cumtarmacro + Targetsales2021::where('sow', 'Macro')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarmicro = $cumtarmicro + Targetsales2021::where('sow', 'Micro')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarcolo = $cumtarcolo + Targetsales2021::where('sow', 'Colo')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $cumtarres = $cumtarres + Targetsales2021::where('sow', 'Reseller')->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $totsalepro = Datasales2021::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $tarsalepro = Targetsales2021::select(Carbon::create()->day(1)->month($i)->format('M'))->sum(Carbon::create()->day(1)->month($i)->format('M'));
                $reasalepro = Datasales2020::where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $conowpro = Datasales2021::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cobefpro = Datasales2020::where('kategori', 'Carry Over')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsnowpro = Datasales2021::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $nsbefpro = Datasales2020::where('kategori', 'New Sales')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $macronowpro = Datasales2021::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $macrobefpro = Datasales2020::where('sow', 'Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $micronowpro = Datasales2021::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $microbefpro = Datasales2020::where('sow', 'Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colonowpro = Datasales2021::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $colobefpro = Datasales2020::where('sow', 'Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resenowpro = Datasales2021::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $resebefpro = Datasales2020::where('sow', 'Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1nowpro = Datasales2021::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a1befpro = Datasales2020::where('area', 'Area 1')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2nowpro = Datasales2021::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a2befpro = Datasales2020::where('area', 'Area 2')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3nowpro = Datasales2021::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a3befpro = Datasales2020::where('area', 'Area 3')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4nowpro = Datasales2021::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $a4befpro = Datasales2020::where('area', 'Area 4')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $bulansales = Carbon::create()->day(1)->month($i)->format('M');
                $cumcomacro =  $cumcomacro + Datasales2021::where('kategori', 'Carry Over')->where('sow','Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcomicro = $cumcomicro + Datasales2021::where('kategori', 'Carry Over')->where('sow','Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcocolo = $cumcocolo + Datasales2021::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumcores = $cumcores + Datasales2021::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsmacro =  $cumnsmacro + Datasales2021::where('kategori', 'New Sales')->where('sow','Macro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsmicro = $cumnsmicro + Datasales2021::where('kategori', 'New Sales')->where('sow','Micro')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnscolo = $cumnscolo + Datasales2021::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $cumnsres = $cumnsres + Datasales2021::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsel = $telkomsel + Datasales2021::where('tenant', 'TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkom = $telkom + Datasales2021::where('tenant', 'TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isat = $isat + Datasales2021::where('tenant', 'ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xl = $xl + Datasales2021::where('tenant', 'XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3i = $h3i + Datasales2021::where('tenant', 'H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sf = $sf + Datasales2021::where('tenant', 'SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sti = $sti + Datasales2021::where('tenant', 'STI')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $oth = $oth + Datasales2021::where('tenant', 'Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkomsela = $telkomsela + Datasales2021::where('kategori', 'New Sales')->where('tenant','TSEL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $telkoma = $telkoma + Datasales2021::where('kategori', 'New Sales')->where('tenant','TELKOM')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $isata = $isata + Datasales2021::where('kategori', 'New Sales')->where('tenant','ISAT')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $xla = $xla + Datasales2021::where('kategori', 'New Sales')->where('tenant','XL')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $h3ia = $h3ia + Datasales2021::where('kategori', 'New Sales')->where('tenant','H3I')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $sfa = $sfa + Datasales2021::where('kategori', 'New Sales')->where('tenant','SMART')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $stia = $stia + Datasales2021::where('kategori', 'New Sales')->where('tenant','STI')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                $otha = $otha + Datasales2021::where('kategori', 'New Sales')->where('tenant','Others')->where('bulan', Carbon::create()->day(1)->month($i)->format('M'))->count();
                array_push($labelbln, $bulansales);
                array_push($totsale, $totsalepro);
                array_push($tarsale, $tarsalepro);
                array_push($reasale, $reasalepro);
                array_push($conow, $conowpro);
                array_push($cobef, $cobefpro);
                array_push($nsnow, $nsnowpro);
                array_push($nsbef, $nsbefpro);
                array_push($macronow, $macronowpro);
                array_push($macrobef, $macrobefpro);
                array_push($micronow, $micronowpro);
                array_push($microbef, $microbefpro);
                array_push($colonow,$colonowpro);
                array_push($colobef,$colobefpro);
                array_push($resenow,$resenowpro);
                array_push($resebef,$resebefpro);
                array_push($a1now,$a1nowpro);
                array_push($a1bef,$a1befpro);
                array_push($a2now,$a2nowpro);
                array_push($a2bef,$a2befpro);
                array_push($a3now,$a3nowpro);
                array_push($a3bef,$a3befpro);
                array_push($a4now,$a4nowpro);
                array_push($a4bef,$a4befpro);
            }
            $labelbulan = json_encode($labelbln);
            $hasiltotsale = json_encode($totsale);
            $hasiltarsale = json_encode($tarsale);
            $hasilreasale = json_encode($reasale);
            $hasilconow = json_encode($conow);
            $hasilcobef = json_encode($cobef);
            $hasilnsnow = json_encode($nsnow);
            $hasilnsbef = json_encode($nsbef);
            $hasilmacronow = json_encode($macronow);
            $hasilmacrobef = json_encode($macrobef);
            $hasilmicronow = json_encode($micronow);
            $hasilmicrobef = json_encode($microbef);
            $hasilcolonow = json_encode($colonow);
            $hasilcolobef = json_encode($colobef);
            $hasilresenow = json_encode($resenow);
            $hasilresebef = json_encode($resebef);
            $hasila1now = json_encode($a1now);
            $hasila1bef = json_encode($a1bef);
            $hasila2now = json_encode($a2now);
            $hasila2bef = json_encode($a2bef);
            $hasila3now = json_encode($a3now);
            $hasila3bef = json_encode($a3bef);
            $hasila4now = json_encode($a4now);
            $hasila4bef = json_encode($a4bef);
            $tarmacro = Targetsales2021::select($bulan)->where('sow', 'Macro')->sum($bulan);
            $tarmicro = Targetsales2021::select($bulan)->where('sow', 'Micro')->sum($bulan);
            $tarcolo = Targetsales2021::select($bulan)->where('sow', 'Colo')->sum($bulan);
            $tarres = Targetsales2021::select($bulan)->where('sow', 'Reseller')->sum($bulan);
            $macroco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Macro')->where('bulan', $bulan)->count();
            $microco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Micro')->where('bulan', $bulan)->count();
            $coloco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Colo')->where('bulan', $bulan)->count();
            $reseco = Datasales2021::where('kategori', 'Carry Over')->where('sow','Reseller')->where('bulan', $bulan)->count();
            $macrons = Datasales2021::where('kategori', 'New Sales')->where('sow','Macro')->where('bulan', $bulan)->count();
            $microns = Datasales2021::where('kategori', 'New Sales')->where('sow','Micro')->where('bulan', $bulan)->count();
            $colons = Datasales2021::where('kategori', 'New Sales')->where('sow','Colo')->where('bulan', $bulan)->count();
            $resens = Datasales2021::where('kategori', 'New Sales')->where('sow','Reseller')->where('bulan', $bulan)->count();
            $macrototal = $macroco + $macrons;
            $micrototal = $microco + $microns;
            $colototal = $coloco + $colons;
            $resetotal = $reseco + $resens;
            $totalchart = $telkomsel + $telkom + $isat + $xl + $h3i + $sf + $sti + $oth;
            $totalcharta = $telkomsela + $telkoma + $isata + $xla + $h3ia + $sfa + $stia + $otha;
            
            return view('sales.salesreport', [
                'halaman' => $halaman,
                'labelbulan' => $labelbulan,
                'tahun' => $tahun,
                'bulan' => $bulan,
                'pekan' => $pekan,
                'macroco' => $macroco,
                'microco' => $microco,
                'coloco' => $coloco,
                'reseco' => $reseco,
                'macrons' => $macrons,
                'microns' => $microns,
                'colons' => $colons,
                'resens' => $resens,
                'macrototal' => $macrototal,
                'micrototal' => $micrototal,
                'colototal' => $colototal,
                'resetotal' => $resetotal,
                'tarmacro' => $tarmacro,
                'tarmicro' => $tarmicro,
                'tarcolo' => $tarcolo,
                'tarres' => $tarres,
                'summacro' => $summacro,
                'summicro' => $summicro,
                'sumcolo' => $sumcolo,
                'sumrese' => $sumrese,
                'cumtarmacro' => $cumtarmacro,
                'cumtarmicro' => $cumtarmicro,
                'cumtarcolo' => $cumtarcolo,
                'cumtarres' => $cumtarres,
                'cumcomacro' => $cumcomacro,
                'cumcomicro' => $cumcomicro,
                'cumcocolo'  => $cumcocolo,
                'cumcores'  => $cumcores,
                'cumnsmacro' => $cumnsmacro,
                'cumnsmicro' => $cumnsmicro,
                'cumnscolo'  => $cumnscolo,
                'cumnsres'  => $cumnsres,
                'telkomsel' => $telkomsel,
                'telkom' => $telkom,
                'isat' => $isat,
                'xl' => $xl,
                'h3i' => $h3i,
                'sf' => $sf,
                'sti' => $sti,
                'oth' => $oth,
                'totalchart' => $totalchart,
                'telkomsela' => $telkomsela,
                'telkoma' => $telkoma,
                'isata' => $isata,
                'xla' => $xla,
                'h3ia' => $h3ia,
                'sfa' => $sfa,
                'stia' => $stia,
                'otha' => $otha,
                'totalcharta' => $totalcharta,
                'hasilcek' => $hasilcek,
                'totsalepro' => $totsalepro,
                'tarsalepro' => $tarsalepro,
                'reasalepro' => $reasalepro,
                'conowpro' => $conowpro,
                'cobefpro' => $cobefpro,
                'nsnowpro' => $nsnowpro,
                'nsbefpro' => $nsbefpro,
                'macronowpro' => $macronowpro,
                'macrobefpro' => $macrobefpro,
                'micronowpro' => $micronowpro,
                'microbefpro' => $microbefpro,
                'colonowpro' => $colonowpro,
                'colobefpro' => $colobefpro,
                'resenowpro' => $resenowpro,
                'resebefpro' => $resebefpro,
                'a1nowpro' => $a1nowpro,
                'a1befpro' => $a1befpro,
                'a2nowpro' => $a2nowpro,
                'a2befpro' => $a2befpro,
                'a3nowpro' => $a3nowpro,
                'a3befpro' => $a3befpro,
                'a4nowpro' => $a4nowpro,
                'a4befpro' => $a4befpro,
                'hasiltotsale' => $hasiltotsale,
                'hasiltarsale' => $hasiltarsale,
                'hasilreasale' => $hasilreasale,
                'hasilconow' => $hasilconow,
                'hasilcobef' => $hasilcobef,
                'hasilnsnow' => $hasilnsnow,
                'hasilnsbef' => $hasilnsbef,
                'hasilmacronow' => $hasilmacronow,
                'hasilmacrobef' => $hasilmacrobef,
                'hasilmicronow' => $hasilmicronow,
                'hasilmicrobef' => $hasilmicrobef,
                'hasilcolonow' => $hasilcolonow,
                'hasilcolobef' => $hasilcolobef,
                'hasilresenow' => $hasilresenow,
                'hasilresebef' => $hasilresebef,
                'hasila1now' => $hasila1now,
                'hasila1bef' => $hasila1bef,
                'hasila2now' => $hasila2now,
                'hasila2bef' => $hasila2bef,
                'hasila3now' => $hasila3now,
                'hasila3bef' => $hasila3bef,
                'hasila4now' => $hasila4now,
                'hasila4bef' => $hasila4bef,
            ]);
        }
    }
}
