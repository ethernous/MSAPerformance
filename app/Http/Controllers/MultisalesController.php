<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Models\Datasales2022;use App\Models\Datasales2021;use App\Models\Datasales2020;

use App\Models\Area;use App\Models\Tenant;use App\Models\Sow;use Carbon\Carbon;

class MultisalesController extends Controller
{
    public function index($year)
    {   
        if ($year == '2022'){
            $data = Datasales2022::all();
            $carry = Datasales2022::where('kategori', "Carry Over")->count();
            $new = Datasales2022::where('kategori', "New Sales")->count();
        }elseif ($year == '2021'){
            $data = Datasales2021::all();
            $carry = Datasales2021::where('kategori', "Carry Over")->count();
            $new = Datasales2021::where('kategori', "New Sales")->count();
        }elseif ($year == '2020'){
            $data = Datasales2020::all();
            $carry = Datasales2020::where('kategori', "Carry Over")->count();
            $new = Datasales2020::where('kategori', "New Sales")->count();
        }
        return view('sales.level0', [
            'data' =>$data,
            'carry' =>$carry,
            'new' =>$new,
            'year' => $year,
        ]);
    }
    public function level1($year, $id){
        $category = $id;
        if ($year == '2022'){
            if ($id == "Carry Over"){
                $data = Datasales2022::where('kategori', "Carry Over")->get();
                $area = Area::count();
                $dataarea = Area::all();
                $sow = Sow::where('tahunsow', $year)->count();
                $datasow = SoW::where('tahunsow', $year)->get();
                $tenant = Tenant::count();
                $datatenant = Tenant::all();
            } elseif ($id == "New Sales"){
                $data = Datasales2022::where('kategori', "New Sales")->get();
                $area = Area::count();
                $dataarea = Area::all();
                $sow = Sow::where('tahunsow', $year)->count();
                $datasow = SoW::where('tahunsow', $year)->get();
                $tenant = Tenant::count();
                $datatenant = Tenant::all();
            }
        }elseif ($year == '2021'){
            if ($id == "Carry Over"){
                $data = Datasales2021::where('kategori', "Carry Over")->get();
                $area = Area::count();
                $dataarea = Area::all();
                $sow = Sow::where('tahunsow', $year)->count();
                $datasow = SoW::where('tahunsow', $year)->get();
                $tenant = Tenant::count();
                $datatenant = Tenant::all();
            } elseif ($id == "New Sales"){
                $data = Datasales2021::where('kategori', "New Sales")->get();
                $area = Area::count();
                $dataarea = Area::all();
                $sow = Sow::where('tahunsow', $year)->count();
                $datasow = SoW::where('tahunsow', $year)->get();
                $tenant = Tenant::count();
                $datatenant = Tenant::all();
            }
        }elseif ($year == '2020'){
            if ($id == "Carry Over"){
                $data = Datasales2020::where('kategori', "Carry Over")->get();
                $area = Area::count();
                $dataarea = Area::all();
                $sow = Sow::where('tahunsow', $year)->count();
                $datasow = SoW::where('tahunsow', $year)->get();
                $tenant = Tenant::count();
                $datatenant = Tenant::all();
            } elseif ($id == "New Sales"){
                $data = Datasales2020::where('kategori', "New Sales")->get();
                $area = Area::count();
                $dataarea = Area::all();
                $sow = Sow::where('tahunsow', $year)->count();
                $datasow = SoW::where('tahunsow', $year)->get();
                $tenant = Tenant::count();
                $datatenant = Tenant::all();
            }
        }
        return view('sales.level1', [
            'data'      =>$data, 
            'year'      =>$year,
            'category'  =>$category,
            'area'      =>$area,
            'dataarea'  =>$dataarea,
            'tenant'    =>$tenant,
            'datatenant'=>$datatenant,
            'sow'       =>$sow,
            'datasow'   =>$datasow,
        ]);
    }
    public function level2($year, $category, $segmen, $id)
    {
        $segval = $id;
        if($year == '2022'){
            $data = Datasales2022::where('kategori', $category)->where($segmen, $id)->get();
        } elseif($year == '2021'){
            $data = Datasales2021::where('kategori', $category)->where($segmen, $id)->get();
        } elseif($year == '2020'){
            $data = Datasales2020::where('kategori', $category)->where($segmen, $id)->get();
        }
            return view('sales.level2', [
            'data' =>$data,
            'year' =>$year,
            'category' =>$category,
            'segmen' =>$segmen,
            'segval' =>$segval,
        ]);
    }
    public function level3($year, $category, $segmen, $segval)
    {
        if ($year == '2022') {
            $data = Datasales2022::where('kategori', $category)->where($segmen, $segval)->get();
        } elseif($year == '2021') {
            $data = Datasales2021::where('kategori', $category)->where($segmen, $segval)->get();
        } elseif($year == '2020'){
            $data = Datasales2020::where('kategori', $category)->where($segmen, $segval)->get();
        }
        
        return view('sales.level3', [
            'data' =>$data,
            'year' =>$year
        ]);
    }
}
