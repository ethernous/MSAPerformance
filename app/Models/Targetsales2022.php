<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Targetsales2022 extends Model
{
    use HasFactory;
    protected $table = 'tarsale2022';
    protected $guarded = [];
    protected  $primaryKey = 'sow';
    public $incrementing = 'false';
    public $keyType = 'string';
}
