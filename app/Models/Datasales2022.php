<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Datasales2022 extends Model
{
    use HasFactory;
    protected $table = 'datasale2022';
    protected $guarded = [];
    protected  $primaryKey = 'piddmt';
    public $incrementing = 'false';
    public $keyType = 'string';
}
