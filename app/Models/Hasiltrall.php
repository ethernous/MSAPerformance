<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hasiltrall extends Model
{
    use HasFactory;
    protected $table = 'hasiltrall';
    protected $guarded = [];
    protected  $primaryKey = 'sow';
    public $incrementing = 'false';
    public $keyType = 'string';
}
