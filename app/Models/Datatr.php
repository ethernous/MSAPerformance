<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Datatr extends Model
{
    use HasFactory;
    protected $table = 'datatr';
    protected $guarded = [];
    protected  $primaryKey = 'siteiddmt';
    public $incrementing = 'false';
    public $keyType = 'string';
}
