<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hasilsales2022 extends Model
{
    use HasFactory;
    protected $table = 'hassale2022';
    protected $guarded = [];
    protected  $primaryKey = 'sow';
    public $incrementing = 'false';
    public $keyType = 'string';
}
