<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sow extends Model
{
    use HasFactory;
    protected $table = 'sow';
    protected $guarded = [];
    protected  $primaryKey = 'jenissow';
    public $incrementing = 'false';
    public $keyType = 'string';
}
