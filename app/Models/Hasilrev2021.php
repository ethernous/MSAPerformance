<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hasilrev2021 extends Model
{
    use HasFactory;
    protected $table = 'hasrev2021';
    protected $guarded = [];
    protected  $primaryKey = 'piddmt';
    public $incrementing = 'false';
    public $keyType = 'string';
}
