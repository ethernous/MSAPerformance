<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Targetsales2020 extends Model
{
    use HasFactory;
    protected $table = 'tarsale2020';
    protected $guarded = [];
    protected  $primaryKey = 'sow';
    public $incrementing = 'false';
    public $keyType = 'string';
}
