<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hasilsales2019 extends Model
{
    use HasFactory;
    protected $table = 'hassale2019';
    protected $guarded = [];
    protected  $primaryKey = 'sow';
    public $incrementing = 'false';
    public $keyType = 'string';
}
