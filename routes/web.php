<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['register'=>false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/sales/dashboard', [App\Http\Controllers\SalesController::class, 'index'])->name('Sales Dashboard');
Route::get('/sales/dashboard/{segmen}/{segval}', [App\Http\Controllers\SalesController::class, 'details'])->name('Sales Dashboard Details');
Route::get('/rev/dashboard', [App\Http\Controllers\RevController::class, 'index'])->name('Revenue Dashboard');
Route::get('/tratio/dashboard', [App\Http\Controllers\TRController::class, 'index'])->name('TR Dashboard');

Route::get('/salerep', [App\Http\Controllers\SalerepController::class, 'index'])->name('Sales Selection');
Route::get('/saleresult', [App\Http\Controllers\SalesrepController::class, 'cari'])->name('Sales Report');

Route::get('/revrep', [App\Http\Controllers\RevrepController::class, 'index'])->name('Revenue Selection');
Route::get('/revresult', [App\Http\Controllers\RevrepController::class, 'cari'])->name('Revenue Report');

Route::get('/trrep', [App\Http\Controllers\TrrepController::class, 'index'])->name('TR Selection');
Route::get('/trresult', [App\Http\Controllers\TrrepController::class, 'cari'])->name('TR Report');

Route::get('/search', [App\Http\Controllers\SearchController::class, 'search'])->name('Search Result');

Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('Profile');
Route::put('/profile', [App\Http\Controllers\ProfileController::class, 'profile'])->name('profile.update');
Route::get('/about', [App\Http\Controllers\ProfileController::class, 'about'])->name('About');