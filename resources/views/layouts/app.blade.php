<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" href="{{ asset('img/favicon.png') }}">
    <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('img/favicon.png') }}">
    <title>
        {{ config('app.name', 'Performance Dashboard | Mitratel') }}
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="{{ asset('css/nucleo-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="{{ asset('css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="{{ asset('css/argon-dashboard.css?v=2.0.1') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>

<body class="g-sidenav-show   bg-gray-100">
    <div class="min-height-300 bg-danger position-absolute w-100"></div>
    <aside
        class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4 "
        id="sidenav-main">
        <div class="sidenav-header">
            <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
                aria-hidden="true" id="iconSidenav"></i>
            <a class="navbar-brand m-0" href="{{ route('home') }}">
                <img src="{{ asset('img/logo-ct.png') }}" class="navbar-brand-img h-100" alt="main_logo">
                <span class="ms-1 font-weight-bold">Mitratel Performance</span>
            </a>
        </div>
        <hr class="horizontal dark mt-0">
        <div class="collapse navbar-collapse  w-auto h-auto" id="sidenav-collapse-main">
            <ul class="navbar-nav">
                @if (Auth::User()->jobdesc == 'all')
                <li class="nav-item">
                    <a data-bs-toggle="collapse" href="#dashboardsExamples" class="nav-link {{ Nav::hasSegment(['dashboard'], 2) }}"
                        aria-controls="dashboardsExamples" role="button" aria-expanded="false">
                        <div
                            class="icon icon-shape icon-sm text-center d-flex align-items-center justify-content-center">
                            <i class="ni ni-shop text-primary text-sm opacity-10"></i>
                        </div>
                        <span class="nav-link-text ms-1">Dashboards</span>
                    </a>
                    <div class="collapse {{ Nav::hasSegment(['dashboard'], 2, 'show') }}" id="dashboardsExamples">
                        <ul class="nav ms-4">
                            <li class="nav-item {{ Nav::isRoute('Sales Dashboard') }}">
                                <a class="nav-link {{ Nav::isRoute('Sales Dashboard') }}" href="{{ route('Sales Dashboard') }}">
                                    <span class="sidenav-mini-icon"> S </span>
                                    <span class="sidenav-normal"> Sales Dashboard </span>
                                </a>
                            </li>
                            <li class="nav-item {{ Nav::isRoute('Revenue Dashboard') }}">
                                <a class="nav-link {{ Nav::isRoute('Revenue Dashboard') }}" href="{{ route('Revenue Dashboard') }}">
                                    <span class="sidenav-mini-icon"> R </span>
                                    <span class="sidenav-normal"> Revenue Dashboard </span>
                                </a>
                            </li>
                            <li class="nav-item {{ Nav::isRoute('TR Dashboard') }}">
                                <a class="nav-link {{ Nav::isRoute('TR Dashboard') }}" href="{{ route('TR Dashboard') }}">
                                    <span class="sidenav-mini-icon"> TR </span>
                                    <span class="sidenav-normal"> Tenancy Ratio Dashboard </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item mt-3">
                    <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Reports</h6>
                </li>
                <li class="nav-item">
                    <a data-bs-toggle="collapse" href="#pagesExamples" class="nav-link {{ Nav::hasSegment(['salerep', 'saleresult', 'revrep', 'revresult', 'trrep', 'trresult'], 1) }}" aria-controls="pagesExamples"
                        role="button" aria-expanded="false">
                        <div
                            class="icon icon-shape icon-sm text-center d-flex align-items-center justify-content-center">
                            <i class="ni ni-ungroup text-warning text-sm opacity-10"></i>
                        </div>
                        <span class="nav-link-text ms-1">Reports List</span>
                    </a>
                    <div class="collapse {{ Nav::hasSegment(['salerep', 'saleresult', 'revrep', 'revresult', 'trrep', 'trresult'], 1, 'show') }}" id="pagesExamples">
                        <ul class="nav ms-4">
                            <li class="nav-item {{ Nav::isRoute('Sales Selection') }}">
                                <a class="nav-link {{ Nav::isRoute('Sales Selection') }}" href="{{ route('Sales Selection') }}">
                                    <span class="sidenav-mini-icon"> S </span>
                                    <span class="sidenav-normal"> Sales Report </span>
                                </a>
                            </li>
                            <li class="nav-item {{ Nav::isRoute('Revenue Selection') }}">
                                <a class="nav-link {{ Nav::isRoute('Revenue Selection') }}" href="{{ route('Revenue Selection') }}">
                                    <span class="sidenav-mini-icon"> R </span>
                                    <span class="sidenav-normal"> Revenue Report </span>
                                </a>
                            </li>
                            <li class="nav-item {{ Nav::isRoute('TR Selection') }}">
                                <a class="nav-link {{ Nav::isRoute('TR Selection') }}" href="{{ route('TR Selection') }}">
                                    <span class="sidenav-mini-icon"> TR </span>
                                    <span class="sidenav-normal"> Tenancy Ratio Report </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <hr class="horizontal dark" />
                    <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Settings</h6>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Nav::isRoute('Profile') }}" href="{{ route('Profile') }}">
                        <div
                            class="icon icon-shape icon-sm text-center  me-2 d-flex align-items-center justify-content-center">
                            <i class="ni ni-single-02 text-dark text-sm"></i>
                        </div>
                        <span class="nav-link-text ms-1">Profile</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Nav::isRoute('About') }}" href="{{ route('About') }}">
                        <div
                            class="icon icon-shape icon-sm text-center  me-2 d-flex align-items-center justify-content-center">
                            <i class="ni ni-app text-dark text-sm"></i>
                        </div>
                        <span class="nav-link-text ms-1">About</span>
                    </a>
                </li>
                @elseif (Auth::User()->jobdesc == 'Sales')
                @elseif (Auth::User()->jobdesc == 'Rev')
                @elseif (Auth::User()->jobdesc == 'TR')
                @endif
            </ul>
        </div>
        <div class="sidenav-footer mx-3 my-3">
            <a href="#" data-bs-toggle="modal" data-bs-target="#modal-contact"
                class="btn btn-dark btn-sm w-100 mb-3">Need Help?</a>
            <p class="text-center text-xs">Marketing Strategy & Analytics</p>
        </div>
    </aside>
    <main class="main-content position-relative border-radius-lg ">
        <!-- Navbar -->
        <nav class="navbar navbar-main navbar-expand-lg  px-0 mx-4 shadow-none border-radius-xl z-index-sticky " id="navbarBlur" data-scroll="false">
            <div class="container-fluid py-1 px-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                        <li class="breadcrumb-item text-sm">
                            <a class="text-white" href="javascript:;">
                                <i class="ni ni-box-2"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item text-sm text-white"><a class="opacity-5 text-white"
                                href="javascript:;">Pages</a></li>
                        <li class="breadcrumb-item text-sm text-white active" aria-current="page">{{ Route::currentRouteName() }}</li>
                    </ol>
                    <h6 class="font-weight-bolder mb-0 text-white">{{ Route::currentRouteName() }}</h6>
                </nav>
                <div class="sidenav-toggler sidenav-toggler-inner d-xl-block d-none ">
                    <a href="javascript:;" class="nav-link p-0">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line bg-white"></i>
                            <i class="sidenav-toggler-line bg-white"></i>
                            <i class="sidenav-toggler-line bg-white"></i>
                        </div>
                    </a>
                </div>
                <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                    <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                        <span class="nav-link text-white font-weight-bold px-0">{{ Auth::user()->name }}</span>
                    </div>
                    {{-- @if (Auth::User()->jobdesc == 'all')
                    <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                        <form action="/search" method="get">
                            <div class="input-group">
                                <select class="form-select" aria-label="Default select example" name="dbase">
                                    <option selected>Segment</option>
                                    <option value="Sales">Sales Data</option>
                                    <option value="Revenue">Revenue Data</option>
                                    <option value="Tratio">Tenancy Ratio Data</option>
                                </select>
                                <select class="form-select" aria-label="Default select example" name="year">
                                    <option selected>Year</option>
                                    <option value="2022">2022</option>
                                    <option value="2021">2021</option>
                                    <option value="2020">2020</option>
                                </select>
                                <input type="text" class="form-control" name="cari" placeholder="Type here...">
                                <button class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true" type="submit"></i></button>
                            </div>
                        </form>
                    </div>
                    @elseif (Auth::User()->jobdesc == 'Sales')
                    <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                        <form action="/search" method="get">
                            <div class="input-group">
                                <select class="form-select" aria-label="Default select example" name="dbase">
                                    <option selected>Segment</option>
                                    <option value="Sales">Sales Data</option>
                                </select>
                                <select class="form-select" aria-label="Default select example" name="year">
                                    <option selected>Year</option>
                                    <option value="2022">2022</option>
                                    <option value="2021">2021</option>
                                    <option value="2020">2020</option>
                                </select>
                                <input type="text" class="form-control" name="cari" placeholder="Type here...">
                                <button class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true" type="submit"></i></button>
                            </div>
                        </form>
                    </div>
                    @elseif (Auth::User()->jobdesc == 'Rev')
                    <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                        <form action="/search" method="get">
                            <div class="input-group">
                                <select class="form-select" aria-label="Default select example" name="dbase">
                                    <option selected>Segment</option>
                                    <option value="Revenue">Revenue Data</option>
                                </select>
                                <select class="form-select" aria-label="Default select example" name="year">
                                    <option selected>Year</option>
                                    <option value="2022">2022</option>
                                    <option value="2021">2021</option>
                                    <option value="2020">2020</option>
                                </select>
                                <input type="text" class="form-control" name="cari" placeholder="Type here...">
                                <button class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true" type="submit"></i></button>
                            </div>
                        </form>
                    </div>
                    @elseif (Auth::User()->jobdesc == 'TR')
                    <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                        <form action="/search" method="get">
                            <div class="input-group">
                                <select class="form-select" aria-label="Default select example" name="dbase">
                                    <option selected>Segment</option>
                                    <option value="Tratio">Tenancy Ratio Data</option>
                                </select>
                                <select class="form-select" aria-label="Default select example" name="year">
                                    <option selected>Year</option>
                                    <option value="2022">2022</option>
                                    <option value="2021">2021</option>
                                    <option value="2020">2020</option>
                                </select>
                                <input type="text" class="form-control" name="cari" placeholder="Type here...">
                                <button class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true" type="submit"></i></button>
                            </div>
                        </form>
                    </div>
                    @endif --}}
                    <ul class="navbar-nav  justify-content-end">
                        <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
                            <a href="javascript:;" class="nav-link text-white p-0" id="iconNavbarSidenav">
                                <div class="sidenav-toggler-inner">
                                    <i class="sidenav-toggler-line bg-white"></i>
                                    <i class="sidenav-toggler-line bg-white"></i>
                                    <i class="sidenav-toggler-line bg-white"></i>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item px-3 d-flex align-items-center">
                            <a href="javascript:;" class="nav-link text-white p-0">
                                <i class="fa fa-cog fixed-plugin-button-nav cursor-pointer"></i>
                            </a>
                        </li>
                        <li class="nav-item d-flex align-items-center">
                            <a href="#" class="nav-link text-white font-weight-bold px-0" data-bs-toggle="modal" data-bs-target="#modal-logout">
                                <i class="fa fa-sign-out me-sm-1"></i>
                                <span class="d-sm-inline d-none">Sign Out</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="container-fluid py-4">
            @yield('content')
            <footer class="footer pt-3  ">
                <div class="container-fluid">
                    <div class="row align-items-center justify-content-lg-between">
                        <div class="col-lg-6 mb-lg-0 mb-4">
                            <div class="copyright text-center text-sm text-muted text-lg-start">
                                © <script>
                                    document.write(new Date().getFullYear())
                                </script>,
                                made with <i class="fa fa-heart"></i> by
                                <a href="https://www.mitratel.co.id" class="font-weight-bold" target="_blank">MSA</a>
                                for an easier data.
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </main>
    <div class="modal fade" id="modal-logout" tabindex="-1" role="dialog" aria-labelledby="modal-logout" aria-hidden="true">
        <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="modal-title-default">Session Logout</h6>
                </div>
                <div class="modal-body">
                    Select "Sign Out" below if you are ready to end your current session.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link  ml-auto" data-bs-dismiss="modal">Close</button>
                    <a class="btn btn-danger" href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Sign Out') }}</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
        <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="modal-title-default"><i class="fa-solid fa-id-card"></i> Dev Contact</h6>
                </div>
                <div class="modal-body">
                    <i class="fa-solid fa-envelope"></i><a href="mailto:ilham.rabbani@pt-sdm.co.id" class="text-dark font-weight-bold" target="blank"> ilham.rabbani@pt-sdm.co.id</a>
                    <br>
                    <i class="fa-brands fa-whatsapp"></i><a href="https://wa.me/+6281214031043" class="text-dark font-weight-bold" target="blank"> +6281214031043</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link  ml-auto" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="fixed-plugin">
        <a class="fixed-plugin-button text-dark position-fixed px-3 py-2">
            <i class="fa fa-cog py-2"> </i>
        </a>
        <div class="card shadow-lg">
            <div class="card-header pb-0 pt-3 bg-transparent ">
                <div class="float-start">
                    <h5 class="mt-3 mb-0">Dashboard Configurator</h5>
                    <p>See our dashboard options.</p>
                </div>
                <div class="float-end mt-4">
                    <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
                        <i class="fa fa-close"></i>
                    </button>
                </div>
                <!-- End Toggle Button -->
            </div>
            <hr class="horizontal dark my-1">
            <div class="card-body pt-sm-3 pt-0 overflow-auto">
                <!-- Sidebar Backgrounds -->
                <div>
                    <h6 class="mb-0">Sidebar Colors</h6>
                </div>
                <a href="javascript:void(0)" class="switch-trigger background-color">
                    <div class="badge-colors my-2 text-start">
                        <span class="badge filter bg-gradient-primary active" data-color="primary"
                            onclick="sidebarColor(this)"></span>
                        <span class="badge filter bg-gradient-dark" data-color="dark"
                            onclick="sidebarColor(this)"></span>
                        <span class="badge filter bg-gradient-info" data-color="info"
                            onclick="sidebarColor(this)"></span>
                        <span class="badge filter bg-gradient-success" data-color="success"
                            onclick="sidebarColor(this)"></span>
                        <span class="badge filter bg-gradient-warning" data-color="warning"
                            onclick="sidebarColor(this)"></span>
                        <span class="badge filter bg-gradient-danger" data-color="danger"
                            onclick="sidebarColor(this)"></span>
                    </div>
                </a>
                <!-- Sidenav Type -->
                <div class="mt-3">
                    <h6 class="mb-0">Sidenav Type</h6>
                    <p class="text-sm">Choose between 2 different sidenav types.</p>
                </div>
                <div class="d-flex">
                    <button class="btn bg-gradient-primary w-100 px-3 mb-2 active me-2" data-class="bg-white"
                        onclick="sidebarType(this)">White</button>
                    <button class="btn bg-gradient-primary w-100 px-3 mb-2" data-class="bg-default"
                        onclick="sidebarType(this)">Dark</button>
                </div>
                <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>
                <!-- Navbar Fixed -->
                <div class="d-flex my-3">
                    <h6 class="mb-0">Navbar Fixed</h6>
                    <div class="form-check form-switch ps-0 ms-auto my-auto">
                        <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed"
                            onclick="navbarFixed(this)">
                    </div>
                </div>
                <hr class="horizontal dark mb-1">
                <div class="d-flex my-4">
                    <h6 class="mb-0">Sidenav Mini</h6>
                    <div class="form-check form-switch ps-0 ms-auto my-auto">
                        <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarMinimize"
                            onclick="navbarMinimize(this)">
                    </div>
                </div>
                <hr class="horizontal dark my-sm-4">
                <div class="mt-2 mb-5 d-flex">
                    <h6 class="mb-0">Light / Dark</h6>
                    <div class="form-check form-switch ps-0 ms-auto my-auto">
                        <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version"
                            onclick="darkMode(this)">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--   Core JS Files   -->
    <script src="{{ asset('js/core/popper.min.js') }}"></script>
    <script src="{{ asset('js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/plugins/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('js/plugins/smooth-scrollbar.min.js') }}"></script>
    <script src="{{ asset('js/plugins/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('js/plugins/choices.min.js') }}"></script>
    <script src="{{ asset('js/plugins/round-slider.min.js') }}"></script>
    <!-- Kanban scripts -->
    <script src="{{ asset('js/plugins/dragula/dragula.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jkanban/jkanban.js') }}"></script>
    <script src="{{ asset('js/plugins/chartjs.min.js') }}"></script>
    <script>
        var win = navigator.platform.indexOf('Win') > -1;
        if (win && document.querySelector('#sidenav-scrollbar')) {
            var options = {
                damping: '0.5'
            }
            Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
        }
    </script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('js/argon-dashboard.min.js?v=2.0.2') }}"></script>
    @stack('scripts')
</body>

</html>