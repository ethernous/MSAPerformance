<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" href="https://www.mitratel.co.id/wp-content/uploads/2021/01/cropped-Asset-6@4x-180x180.png">
    <link rel="icon" sizes="192x192" type="image/png" href="https://www.mitratel.co.id/wp-content/uploads/2021/01/cropped-Asset-6@4x-192x192.png">
    <title>
        {{ config('app.name', 'Performance Dashboard | Mitratel') }}
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="{{ asset('css/nucleo-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="{{ asset('css/nucleo-svg.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.css') }}">
    <!-- CSS Files -->
    <link id="pagestyle" href="{{ asset('css/argon-dashboard.css?v=2.0.1') }}" rel="stylesheet" />
</head>

<body class="">
  <main class="main-content  mt-0">
      @yield('content')
  </main>
  <div class="modal fade" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default"><i class="fa-solid fa-id-card"></i> Dev Contact</h6>
            </div>
            <div class="modal-body">
                <i class="fa-solid fa-envelope"></i><a href="mailto:ilham.rabbani@pt-sdm.co.id" class="text-dark font-weight-bold" target="blank"> ilham.rabbani@pt-sdm.co.id</a>
                <br>
                <i class="fa-brands fa-whatsapp"></i><a href="https://wa.me/+6281214031043" class="text-dark font-weight-bold" target="blank"> +6281214031043</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link  ml-auto" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
  <!--   Core JS Files   -->
  <script src="{{ asset('js/core/popper.min.js') }}"></script>
  <script src="{{ asset('js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/plugins/perfect-scrollbar.min.js') }}"></script>
  <script src="{{ asset('js/plugins/smooth-scrollbar.min.js') }}"></script>
  <!-- Kanban scripts -->
  <script src="{{ asset('js/plugins/dragula/dragula.min.js') }}"></script>
  <script src="{{ asset('js/plugins/jkanban/jkanban.js') }}"></script>
  <script src="{{ asset('js/plugins/chartjs.min.js') }}"></script>
  <script>
      var win = navigator.platform.indexOf('Win') > -1;
      if (win && document.querySelector('#sidenav-scrollbar')) {
          var options = {
              damping: '0.5'
          }
          Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
      }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('js/argon-dashboard.min.js?v=2.0.1') }}"></script>
</body>

</html>