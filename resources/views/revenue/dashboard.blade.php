@extends('layouts.app')

@section('content')
<div class="row  mt-n2">
    <div class="col-xl-8 col-lg-7">
        <div class="row">
            <div class="col-sm-3">
                <div class="card overflow-hidden">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-tower-cell opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">Rev Achievement</h6>
                        <span class="text-xs">YTD {{ \Carbon\Carbon::now()->format('M Y') }}</span>
                        <hr class="horizontal dark my-3">
                        <h5 class="mb-0">{{ number_format(($sumtotalrevnew)/1000000000) }}M</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 mt-sm-0 mt-4">
                <div class="card overflow-hidden">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-tower-cell opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">Rev Achievement</h6>
                        <span class="text-xs">YTD {{ \Carbon\Carbon::now()->subWeeks(52)->format('M Y') }}</span>
                        <hr class="horizontal dark my-3">
                        <h5 class="mb-0">{{ number_format(($sumtotalrevold)/1000000000) }}M</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 mt-sm-0 mt-4">
                <div class="card overflow-hidden">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-bullseye opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">Rev Target</h6>
                        <span class="text-xs">YTD {{ \Carbon\Carbon::now()->format('M Y') }}</span>
                        <hr class="horizontal dark my-3">
                        <h5 class="mb-0">{{ number_format(($sumtargetrev)/1000000000) }}M</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 mt-sm-0 mt-4">
                <div class="card overflow-hidden">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-arrow-up-right-dots opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">Rev Growth</h6>
                        <span class="text-xs">YTD {{ \Carbon\Carbon::now()->format('M Y') }}</span>
                        <hr class="horizontal dark my-3">
                        <a href="/saleresult?year={{ \Carbon\Carbon::now()->format('Y') }}&month={{ \Carbon\Carbon::now()->format('M') }}&week=no"><h5 class="mb-0">{{ number_format((float)(($sumtotalrevnew/$sumtotalrevold)-1)*100, 2, '.', '') }}%</h5></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-5 mt-lg-0 mt-4">
        <div class="row">
            <div class="col-lg-12">
                
                    <div class="overflow-hidden position-relative border-radius-lg bg-cover h-100"
                        style="background-image: url(https://www.mitratel.co.id/wp-content/uploads/2022/03/Solusi-Kami-2c-1-1024x811.jpg);">
                        <span class="mask bg-gradient-dark"></span>
                        <div class="card-body position-relative z-index-1 h-100 p-3">
                            <h6 class="text-white font-weight-bolder mb-3">Hey {{ Auth::User()->name }}!</h6>
                            <p class="text-white mb-3">Welcome to Revenue Dashboard, since you are a/an {{ Auth::User()->jabatan }}, you gained access to {{ Auth::User()->jobdesc }} pages. Feel free to contact the developer if any error occurs!</p>
                            <hr>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<hr class="horizontal dark mt-4">
<div class="row mt-4">
    <div class="col-lg-6 col-sm-6">
        <div class="card h-100">
            <div class="card-body">
                <div class="chart">
                    <canvas id="segment-chart"height="300"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-sm-6">
        <div class="card h-100">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                              <th class=" text-center" scope="col">YTD</th>
                              <th class="" scope="col">Recurring</th>
                              <th class="" scope="col">Net Add</th>
                              <th class="" scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th class=" text-center" scope="row">Anchor Tenant</th>
                                <td class=" text-end">{{ number_format($ytdanchorrec/1000000000) }}M</td>
                                <td class=" text-end">{{ number_format($ytdanchoradd/1000000000) }}M</td>
                                <td class=" text-end">{{ number_format(($ytdanchorrec + $ytdanchoradd)/1000000000) }}M</td>
                            </tr>
                            <tr>
                                <th class=" text-center" scope="row">2nd Tenant</th>
                                <td class=" text-end">{{ number_format($ytdsectenantrec/1000000000) }}M</td>
                                <td class=" text-end">{{ number_format($ytdsectenantadd/1000000000) }}M</td>
                                <td class=" text-end">{{ number_format(($ytdsectenantrec + $ytdsectenantadd)/1000000000) }}M</td>
                            </tr>
                            <tr>
                                <th class=" text-center" scope="row">Reseller</th>
                                <td class=" text-end">{{ number_format($ytdresellerrec/1000000000) }}M</td>
                                <td class=" text-end">{{ number_format($ytdreselleradd/1000000000) }}M</td>
                                <td class=" text-end">{{ number_format(($ytdresellerrec + $ytdreselleradd)/1000000000) }}M</td>
                            </tr>
                            <tr>
                                <td class=" text-center" scope="row">Project Solution</th>
                                    <td class=" text-end">{{ number_format($ytdprojectrec/1000000000) }}M</td>
                                    <td class=" text-end">{{ number_format($ytdprojectadd/1000000000) }}M</td>
                                    <td class=" text-end">{{ number_format(($ytdprojectrec + $ytdprojectadd)/1000000000) }}M</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class=" text-center" scope="row">Sum</th>
                                <td class=" text-bold text-end">{{ number_format(($ytdanchorrec + $ytdsectenantrec + $ytdresellerrec + $ytdprojectrec)/1000000000) }}M</td>
                                <td class=" text-bold text-end">{{ number_format(($ytdanchoradd + $ytdsectenantadd + $ytdreselleradd + $ytdprojectadd)/1000000000) }}M</td>
                                <td class=" text-bold text-end">{{ number_format(($ytdanchorrec + $ytdsectenantrec + $ytdresellerrec + $ytdprojectrec + $ytdanchoradd + $ytdsectenantadd + $ytdreselleradd + $ytdprojectadd)/1000000000) }}M</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        var ctx4 = document.getElementById("segment-chart").getContext("2d");
        new Chart(ctx4, {
            type: "line",
            data: {
                labels: {!! $hasilbulanrev !!},
                datasets: [{
                        label: "Sales 1",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#5e72e4",
                        borderColor: "#5e72e4",
                        borderWidth: 3,
                        data: {{ $hasilcharttg1 }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Sales 2",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#3A416F",
                        borderColor: "#3A416F",
                        borderWidth: 3,
                        data: {{ $hasilcharttg2 }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Sales 3",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#17c1e8",
                        borderColor: "#17c1e8",
                        borderWidth: 3,
                        data: {{ $hasilcharttg3 }},
                        maxBarThickness: 6
                    },
                ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: true,
                        position: 'bottom',
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: false,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            padding: 10,
                            color: '#b2b9bf',
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            color: '#b2b9bf',
                            padding: 10,
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                },
            },
        });
    </script>
@endpush
