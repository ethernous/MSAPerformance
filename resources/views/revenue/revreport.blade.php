@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-8 m-auto">
        <div class="card">
            <div class="card-body pt-0">
                <form action="/revresult" method="get">
                    <div class="row mt-4">
                        <div class="col-xl-4 col-md-12">
                            <label class="form-label">Year : </label>
                            <select name="year" class="form-select input-group">
                                <option value="no">Select Year from the list below</option>
                                <option value="2022" @if ($year==2022) selected @endif>2022</option>
                            </select>
                        </div>
                        <div class="col-xl-5 col-md-12">
                            <label class="form-label">Month : </label>
                            <select name="month" class="form-select input-group">
                                <option value="no">Select month from the list below</option>
                                <option value="Jan" @if ($month=='Jan' ) selected @endif>January</option>
                                <option value="Feb" @if ($month=='Feb' ) selected @endif>February</option>
                                <option value="Mar" @if ($month=='Mar' ) selected @endif>March</option>
                                <option value="Apr" @if ($month=='Apr' ) selected @endif>April</option>
                                <option value="May" @if ($month=='May' ) selected @endif>May</option>
                                <option value="Jun" @if ($month=='Jun' ) selected @endif>June</option>
                                <option value="Jul" @if ($month=='Jul' ) selected @endif>July</option>
                                <option value="Aug" @if ($month=='Aug' ) selected @endif>August</option>
                                <option value="Sep" @if ($month=='Sep' ) selected @endif>September</option>
                                <option value="Oct" @if ($month=='Oct' ) selected @endif>October</option>
                                <option value="Nov" @if ($month=='Nov' ) selected @endif>November</option>
                                <option value="Dec" @if ($month=='Dec' ) selected @endif>December</option>
                            </select>
                        </div>
                        <div class="col-xl-3 ">
                            <label class="form-label"></label>
                            <div class="input-group">
                                <button class="btn btn-lg btn-danger" type="submit">Generate</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-4 col-md-4">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-rupiah-sign opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">Total Revenue</h6>
                        <span class="text-xs">YTD {{ $month }}, {{ $year }}</span>
                        <hr class="horizontal dark my-3">
                        <h6 class="mb-0">IDR {{ number_format(($ytdanchor + $ytdsectenant + $ytdreseller + $ytdproject)/1000000000) }}M</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-md-0 mt-4">
                <div class="card">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-secondary shadow text-center border-radius-lg">
                            <i class="fas fa-rupiah-sign opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">Total Revenue</h6>
                        <span class="text-xs">YTD {{ $month }}, {{ (\Carbon\Carbon::now()->year)-1 }}</span>
                        <hr class="horizontal dark my-3">
                        <h6 class="mb-0">IDR {{ number_format(($ytdachanchor + $ytdachsectenant + $ytdachreseller + $ytdachproject)/1000000000) }}M</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-warning shadow text-center border-radius-lg">
                            <i class="fas fa-trophy opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">Achievement</h6>
                        <span class="text-xs">YTD {{ $month }}, {{ $year }}</span>
                        <hr class="horizontal dark my-3">
                        <h6 class="mb-0">{{ number_format((float)($ytdanchor + $ytdsectenant + $ytdreseller + $ytdproject)/($ytdtaranchor + $ytdtarsectenant + $ytdtarreseller + $ytdtarproject)*100,2, '.', '') }}%</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-md-0 mt-4">
                <div class="card">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-info shadow text-center border-radius-lg">
                            <i class="fas fa-chart-line opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">Growth</h6>
                        <span class="text-xs">YTD {{ $month }}, {{ $year }}</span>
                        <hr class="horizontal dark my-3">
                        <h6 class="mb-0">{{ number_format((float)(($ytdanchor + $ytdsectenant + $ytdreseller + $ytdproject)/($ytdachanchor + $ytdachsectenant + $ytdachreseller + $ytdachproject + $ytdachdb + $ytdachms)-1)*100,2, '.', '') }}%</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-8">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                        <i class="fas fa-clipboard-list text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Revenue Report of YTD {{ $month }}, {{ $year }}</p>
                        <h5 class="font-weight-bolder mb-0">
                            IDR {{ number_format($ytdanchor + $ytdsectenant + $ytdreseller + $ytdproject) }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="summary-chart" class="chart-canvas" height="390"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-6 col-md-6">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-secondary shadow text-center border-radius-md">
                        <i class="fas fa-clipboard-list text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Revenue based on category</p>
                        <h5 class="font-weight-bolder mb-0">
                            IDR {{ number_format($ytdanchor + $ytdsectenant + $ytdreseller + $ytdproject) }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="category-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-info shadow text-center border-radius-md">
                        <i class="fas fa-sheet-plastic text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Revenue based on portofolio</p>
                        <h5 class="font-weight-bolder mb-0">
                            IDR {{ number_format($ytdanchor + $ytdsectenant + $ytdreseller + $ytdproject) }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="porto-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-6 col-md-6 mt-lg-0 mt-4">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-danger shadow text-center border-radius-md">
                        <i class="fas fa-tower-cell text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Revenue based on tenant</p>
                        <h5 class="font-weight-bolder mb-0">
                            IDR {{ number_format($ytdanchor + $ytdsectenant + $ytdreseller + $ytdproject) }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="tenant-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 mt-lg-0 mt-4">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                        <i class="fab fa-buffer text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Revenue based on sales segment</p>
                        <h5 class="font-weight-bolder mb-0">
                            IDR {{ number_format($ytdanchor + $ytdsectenant + $ytdreseller + $ytdproject) }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="segment-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-2 col-md-6">
        <div class="card">
            <div class="card-header mx-4 p-3 text-center">
                <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                    <i class="fas fa-anchor opacity-10" aria-hidden="true"></i>
                </div>
            </div>
            <div class="card-body pt-0 p-3 text-center">
                <h6 class="text-center mb-0">Anchor Tenant</h6>
                <span class="text-xs">{{ number_format((float)($anchor/$share)*100,2, '.', '' )}}%</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">IDR</h6>
                <h6 class="mb-0">{{ number_format($anchor) }}</h6>
                <span class="text-xs">MTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">IDR</h6>
                <h6 class="mb-0">{{ number_format($ytdanchor) }}</h6>
                <span class="text-xs">YTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">{{ number_format((float)($anchor/$taranchor)*100,2, '.', '') }}%</h6>
                <span class="text-xs">Ach MTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">{{ number_format((float)($ytdanchor/$ytdtaranchor)*100,2, '.', '') }}%</h6>
                <span class="text-xs">Ach YTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">{{ number_format((float)(($ytdanchor/$ytdachanchor)-1)*100,2, '.', '') }}%</h6>
                <span class="text-xs">YoY</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-6  mt-lg-0 mt-md-0 mt-4">
        <div class="card">
            <div class="card-header mx-4 p-3 text-center">
                <div class="icon icon-shape icon-lg bg-gradient-secondary shadow text-center border-radius-lg">
                    <i class="fab fa-paypal opacity-10" aria-hidden="true"></i>
                </div>
            </div>
            <div class="card-body pt-0 p-3 text-center">
                <h6 class="text-center mb-0">2nd Tenant</h6>
                <span class="text-xs">{{ number_format((float)($sectenant/$share)*100,2, '.', '' )}}%</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">IDR</h6>
                <h6 class="mb-0">{{ number_format($sectenant) }}</h6>
                <span class="text-xs">MTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">IDR</h6>
                <h6 class="mb-0">{{ number_format($ytdsectenant) }}</h6>
                <span class="text-xs">YTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">{{ number_format((float)($sectenant/$tarsectenant)*100,2, '.', '') }}%</h6>
                <span class="text-xs">Ach MTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">{{ number_format((float)($ytdsectenant/$ytdtarsectenant)*100,2, '.', '') }}%</h6>
                <span class="text-xs">Ach YTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">{{ number_format((float)(($ytdsectenant/$ytdachsectenant)-1)*100,2, '.', '') }}%</h6>
                <span class="text-xs">YoY</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-6 mt-lg-0 mt-md-4 mt-4">
        <div class="card">
            <div class="card-header mx-4 p-3 text-center">
                <div class="icon icon-shape icon-lg bg-gradient-success shadow text-center border-radius-lg">
                    <i class="fas fa-people-carry-box opacity-10" aria-hidden="true"></i>
                </div>
            </div>
            <div class="card-body pt-0 p-3 text-center">
                <h6 class="text-center mb-0">Reseller</h6>
                <span class="text-xs">{{ number_format((float)($reseller/$share)*100,2, '.', '' )}}%</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">IDR</h6>
                <h6 class="mb-0">{{ number_format($reseller) }}</h6>
                <span class="text-xs">MTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">IDR</h6>
                <h6 class="mb-0">{{ number_format($ytdreseller) }}</h6>
                <span class="text-xs">YTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">{{ number_format((float)($reseller/$tarreseller)*100,2, '.', '') }}%</h6>
                <span class="text-xs">Ach MTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">{{ number_format((float)($ytdreseller/$ytdtarreseller)*100,2, '.', '') }}%</h6>
                <span class="text-xs">Ach YTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">{{ number_format((float)(($ytdreseller/$ytdachreseller)-1)*100,2, '.', '') }}%</h6>
                <span class="text-xs">YoY</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-6 mt-lg-0 mt-md-4 mt-4">
        <div class="card">
            <div class="card-header mx-4 p-3 text-center">
                <div class="icon icon-shape icon-lg bg-gradient-info shadow text-center border-radius-lg">
                    <i class="fas fa-diagram-project opacity-10" aria-hidden="true"></i>
                </div>
            </div>
            <div class="card-body pt-0 p-3 text-center">
                <h6 class="text-center mb-0">Project Solution</h6>
                <span class="text-xs">{{ number_format((float)($project/$share)*100,2, '.', '' )}}%</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">IDR</h6>
                <h6 class="mb-0">{{ number_format($project) }}</h6>
                <span class="text-xs">MTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">IDR</h6>
                <h6 class="mb-0">{{ number_format($ytdproject) }}</h6>
                <span class="text-xs">YTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">{{ number_format((float)($project/$tarproject)*100,2, '.', '') }}%</h6>
                <span class="text-xs">Ach MTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">{{ number_format((float)($ytdproject/$ytdtarproject)*100,2, '.', '') }}%</h6>
                <span class="text-xs">Ach YTD</span>
                <hr class="horizontal dark my-3">
                <h6 class="mb-0">{{ number_format((float)(($ytdproject/$ytdachproject)-1)*100,2, '.', '') }}%</h6>
                <span class="text-xs">YoY</span>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 mt-lg-0 mt-md-4 mt-4">
        <div class="card">
            <div class="card-header mx-4 p-3 text-center">
                <div class="icon icon-shape icon-lg bg-gradient-warning shadow text-center border-radius-lg">
                    <i class="fas fa-stopwatch opacity-10" aria-hidden="true"></i>
                </div>
            </div>
            <div class="card-body pt-0 p-3 text-center">
                <h6 class="text-center mb-0">Unconsolidated</h6>
                <span class="text-xs">IDR {{ number_format($ytdanchor + $ytdsectenant + $ytdreseller + $ytdproject) }}</span>
                <hr class="horizontal dark my-3">
                <h5 class="mb-0">{{ number_format((float)($ytdanchor + $ytdsectenant + $ytdreseller + $ytdproject)/($ytdtaranchor + $ytdtarsectenant + $ytdtarreseller + $ytdtarproject)*100,2, '.', '') }}%</h5>
                <span class="text-xs">Ach</span>
                <hr class="horizontal dark my-3">
                <h5 class="mb-0">{{ number_format((float)(($ytdanchor + $ytdsectenant + $ytdreseller + $ytdproject)/($ytdachanchor + $ytdachsectenant + $ytdachreseller + $ytdachproject + $ytdachdb + $ytdachms)-1)*100,2, '.', '') }}%</h5>
                <span class="text-xs">Growth</span>
            </div>
        </div>
        <div class="card mt-4">
            <div class="card-body pt-0 p-3">
                <div class="table-responsive text-center">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                              <th class="text-xs text-center" scope="col">YTD</th>
                              <th class="text-xs" scope="col">Recurring</th>
                              <th class="text-xs" scope="col">Net Add</th>
                              <th class="text-xs" scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th class="text-xs text-center" scope="row">Anchor</th>
                                <td class="text-xs">{{ number_format($ytdanchorrec/1000000000) }}M</td>
                                <td class="text-xs">{{ number_format($ytdanchoradd/1000000000) }}M</td>
                                <td class="text-xs">{{ number_format(($ytdanchorrec + $ytdanchoradd)/1000000000) }}M</td>
                            </tr>
                            <tr>
                                <th class="text-xs text-center" scope="row">2nd</th>
                                <td class="text-xs">{{ number_format($ytdsectenantrec/1000000000) }}M</td>
                                <td class="text-xs">{{ number_format($ytdsectenantadd/1000000000) }}M</td>
                                <td class="text-xs">{{ number_format(($ytdsectenantrec + $ytdsectenantadd)/1000000000) }}M</td>
                            </tr>
                            <tr>
                                <th class="text-xs text-center" scope="row">Reseller</th>
                                <td class="text-xs">{{ number_format($ytdresellerrec/1000000000) }}M</td>
                                <td class="text-xs">{{ number_format($ytdreselleradd/1000000000) }}M</td>
                                <td class="text-xs">{{ number_format(($ytdresellerrec + $ytdreselleradd)/1000000000) }}M</td>
                            </tr>
                            <tr>
                                <td class="text-xs text-center" scope="row">Project</th>
                                    <td class="text-xs">{{ number_format($ytdprojectrec/1000000000) }}M</td>
                                    <td class="text-xs">{{ number_format($ytdprojectadd/1000000000) }}M</td>
                                    <td class="text-xs">{{ number_format(($ytdprojectrec + $ytdprojectadd)/1000000000) }}M</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-xs text-center" scope="row">Sum</th>
                                <td class="text-xs text-bold">{{ number_format(($ytdanchorrec + $ytdsectenantrec + $ytdresellerrec + $ytdprojectrec)/1000000000) }}M</td>
                                <td class="text-xs text-bold">{{ number_format(($ytdanchoradd + $ytdsectenantadd + $ytdreselleradd + $ytdprojectadd)/1000000000) }}M</td>
                                <td class="text-xs text-bold">{{ number_format(($ytdanchorrec + $ytdsectenantrec + $ytdresellerrec + $ytdprojectrec + $ytdanchoradd + $ytdsectenantadd + $ytdreselleradd + $ytdprojectadd)/1000000000) }}M</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header pb-0 p-3">
                <div class="d-flex align-items-center">
                    <h6 class="mb-0">YTD Tenant</h6>
                    <button type="button"
                        class="btn btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-auto"
                        data-bs-toggle="tooltip" data-bs-placement="bottom">
                        <i class="fas fa-info" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-5 text-center">
                        <div class="chart">
                            <canvas id="chart-ytdtenant" class="chart-canvas" height="394" width="545"
                                style="display: block; box-sizing: border-box; height: 197px; width: 165.1px;"></canvas>
                        </div>
                        <h4 class="font-weight-bold mt-n8">
                            <span>{{ number_format(($ytdanchorrec + $ytdsectenantrec + $ytdresellerrec + $ytdprojectrec + $ytdanchoradd + $ytdsectenantadd + $ytdreselleradd + $ytdprojectadd)/1000000000) }}M</span>
                            <span class="d-block text-body text-sm">YTD in %</span>
                        </h4>
                    </div>
                    <div class="col-7">
                        <div class="table-responsive">
                            <table class="table align-items-center mb-0">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-0">
                                                <span class="badge bg-red me-3"> </span>
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">Telkomsel</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="align-middle text-end text-sm">
                                            <span class="text-xs font-weight-bold">IDR {{ number_format($ytdtelkomsel) }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-0">
                                                <span class="badge bg-red me-3"> </span>
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">Telkom</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="align-middle text-end text-sm">
                                            <span class="text-xs font-weight-bold">IDR {{ number_format($ytdtelkom) }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-0">
                                                <span class="badge bg-blue me-3"> </span>
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">XL</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="align-middle text-end text-sm">
                                            <span class="text-xs font-weight-bold">IDR {{ number_format($ytdxl) }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-0">
                                                <span class="badge bg-yellow me-3"> </span>
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">Indosat</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="align-middle text-end text-sm">
                                            <span class="text-xs font-weight-bold">IDR {{ number_format($ytdisat) }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-0">
                                                <span class="badge bg-purple me-3"> </span>
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">Smartfren</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="align-middle text-end text-sm">
                                            <span class="text-xs font-weight-bold">IDR {{ number_format($ytdsf) }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-0">
                                                <span class="badge bg-dark me-3"> </span>
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">H3I</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="align-middle text-end text-sm">
                                            <span class="text-xs font-weight-bold">IDR {{ number_format($ytdsf) }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-0">
                                                <span class="badge bg-warning me-3"> </span>
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">Bakti</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="align-middle text-end text-sm">
                                            <span class="text-xs font-weight-bold">IDR {{ number_format($ytdbakti) }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-0">
                                                <span class="badge bg-secondary me-3"> </span>
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">Others</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="align-middle text-end text-sm">
                                            <span class="text-xs font-weight-bold">IDR {{ number_format($ytdothers) }}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body p-3 text-center">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <td></td>
                                <td></td>
                                <td class="font-weight-bold">Telkomsel</td>
                                <td class="font-weight-bold">Telkom</td>
                                <td class="font-weight-bold">H3I</td>
                                <td class="font-weight-bold">XL</td>
                                <td class="font-weight-bold">ISAT</td>
                                <td class="font-weight-bold">SF</td>
                                <td class="font-weight-bold">Others</td>
                                <td class="font-weight-bold">Bakti</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td rowspan="4" style="vertical-align : middle;text-align:center;" class="bg-primary text-white">Anchor Tenant</td>
                                <th>MTD</th>
                                <th>{{ number_format(($telkomselanchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($telkomanchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($h3ianchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($xlanchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($isatanchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($sfanchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($othersanchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($baktianchor/1000000000),2, '.', '') }}M</th>
                            </tr>
                            <tr>
                                <th>YTD</th>
                                <th>{{ number_format(($ytdtelkomselanchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdtelkomanchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdh3ianchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdxlanchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdisatanchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdsfanchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdothersanchor/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdbaktianchor/1000000000),2, '.', '') }}M</th>
                            </tr>
                            <tr>
                                <th>Share</th>
                                <th class="text-primary">{{ number_format(($ytdtelkomselanchor/($ytdtelkomselanchor + $ytdtelkomanchor + $ytdh3ianchor + $ytdxlanchor + $ytdsfanchor + $ytdothersanchor + $ytdbaktianchor  + $ytdisatanchor))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdtelkomanchor/($ytdtelkomselanchor + $ytdtelkomanchor + $ytdh3ianchor + $ytdxlanchor + $ytdsfanchor + $ytdothersanchor + $ytdbaktianchor  + $ytdisatanchor))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdh3ianchor/($ytdtelkomselanchor + $ytdtelkomanchor + $ytdh3ianchor + $ytdxlanchor + $ytdsfanchor + $ytdothersanchor + $ytdbaktianchor  + $ytdisatanchor))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdxlanchor/($ytdtelkomselanchor + $ytdtelkomanchor + $ytdh3ianchor + $ytdxlanchor + $ytdsfanchor + $ytdothersanchor + $ytdbaktianchor  + $ytdisatanchor))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdisatanchor/($ytdtelkomselanchor + $ytdtelkomanchor + $ytdh3ianchor + $ytdxlanchor + $ytdsfanchor + $ytdothersanchor + $ytdbaktianchor  + $ytdisatanchor))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdsfanchor/($ytdtelkomselanchor + $ytdtelkomanchor + $ytdh3ianchor + $ytdxlanchor + $ytdsfanchor + $ytdothersanchor + $ytdbaktianchor  + $ytdisatanchor))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdothersanchor/($ytdtelkomselanchor + $ytdtelkomanchor + $ytdh3ianchor + $ytdxlanchor + $ytdsfanchor + $ytdothersanchor + $ytdbaktianchor  + $ytdisatanchor))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdbaktianchor/($ytdtelkomselanchor + $ytdtelkomanchor + $ytdh3ianchor + $ytdxlanchor + $ytdsfanchor + $ytdothersanchor + $ytdbaktianchor  + $ytdisatanchor))*100,2, '.', '') }}%</th></th>
                            </tr>
                            <tr>
                                <th>YoY</th>
                                @if ($ytdtelkomselanchora == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdtelkomselanchor/$ytdtelkomselanchora)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdtelkomselanchor/$ytdtelkomselanchora)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdtelkomselanchor/$ytdtelkomselanchora)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdtelkomselanchor/$ytdtelkomselanchora)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdtelkomanchora == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdtelkomanchor/$ytdtelkomanchora)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdtelkomanchor/$ytdtelkomanchora)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdtelkomanchor/$ytdtelkomanchora)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdtelkomanchor/$ytdtelkomanchora)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdh3ianchora == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdh3ianchor/$ytdh3ianchora)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdh3ianchor/$ytdh3ianchora)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdh3ianchor/$ytdh3ianchora)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdh3ianchor/$ytdh3ianchora)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdxlanchora == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdxlanchor/$ytdxlanchora)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdxlanchor/$ytdxlanchora)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdxlanchor/$ytdxlanchora)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdxlanchor/$ytdxlanchora)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdisatanchora == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdisatanchor/$ytdisatanchora)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdisatanchor/$ytdisatanchora)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdisatanchor/$ytdisatanchora)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdisatanchor/$ytdisatanchora)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdsfanchora == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdsfanchor/$ytdsfanchora)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdsfanchor/$ytdsfanchora)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdsfanchor/$ytdsfanchora)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdsfanchor/$ytdsfanchora)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdothersanchora == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdothersanchor/$ytdothersanchora)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdothersanchor/$ytdothersanchora)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdothersanchor/$ytdothersanchora)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdothersanchor/$ytdothersanchora)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdbaktianchora == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdbaktianchor/$ytdbaktianchora)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdbaktianchor/$ytdbaktianchora)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdbaktianchor/$ytdbaktianchora)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdbaktianchor/$ytdbaktianchora)-1)*100, 2, '.', '') }}%</th>
                                @endif
                            </tr>
                            <tr>
                                <td rowspan="4" style="vertical-align : middle;text-align:center;" class="bg-secondary text-white">2nd Tenant</td>
                                <th>MTD</th>
                                <th>{{ number_format(($telkomsel2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($telkom2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($h3i2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($xl2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($isat2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($sf2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($others2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($bakti2nd/1000000000),2, '.', '') }}M</th>
                            </tr>
                            <tr>
                                <th>YTD</th>
                                <th>{{ number_format(($ytdtelkomsel2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdtelkom2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdh3i2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdxl2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdisat2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdsf2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdothers2nd/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdbakti2nd/1000000000),2, '.', '') }}M</th>
                            </tr>
                            <tr>
                                <th>Share</th>
                                <th class="text-primary">{{ number_format(($ytdtelkomsel2nd/($ytdtelkomsel2nd + $ytdtelkom2nd + $ytdh3i2nd + $ytdxl2nd + $ytdsf2nd + $ytdothers2nd + $ytdbakti2nd  + $ytdisat2nd))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdtelkom2nd/($ytdtelkomsel2nd + $ytdtelkom2nd + $ytdh3i2nd + $ytdxl2nd + $ytdsf2nd + $ytdothers2nd + $ytdbakti2nd  + $ytdisat2nd))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdh3i2nd/($ytdtelkomsel2nd + $ytdtelkom2nd + $ytdh3i2nd + $ytdxl2nd + $ytdsf2nd + $ytdothers2nd + $ytdbakti2nd  + $ytdisat2nd))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdxl2nd/($ytdtelkomsel2nd + $ytdtelkom2nd + $ytdh3i2nd + $ytdxl2nd + $ytdsf2nd + $ytdothers2nd + $ytdbakti2nd  + $ytdisat2nd))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdisat2nd/($ytdtelkomsel2nd + $ytdtelkom2nd + $ytdh3i2nd + $ytdxl2nd + $ytdsf2nd + $ytdothers2nd + $ytdbakti2nd  + $ytdisat2nd))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdsf2nd/($ytdtelkomsel2nd + $ytdtelkom2nd + $ytdh3i2nd + $ytdxl2nd + $ytdsf2nd + $ytdothers2nd + $ytdbakti2nd  + $ytdisat2nd))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdothers2nd/($ytdtelkomsel2nd + $ytdtelkom2nd + $ytdh3i2nd + $ytdxl2nd + $ytdsf2nd + $ytdothers2nd + $ytdbakti2nd  + $ytdisat2nd))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdbakti2nd/($ytdtelkomsel2nd + $ytdtelkom2nd + $ytdh3i2nd + $ytdxl2nd + $ytdsf2nd + $ytdothers2nd + $ytdbakti2nd  + $ytdisat2nd))*100,2, '.', '') }}%</th></th>
                            </tr>
                            <tr>
                                <th>YoY</th>
                                @if ($ytdtelkomsel2nda == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdtelkomsel2nd/$ytdtelkomsel2nda)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdtelkomsel2nd/$ytdtelkomsel2nda)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdtelkomsel2nd/$ytdtelkomsel2nda)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdtelkomsel2nd/$ytdtelkomsel2nda)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdtelkom2nda == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdtelkom2nd/$ytdtelkom2nda)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdtelkom2nd/$ytdtelkom2nda)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdtelkom2nd/$ytdtelkom2nda)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdtelkom2nd/$ytdtelkom2nda)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdh3i2nda == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdh3i2nd/$ytdh3i2nda)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdh3i2nd/$ytdh3i2nda)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdh3i2nd/$ytdh3i2nda)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdh3i2nd/$ytdh3i2nda)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdxl2nda == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdxl2nd/$ytdxl2nda)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdxl2nd/$ytdxl2nda)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdxl2nd/$ytdxl2nda)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdxl2nd/$ytdxl2nda)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdisat2nda == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdisat2nd/$ytdisat2nda)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdisat2nd/$ytdisat2nda)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdisat2nd/$ytdisat2nda)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdisat2nd/$ytdisat2nda)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdsf2nda == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdsf2nd/$ytdsf2nda)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdsf2nd/$ytdsf2nda)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdsf2nd/$ytdsf2nda)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdsf2nd/$ytdsf2nda)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdothers2nda == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdothers2nd/$ytdothers2nda)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdothers2nd/$ytdothers2nda)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdothers2nd/$ytdothers2nda)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdothers2nd/$ytdothers2nda)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdbakti2nda == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdbakti2nd/$ytdbakti2nda)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdbakti2nd/$ytdbakti2nda)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdbakti2nd/$ytdbakti2nda)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdbakti2nd/$ytdbakti2nda)-1)*100, 2, '.', '') }}%</th>
                                @endif
                            </tr>
                            <tr>
                                <td rowspan="4" style="vertical-align : middle;text-align:center;" class="bg-success text-white">Reseller</td>
                                <th>MTD</th>
                                <th>{{ number_format(($telkomselres/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($telkomres/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($h3ires/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($xlres/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($isatres/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($sfres/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($othersres/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($baktires/1000000000),2, '.', '') }}M</th>
                            </tr>
                            <tr>
                                <th>YTD</th>
                                <th>{{ number_format(($ytdtelkomselres/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdtelkomres/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdh3ires/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdxlres/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdisatres/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdsfres/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdothersres/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdbaktires/1000000000),2, '.', '') }}M</th>
                            </tr>
                            <tr>
                                <th>Share</th>
                                <th class="text-primary">{{ number_format(($ytdtelkomselres/($ytdtelkomselres + $ytdtelkomres + $ytdh3ires + $ytdxlres + $ytdsfres + $ytdothersres + $ytdbaktires  + $ytdisatres))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdtelkomres/($ytdtelkomselres + $ytdtelkomres + $ytdh3ires + $ytdxlres + $ytdsfres + $ytdothersres + $ytdbaktires  + $ytdisatres))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdh3ires/($ytdtelkomselres + $ytdtelkomres + $ytdh3ires + $ytdxlres + $ytdsfres + $ytdothersres + $ytdbaktires  + $ytdisatres))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdxlres/($ytdtelkomselres + $ytdtelkomres + $ytdh3ires + $ytdxlres + $ytdsfres + $ytdothersres + $ytdbaktires  + $ytdisatres))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdisatres/($ytdtelkomselres + $ytdtelkomres + $ytdh3ires + $ytdxlres + $ytdsfres + $ytdothersres + $ytdbaktires  + $ytdisatres))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdsfres/($ytdtelkomselres + $ytdtelkomres + $ytdh3ires + $ytdxlres + $ytdsfres + $ytdothersres + $ytdbaktires  + $ytdisatres))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdothersres/($ytdtelkomselres + $ytdtelkomres + $ytdh3ires + $ytdxlres + $ytdsfres + $ytdothersres + $ytdbaktires  + $ytdisatres))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdbaktires/($ytdtelkomselres + $ytdtelkomres + $ytdh3ires + $ytdxlres + $ytdsfres + $ytdothersres + $ytdbaktires  + $ytdisatres))*100,2, '.', '') }}%</th></th>
                            </tr>
                            <tr>
                                <th>YoY</th>
                                @if ($ytdtelkomselresa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdtelkomselres/$ytdtelkomselresa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdtelkomselres/$ytdtelkomselresa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdtelkomselres/$ytdtelkomselresa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdtelkomselres/$ytdtelkomselresa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdtelkomresa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdtelkomres/$ytdtelkomresa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdtelkomres/$ytdtelkomresa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdtelkomres/$ytdtelkomresa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdtelkomres/$ytdtelkomresa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdh3iresa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdh3ires/$ytdh3iresa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdh3ires/$ytdh3iresa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdh3ires/$ytdh3iresa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdh3ires/$ytdh3iresa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdxlresa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdxlres/$ytdxlresa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdxlres/$ytdxlresa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdxlres/$ytdxlresa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdxlres/$ytdxlresa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdisatresa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdisatres/$ytdisatresa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdisatres/$ytdisatresa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdisatres/$ytdisatresa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdisatres/$ytdisatresa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdsfresa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdsfres/$ytdsfresa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdsfres/$ytdsfresa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdsfres/$ytdsfresa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdsfres/$ytdsfresa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdothersresa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdothersres/$ytdothersresa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdothersres/$ytdothersresa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdothersres/$ytdothersresa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdothersres/$ytdothersresa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdbaktiresa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdbaktires/$ytdbaktiresa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdbaktires/$ytdbaktiresa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdbaktires/$ytdbaktiresa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdbaktires/$ytdbaktiresa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                            </tr>
                            <tr>
                                <td rowspan="4" style="vertical-align : middle;text-align:center;" class="bg-info text-white">Project Solution</td>
                                <th>MTD</th>
                                <th>{{ number_format(($telkomselpro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($telkompro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($h3ipro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($xlpro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($isatpro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($sfpro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($otherspro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($baktipro/1000000000),2, '.', '') }}M</th>
                            </tr>
                            <tr>
                                <th>YTD</th>
                                <th>{{ number_format(($ytdtelkomselpro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdtelkompro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdh3ipro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdxlpro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdisatpro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdsfpro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdotherspro/1000000000),2, '.', '') }}M</th>
                                <th>{{ number_format(($ytdbaktipro/1000000000),2, '.', '') }}M</th>
                            </tr>
                            <tr>
                                <th>Share</th>
                                <th class="text-primary">{{ number_format(($ytdtelkomselpro/($ytdtelkomselpro + $ytdtelkompro + $ytdh3ipro + $ytdxlpro + $ytdsfpro + $ytdotherspro + $ytdbaktipro  + $ytdisatpro))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdtelkompro/($ytdtelkomselpro + $ytdtelkompro + $ytdh3ipro + $ytdxlpro + $ytdsfpro + $ytdotherspro + $ytdbaktipro  + $ytdisatpro))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdh3ipro/($ytdtelkomselpro + $ytdtelkompro + $ytdh3ipro + $ytdxlpro + $ytdsfpro + $ytdotherspro + $ytdbaktipro  + $ytdisatpro))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdxlpro/($ytdtelkomselpro + $ytdtelkompro + $ytdh3ipro + $ytdxlpro + $ytdsfpro + $ytdotherspro + $ytdbaktipro  + $ytdisatpro))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdisatpro/($ytdtelkomselpro + $ytdtelkompro + $ytdh3ipro + $ytdxlpro + $ytdsfpro + $ytdotherspro + $ytdbaktipro  + $ytdisatpro))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdsfpro/($ytdtelkomselpro + $ytdtelkompro + $ytdh3ipro + $ytdxlpro + $ytdsfpro + $ytdotherspro + $ytdbaktipro  + $ytdisatpro))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdotherspro/($ytdtelkomselpro + $ytdtelkompro + $ytdh3ipro + $ytdxlpro + $ytdsfpro + $ytdotherspro + $ytdbaktipro  + $ytdisatpro))*100,2, '.', '') }}%</th>
                                <th class="text-primary">{{ number_format(($ytdbaktipro/($ytdtelkomselpro + $ytdtelkompro + $ytdh3ipro + $ytdxlpro + $ytdsfpro + $ytdotherspro + $ytdbaktipro  + $ytdisatpro))*100,2, '.', '') }}%</th></th>
                            </tr>
                            <tr>
                                <th>YoY</th>
                                @if ($ytdtelkomselproa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdtelkomselpro/$ytdtelkomselproa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdtelkomselpro/$ytdtelkomselproa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdtelkomselpro/$ytdtelkomselproa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdtelkomselpro/$ytdtelkomselproa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdtelkomproa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdtelkompro/$ytdtelkomproa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdtelkompro/$ytdtelkomproa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdtelkompro/$ytdtelkomproa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdtelkompro/$ytdtelkomproa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdh3iproa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdh3ipro/$ytdh3iproa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdh3ipro/$ytdh3iproa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdh3ipro/$ytdh3iproa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdh3ipro/$ytdh3iproa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdxlproa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdxlpro/$ytdxlproa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdxlpro/$ytdxlproa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdxlpro/$ytdxlproa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdxlpro/$ytdxlproa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdisatproa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdisatpro/$ytdisatproa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdisatpro/$ytdisatproa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdisatpro/$ytdisatproa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdisatpro/$ytdisatproa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdsfproa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdsfpro/$ytdsfproa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdsfpro/$ytdsfproa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdsfpro/$ytdsfproa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdsfpro/$ytdsfproa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdothersproa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdotherspro/$ytdothersproa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdotherspro/$ytdothersproa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdotherspro/$ytdothersproa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdotherspro/$ytdothersproa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                                @if ($ytdbaktiproa == 0)
                                <th class="text-danger">0.00%</th>
                                @elseif((($ytdbaktipro/$ytdbaktiproa)-1)*100 <= 0)
                                <th class="text-danger">{{ number_format((float)(($ytdbaktipro/$ytdbaktiproa)-1)*100, 2, '.', '') }}%</th>
                                @elseif((($ytdbaktipro/$ytdbaktiproa)-1)*100 > 0)
                                <th class="text-success">{{ number_format((float)(($ytdbaktipro/$ytdbaktiproa)-1)*100, 2, '.', '') }}%</th>
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        var ctx1 = document.getElementById("category-chart").getContext("2d");
        new Chart(ctx1, {
            type: "line",
            data: {
                labels: {!! $hasilbulanrev !!},
                datasets: [{
                        label: "Recurring",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#808080",
                        borderColor: "#808080",
                        borderWidth: 3,
                        data: {{ $hasilchartrec }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Net Add",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#3A416F",
                        borderColor: "#3A416F",
                        borderWidth: 3,
                        data: {{ $hasilchartadd }},
                        maxBarThickness: 6
                    },
                ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: true,
                        position: 'bottom',
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: false,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            padding: 10,
                            color: '#b2b9bf',
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            color: '#b2b9bf',
                            padding: 10,
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                },
            },
        });
        var ctx2 = document.getElementById("porto-chart").getContext("2d");
        new Chart(ctx2, {
            type: "line",
            data: {
                labels: {!! $hasilbulanrev !!},
                datasets: [{
                        label: "Anchor Tenant",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#5e72e4",
                        borderColor: "#5e72e4",
                        borderWidth: 3,
                        data: {{ $hasilchartanc }},
                        maxBarThickness: 6
                    },
                    {
                        label: "2nd Tenant",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#808080",
                        borderColor: "#808080",
                        borderWidth: 3,
                        data: {{ $hasilchart2nd }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Reseller",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#00FF00",
                        borderColor: "#00FF00",
                        borderWidth: 3,
                        data: {{ $hasilchartres }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Project Solution",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#00FFFF",
                        borderColor: "#00FFFF",
                        borderWidth: 3,
                        data: {{ $hasilchartpro }},
                        maxBarThickness: 6
                    },
                ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: true,
                        position: 'bottom',
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: false,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            padding: 10,
                            color: '#b2b9bf',
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            color: '#b2b9bf',
                            padding: 10,
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                },
            },
        });
        var ctx3 = document.getElementById("tenant-chart").getContext("2d");
        new Chart(ctx3, {
            type: "line",
            data: {
                labels: {!! $hasilbulanrev !!},
                datasets: [
                    {
                        label: "Telkomsel",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#ff0000",
                        borderColor: "#ff0000",
                        borderWidth: 3,
                        data: {{ $hasilchartsel }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Telkom",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#800000",
                        borderColor: "#800000",
                        borderWidth: 3,
                        data: {{ $hasilcharttel }},
                        maxBarThickness: 6
                    },
                    {
                        label: "XL",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#0000FF",
                        borderColor: "#0000FF",
                        borderWidth: 3,
                        data: {{ $hasilchartxl }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Indosat",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#f9c70c",
                        borderColor: "#f9c70c",
                        borderWidth: 3,
                        data: {{ $hasilchartsat }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Smartfren",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#800080",
                        borderColor: "#800080",
                        borderWidth: 3,
                        data: {{ $hasilchartsf }},
                        maxBarThickness: 6
                    },
                    {
                        label: "H3I",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#808080",
                        borderColor: "#808080",
                        borderWidth: 3,
                        data: {{ $hasilcharth3i }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Bakti",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#FFA500",
                        borderColor: "#FFA500",
                        borderWidth: 3,
                        data: {{ $hasilchartbak }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Others",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#404040",
                        borderColor: "#404040",
                        borderWidth: 3,
                        data: {{ $hasilchartoth }},
                        maxBarThickness: 6
                    },
                ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: true,
                        position: 'bottom',
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: false,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            padding: 10,
                            color: '#b2b9bf',
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            color: '#b2b9bf',
                            padding: 10,
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                },
            },
        });
        var ctx4 = document.getElementById("segment-chart").getContext("2d");
        new Chart(ctx4, {
            type: "line",
            data: {
                labels: {!! $hasilbulanrev !!},
                datasets: [{
                        label: "Sales 1",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#5e72e4",
                        borderColor: "#5e72e4",
                        borderWidth: 3,
                        data: {{ $hasilcharttg1 }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Sales 2",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#3A416F",
                        borderColor: "#3A416F",
                        borderWidth: 3,
                        data: {{ $hasilcharttg2 }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Sales 3",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#17c1e8",
                        borderColor: "#17c1e8",
                        borderWidth: 3,
                        data: {{ $hasilcharttg3 }},
                        maxBarThickness: 6
                    },
                ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: true,
                        position: 'bottom',
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: false,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            padding: 10,
                            color: '#b2b9bf',
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            color: '#b2b9bf',
                            padding: 10,
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                },
            },
        });
        var ctx5 = document.getElementById("chart-ytdtenant").getContext("2d");

        var gradientStroke1 = ctx5.createLinearGradient(0, 230, 0, 50);

        gradientStroke1.addColorStop(1, 'rgba(203,12,159,0.2)');
        gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
        gradientStroke1.addColorStop(0, 'rgba(203,12,159,0)'); //purple colors

        new Chart(ctx5, {
            type: "doughnut",
            data: {
                labels: ['Telkomsel', 'XL', 'Telkom', 'Indosat', 'Smartfren', 'H3I', 'Bakti', 'Others'],
                datasets: [{
                    label: "Rev YTD in %",
                    weight: 9,
                    cutout: 90,
                    tension: 0.9,
                    pointRadius: 2,
                    borderWidth: 2,
                    backgroundColor: ['red', 'blue', 'red', 'purple', 'black', 'brown', 'orange', 'grey'],
                    data: [{{ number_format((float)($ytdtelkomsel/$ytdtenanttotal)*100,2, '.', '') }}, {{ number_format((float)($ytdxl/$ytdtenanttotal)*100,2, '.', '') }}, {{number_format((float)($ytdtelkom/$ytdtenanttotal)*100,2, '.', '') }}, {{ number_format((float)($ytdisat/$ytdtenanttotal)*100,2, '.', '') }}, {{ number_format((float)($ytdsf/$ytdtenanttotal)*100,2, '.', '') }}, {{ number_format((float)($ytdh3i/$ytdtenanttotal)*100,2, '.', '') }}, {{ number_format((float)($ytdbakti/$ytdtenanttotal)*100,2, '.', '') }}, {{ number_format((float)($ytdothers/$ytdtenanttotal)*100,2, '.', '') }}],
                    fill: false
                }],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false,
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: false
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: false,
                        }
                    },
                },
            },
        });
        var ctx6 = document.getElementById("summary-chart").getContext("2d");
        new Chart(ctx6, {
            type: "line",
            data: {
                labels: {!! $hasilbulanrev !!},
                datasets: [{
                        label: "Total Revenue 2021",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#5e72e4",
                        borderColor: "#5e72e4",
                        borderWidth: 3,
                        data: {{ $hasilchartrav }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Total Revenue 2022",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#3A416F",
                        borderColor: "#3A416F",
                        borderWidth: 3,
                        data: {{ $hasilchartrev }},
                        maxBarThickness: 6
                    },
                    {
                        label: "Target Revenue 2022",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 2,
                        pointBackgroundColor: "#17c1e8",
                        borderColor: "#17c1e8",
                        borderWidth: 3,
                        data: {{ $hasilcharttar }},
                        maxBarThickness: 6
                    },
                ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: true,
                        position: 'bottom',
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: false,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            padding: 10,
                            color: '#b2b9bf',
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            color: '#b2b9bf',
                            padding: 10,
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                },
            },
        });
    </script>
@endpush