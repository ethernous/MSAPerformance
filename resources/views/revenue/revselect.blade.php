@extends('layouts.app')
@section('content')
    <div class="row mb-5">
        <div class="col-xl-12">
            <div class="row">
              <div class="col-xl-6 col-lg-8 mx-auto my-4">
                <div class="card">
                  <div class="card-body">
                    <div class="text-center">
                        <h2>Mitratel's Revenue Report</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--form panels-->
            <div class="row mt-4">
              <div class="col-xl-6 col-lg-8 m-auto">
                <div class="card">
                    <div class="card-header">
                        <h5>Select revenue report you want to see!</h5>
                    </div>
                    <div class="card-body pt-0">
                        <form action="/revresult" method="get">
                            <div class="row">
                                <div class="col-xl-12">
                                    <label class="form-label">Year : </label>
                                    <select name="year" class="form-select input-group">
                                        <option value="no">Select Year from the list below</option>
                                        <option value="2022">2022</option>
                                    </select>
                                </div>
                                <div class="col-xl-12">
                                    <label class="form-label">Month : </label>
                                    <select name="month" class="form-select input-group">
                                        <option value="no">Select month from the list below</option>
                                        <option value="Jan">January</option>
                                        <option value="Feb">February</option>
                                        <option value="Mar">March</option>
                                        <option value="Apr">April</option>
                                        <option value="May">May</option>
                                        <option value="Jun">June</option>
                                        <option value="Jul">July</option>
                                        <option value="Aug">August</option>
                                        <option value="Sep">September</option>
                                        <option value="Oct">October</option>
                                        <option value="Nov">November</option>
                                        <option value="Des">December</option>
                                    </select>
                                </div>
                                <div class="col-xl-2">
                                    <label class="form-label"></label>
                                    <div class="input-group">
                                        <button class="btn btn-lg btn-danger" type="submit">Generate</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
