@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-8 m-auto">
        <div class="card">
            <div class="card-body pt-0">
                <form action="/trresult" method="get">
                    <div class="row mt-4">
                        <div class="col-lg-5">
                            <label class="form-label">Year : </label>
                            <select name="year" class="form-select input-group">
                                <option value="no">Select Year from the list below</option>
                                {{-- <option value="2020" @if ($year==2020) selected @endif>2020</option>
                                <option value="2021" @if ($year==2021) selected @endif>2021</option> --}}
                                <option value="2022" @if ($year==2022) selected @endif>2022</option>
                            </select>
                        </div>
                        <div class="col-lg-5">
                            <label class="form-label">Month : </label>
                            <select name="month" class="form-select input-group">
                                <option value="no">Select month from the list below</option>
                                <option value="Jan" @if ($month=='Jan' ) selected @endif>January</option>
                                <option value="Feb" @if ($month=='Feb' ) selected @endif>February</option>
                                <option value="Mar" @if ($month=='Mar' ) selected @endif>March</option>
                                <option value="Apr" @if ($month=='Apr' ) selected @endif>April</option>
                                <option value="May" @if ($month=='May' ) selected @endif>May</option>
                                <option value="Jun" @if ($month=='Jun' ) selected @endif>June</option>
                                <option value="Jul" @if ($month=='Jul' ) selected @endif>July</option>
                                <option value="Aug" @if ($month=='Aug' ) selected @endif>August</option>
                                <option value="Sep" @if ($month=='Sep' ) selected @endif>September</option>
                                <option value="Oct" @if ($month=='Oct' ) selected @endif>October</option>
                                <option value="Nov" @if ($month=='Nov' ) selected @endif>November</option>
                                <option value="Dec" @if ($month=='Dec' ) selected @endif>December</option>
                            </select>
                        </div>
                        {{-- <div class="col-lg-4">
                            <label class="form-label">Week : </label>
                            <select name="week" class="form-select input-group">
                                <option value="no">Select week from the list below</option>
                                <option value="W1" @if ($week=='W1' ) selected @endif>First Week</option>
                                <option value="W2" @if ($week=='W2' ) selected @endif>Second Week</option>
                                <option value="W3" @if ($week=='W3' ) selected @endif>Third Week</option>
                                <option value="W4" @if ($week=='W4' ) selected @endif>Forth Week</option>
                            </select>
                        </div> --}}
                        <div class="col-lg-2">
                            <label class="form-label">Click to generate report!</label>
                            <div class="input-group">
                                <button class="btn btn-danger" type="submit">Generate</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-12 col-md-12">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-danger shadow text-center border-radius-md">
                        <i class="fas fa-file-contract text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Total Asset/Tenant up to {{ $year }}</p>
                        <h5 class="font-weight-bolder mb-0">
                            {{ $sumtotalaset }} Towers, {{ $totalaset }} Tenants
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="bar-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-12 col-md-12">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-danger shadow text-center border-radius-md">
                        <i class="fas fa-file-contract text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Asset Comparison Monthly</p>
                        <h5 class="font-weight-bolder mb-0">
                            Up to {{ $month }}, {{ $year }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="comparison-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-6 col-md-6">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                        <i class="fas fa-clipboard-list text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">TR Category Monthly</p>
                        <h5 class="font-weight-bolder mb-0">
                            Up to {{ $month }}, {{ $year }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="netadd-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                        <i class="fas fa-sheet-plastic text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">TR Scope of Work Monthly</p>
                        <h5 class="font-weight-bolder mb-0">
                            Up to {{ $month }}, {{ $year }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="porto-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-6 col-md-6 mt-lg-0 mt-4">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                        <i class="fas fa-percent text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Tower Ratio</p>
                        <h5 class="font-weight-bolder mb-0">
                            YTD {{ $month }}, {{ $year }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="ratio-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 mt-lg-0 mt-4">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                        <i class="fas fa-location-dot text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Tenant per Area</p>
                        <h5 class="font-weight-bolder mb-0">
                            YTD {{ $month }}, {{ $year }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="area-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-4 col-md-4 mt-lg-0 mt-4">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                        <i class="fas fa-chart-pie text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Tower Demografy</p>
                        <h5 class="font-weight-bolder mb-0">
                            YTD {{ $month }}, {{ $year }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="demografi-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 mt-lg-0 mt-4">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                        <i class="fas fa-tower-cell text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Tower Market</p>
                        <h5 class="font-weight-bolder mb-0">
                            YTD {{ $month }}, {{ $year }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="totype-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 mt-lg-0 mt-4">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                        <i class="fas fa-network-wired text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Fiberization</p>
                        <h5 class="font-weight-bolder mb-0">
                            YTD {{ $month }}, {{ $year }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 p-0">
                <div class="chart">
                    <canvas id="fiber-chart" class="chart-canvas" height="250"></canvas>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-12 col-md-12 mt-lg-0 mt-4">
        <div class="card overflow-hidden">
            <div class="card-header p-3 pb-0">
                <div class="d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                        <i class="fas fa-chart-pie text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                    <div class="ms-3">
                        <p class="text-sm mb-0 text-uppercase font-weight-bold">Monthly TR Report</p>
                        <h5 class="font-weight-bolder mb-0">
                            YTD {{ $month }}, {{ $year }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body p-3 text-center">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <td rowspan = "2"></td>
                                <td rowspan = "2" style="vertical-align : middle;text-align:center;">FY 2021</td>
                                <td rowspan = "2" style="vertical-align : middle;text-align:center;">YTD {{ $month }} 2021</td>
                                <td colspan = "3" style="vertical-align : middle;text-align:center;">Net Add YTD</td>
                                <td colspan = "3" style="vertical-align : middle;text-align:center;">Cumulative YTD</td>
                                <td rowspan = "2" style="vertical-align : middle;text-align:center;">YoY 2022</td>
                            </tr>
                            <tr>
                                <td>Target</td>
                                <td>Real</td>
                                <td>Ach</td>
                                <td>Target</td>
                                <td>Real</td>
                                <td>Ach</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>MTEL Unc</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Colo MTEL Unc</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Tenant MTEL Unc</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>TR MTEL Unc</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        var ctx1 = document.getElementById("netadd-chart").getContext("2d");

        var gradientStroke1 = ctx1.createLinearGradient(0, 230, 0, 50);

        gradientStroke1.addColorStop(1, 'rgba(50,205,50, 0.2)');
        gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
        gradientStroke1.addColorStop(0, 'rgba(50,205,50,0)'); //purple colors

        var gradientStroke2 = ctx1.createLinearGradient(0, 230, 0, 50);

        gradientStroke2.addColorStop(1, 'rgba(34,139,34,0.2)');
        gradientStroke2.addColorStop(0.2, 'rgba(72,72,176,0.0)');
        gradientStroke2.addColorStop(0, 'rgba(34,139,34,0)'); //purple colors

        new Chart(ctx1, {
            type: "line",
            data: {
                labels: {!! $hasillabelchart !!},
                datasets: [{
                        label: "Existing",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 0,
                        borderColor: "#32CD32",
                        borderWidth: 3,
                        backgroundColor: gradientStroke1,
                        fill: true,
                        data: {{ $hasilchartexisting }},
                        maxBarThickness: 6

                    },
                    {
                        label: "Net Add",
                        tension: 0.4,
                        borderWidth: 0,
                        pointRadius: 0,
                        borderColor: "	#228B22",
                        borderWidth: 3,
                        backgroundColor: gradientStroke2,
                        fill: true,
                        data: {{ $hasilchartnetadd}},
                        maxBarThickness: 6
                    },
                ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: true,
                        position: 'bottom',
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: false,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            padding: 10,
                            color: '#b2b9bf',
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            color: '#b2b9bf',
                            padding: 10,
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                },
            },
        });
        var ctx2 = document.getElementById("porto-chart").getContext("2d");

        var gradientStroke1 = ctx2.createLinearGradient(0, 230, 0, 50);

        gradientStroke1.addColorStop(1, 'rgba(94, 114, 228, 0.2)');
        gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
        gradientStroke1.addColorStop(0, 'rgba(94, 114, 228,0)'); //purple colors

        var gradientStroke2 = ctx2.createLinearGradient(0, 230, 0, 50);

        gradientStroke2.addColorStop(1, 'rgba(20,23,39,0.2)');
        gradientStroke2.addColorStop(0.2, 'rgba(72,72,176,0.0)');
        gradientStroke2.addColorStop(0, 'rgba(20,23,39,0)'); //purple colors

        new Chart(ctx2, {
            data: {
            labels: {!! $hasillabelchart !!},
            datasets: [{
                type: "bar",
                label: "Colo",
                weight: 5,
                tension: 0.4,
                borderWidth: 0,
                pointBackgroundColor: "#32CD32",
                borderColor: "#32CD32",
                backgroundColor: '#32CD32',
                borderRadius: 4,
                borderSkipped: false,
                data: {{ $hasilchartcolo }},
                maxBarThickness: 10,
                },{
                type: "bar",
                label: "Macro",
                weight: 5,
                tension: 0.4,
                borderWidth: 0,
                pointBackgroundColor: "#228B22",
                borderColor: "#228B22",
                backgroundColor: '#228B22',
                borderRadius: 4,
                borderSkipped: false,
                data: {{ $hasilchartmacro }},
                maxBarThickness: 10,
                }
            ],
            },
            options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
                legend: {
                display: true,
                position: 'bottom',
                }
            },
            interaction: {
                intersect: false,
                mode: 'index',
            },
            scales: {
                y: {
                grid: {
                    drawBorder: false,
                    display: true,
                    drawOnChartArea: true,
                    drawTicks: false,
                    borderDash: [5, 5]
                },
                ticks: {
                    display: true,
                    padding: 10,
                    color: '#b2b9bf',
                    font: {
                    size: 11,
                    family: "Open Sans",
                    style: 'normal',
                    lineHeight: 2
                    },
                }
                },
                x: {
                grid: {
                    drawBorder: false,
                    display: true,
                    drawOnChartArea: true,
                    drawTicks: true,
                    borderDash: [5, 5]
                },
                ticks: {
                    display: true,
                    color: '#b2b9bf',
                    padding: 10,
                    font: {
                    size: 11,
                    family: "Open Sans",
                    style: 'normal',
                    lineHeight: 2
                    },
                }
                },
            },
            },
        });
        var ctx3 = document.getElementById("ratio-chart").getContext("2d");

        new Chart(ctx3, {
            type: "pie",
            data: {
                labels: ['0 Tenant', '1 Tenant', '2 Tenants', '3 Tenants', '4 Tenants', '5 Tenants', '6 Tenants'],
                datasets: [{
                    label: "Sum of PID",
                    weight: 9,
                    cutout: 0,
                    tension: 0.9,
                    pointRadius: 2,
                    borderWidth: 2,
                    backgroundColor: ['#20B2AA', '#3CB371', '#00FF00', '#32CD32', '#228B22', '#006400'],
                    data: [{{ $dismantle }}, {{ $satutenant }}, {{ $duatenant }}, {{ $tigatenant }}, {{ $empattenant }}, {{ $limatenant }}, {{ $enamtenant }}],
                    fill: false
                }],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: true,
                        position: 'right',
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: false
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: false,
                        }
                    },
                },
            },
        });
        var ctx4 = document.getElementById("area-chart").getContext("2d");

        new Chart(ctx4, {
            type: "pie",
            data: {
                labels: ['Area 1', 'Area 2', 'Area 3', 'Area 4'],
                datasets: [{
                    label: "Sum of PID",
                    weight: 9,
                    cutout: 0,
                    tension: 0.9,
                    pointRadius: 2,
                    borderWidth: 2,
                    backgroundColor: ['#00FF00', '#32CD32', '#228B22', '#006400'],
                    data: [{{ $a1 }}, {{ $a2 }}, {{ $a3 }}, {{ $a4 }}],
                    fill: false
                }],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: true,
                        position: 'right',
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: false
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: false,
                        }
                    },
                },
            },
        });
        var ctx5 = document.getElementById("demografi-chart").getContext("2d");

        new Chart(ctx5, {
            type: "pie",
            data: {
                labels: ['Rural', 'Urban', 'Sub Urban'],
                datasets: [{
                    label: "Projects",
                    weight: 9,
                    cutout: 0,
                    tension: 0.9,
                    pointRadius: 2,
                    borderWidth: 2,
                    backgroundColor: ['#5e72e4', '#3A416F', '#a8b8d8'],
                    data: [{{ $rural }}, {{ $urban }}, {{ $suburban }}],
                    fill: false
                }],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false,
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: false
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: false,
                        }
                    },
                },
            },
        });
        
        var ctx6 = document.getElementById("totype-chart").getContext("2d");

        new Chart(ctx6, {
            type: "pie",
            data: {
                labels: ['0-50jt', '50-100jt', '100-150jt', '150-200jt', '> 200jt'],
                datasets: [{
                    label: "Tower Type",
                    weight: 9,
                    cutout: 0,
                    tension: 0.9,
                    pointRadius: 2,
                    borderWidth: 2,
                    backgroundColor: ['#17c1e8', '#5e72e4', '#3A416F', '#a8b8d8', '#5F9EA0'],
                    data: [{{ $mar50jt }}, {{ $mar100jt }}, {{ $mar150jt }}, {{ $mar200jt }}, {{ $mara200jt }}],
                    fill: false
                }],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false,
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: false
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: false,
                        }
                    },
                },
            },
        });
        
        var ctx7 = document.getElementById("fiber-chart").getContext("2d");

        new Chart(ctx7, {
            type: "pie",
            data: {
                labels: ['NY Fiber', 'FO Telkom'],
                datasets: [{
                    label: "Projects",
                    weight: 9,
                    cutout: 0,
                    tension: 0.9,
                    pointRadius: 2,
                    borderWidth: 2,
                    backgroundColor: ['#3A416F', '#5e72e4'],
                    data: [{{ $nyfibre }}, {{ $fotelkom }}],
                    fill: false
                }],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false,
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: false
                        }
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: false,
                        }
                    },
                },
            },
        });
        var ctx8 = document.getElementById("bar-chart").getContext("2d");
        var gradientStroke1 = ctx8.createLinearGradient(0, 230, 0, 50);

        gradientStroke1.addColorStop(1, 'rgba(20,23,39,0.2)');
        gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
        gradientStroke1.addColorStop(0, 'rgba(20,23,39,0)'); //purple colors

        var gradientStroke2 = ctx8.createLinearGradient(0, 230, 0, 50);

        gradientStroke2.addColorStop(1, 'rgba(203,12,159,0.2)');
        gradientStroke2.addColorStop(0.2, 'rgba(72,72,176,0.0)');
        gradientStroke2.addColorStop(0, 'rgba(203,12,159,0)'); //purple colors
        new Chart(ctx8, {
            data: {
            labels: {!! $hasillabelaset !!},
            datasets: [{
                type: "bar",
                label: "Tower Sum",
                weight: 5,
                tension: 0.4,
                borderWidth: 0,
                pointBackgroundColor: "#DC143C",
                borderColor: "#DC143C",
                backgroundColor: '#DC143C',
                borderRadius: 4,
                borderSkipped: false,
                data: {{ $hasilchartaset }},
                maxBarThickness: 10,
                },{
                type: "bar",
                label: "Tenant Sum",
                weight: 5,
                tension: 0.4,
                borderWidth: 0,
                pointBackgroundColor: "#808080",
                borderColor: "#808080",
                backgroundColor: '#808080',
                borderRadius: 4,
                borderSkipped: false,
                data: {{ $hasilcharttenant }},
                maxBarThickness: 10,
                },{
                type: "line",
                label: "Tower Asset Sum",
                tension: 0.4,
                borderWidth: 0,
                pointRadius: 0,
                pointBackgroundColor: "#DC143C",
                borderColor: "#DC143C",
                borderWidth: 3,
                backgroundColor: gradientStroke2,
                data: {{ $hasiltotalchart }},
                fill: true,
                },{
                type: "line",
                label: "Tower Tenant Sum",
                tension: 0.4,
                borderWidth: 0,
                pointRadius: 0,
                pointBackgroundColor: "#808080",
                borderColor: "#808080",
                borderWidth: 3,
                backgroundColor: gradientStroke1,
                data: {{ $hasiltotaltenant }},
                fill: true,
                }
            ],
            },
            options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
                legend: {
                display: true,
                position: 'bottom',
                }
            },
            interaction: {
                intersect: false,
                mode: 'index',
            },
            scales: {
                y: {
                grid: {
                    drawBorder: false,
                    display: true,
                    drawOnChartArea: true,
                    drawTicks: false,
                    borderDash: [5, 5]
                },
                ticks: {
                    display: true,
                    padding: 10,
                    color: '#b2b9bf',
                    font: {
                    size: 11,
                    family: "Open Sans",
                    style: 'normal',
                    lineHeight: 2
                    },
                }
                },
                x: {
                grid: {
                    drawBorder: false,
                    display: true,
                    drawOnChartArea: true,
                    drawTicks: true,
                    borderDash: [5, 5]
                },
                ticks: {
                    display: true,
                    color: '#b2b9bf',
                    padding: 10,
                    font: {
                    size: 11,
                    family: "Open Sans",
                    style: 'normal',
                    lineHeight: 2
                    },
                }
                },
            },
            },
        });
        var ctx9 = document.getElementById("comparison-chart").getContext("2d");

        var gradientStroke1 = ctx9.createLinearGradient(0, 230, 0, 50);

        gradientStroke1.addColorStop(1, 'rgba(94, 114, 228, 0.2)');
        gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
        gradientStroke1.addColorStop(0, 'rgba(94, 114, 228,0)'); //purple colors

        var gradientStroke2 = ctx9.createLinearGradient(0, 230, 0, 50);

        gradientStroke2.addColorStop(1, 'rgba(203,12,159,0.2)');
        gradientStroke2.addColorStop(0.2, 'rgba(72,72,176,0.0)');
        gradientStroke2.addColorStop(0, 'rgba(203,12,159,0)'); //purple colors

        new Chart(ctx9, {
            data: {
            labels: {!! $hasillabelchart !!},
            datasets: [{
                type: "bar",
                label: "2022 Asset",
                weight: 5,
                tension: 0.4,
                borderWidth: 0,
                pointBackgroundColor: "#DC143C",
                borderColor: "#DC143C",
                backgroundColor: '#DC143C',
                borderRadius: 4,
                borderSkipped: false,
                data: {{ $hasilmtdtrnew }},
                maxBarThickness: 10,
                },{
                type: "bar",
                label: "2021 Asset",
                weight: 5,
                tension: 0.4,
                borderWidth: 0,
                pointBackgroundColor: "#808080",
                borderColor: "#808080",
                backgroundColor: '#808080',
                borderRadius: 4,
                borderSkipped: false,
                data: {{ $hasilmtdtrold }},
                maxBarThickness: 10,
                },{
                type: "line",
                label: "Tower Asset Sum",
                tension: 0.4,
                borderWidth: 0,
                pointRadius: 0,
                pointBackgroundColor: "#DC143C",
                borderColor: "#DC143C",
                borderWidth: 3,
                backgroundColor: gradientStroke2,
                data: {{ $hasiltargettr }},
                fill: true,
                }
            ],
            },
            options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
                legend: {
                display: true,
                position: 'bottom',
                }
            },
            interaction: {
                intersect: false,
                mode: 'index',
            },
            scales: {
                y: {
                grid: {
                    drawBorder: false,
                    display: true,
                    drawOnChartArea: true,
                    drawTicks: false,
                    borderDash: [5, 5]
                },
                ticks: {
                    display: true,
                    padding: 10,
                    color: '#b2b9bf',
                    font: {
                    size: 11,
                    family: "Open Sans",
                    style: 'normal',
                    lineHeight: 2
                    },
                }
                },
                x: {
                grid: {
                    drawBorder: false,
                    display: true,
                    drawOnChartArea: true,
                    drawTicks: true,
                    borderDash: [5, 5]
                },
                ticks: {
                    display: true,
                    color: '#b2b9bf',
                    padding: 10,
                    font: {
                    size: 11,
                    family: "Open Sans",
                    style: 'normal',
                    lineHeight: 2
                    },
                }
                },
            },
            },
        });
    </script>
@endpush