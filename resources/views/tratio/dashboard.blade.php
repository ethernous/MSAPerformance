@extends('layouts.app')

@section('content')
<div class="row  mt-n2">
    <div class="col-xl-8 col-lg-7">
        <div class="row">
            <div class="col-sm-3">
                <div class="card overflow-hidden">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-tower-cell opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">TR Achievement</h6>
                        <span class="text-xs">YTD {{ \Carbon\Carbon::now()->format('M Y') }}</span>
                        <hr class="horizontal dark my-3">
                        <h5 class="mb-0">{{ $ytdtrnew }}</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 mt-sm-0 mt-4">
                <div class="card overflow-hidden">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-tower-cell opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">TR Achievement</h6>
                        <span class="text-xs">YTD {{ \Carbon\Carbon::now()->subWeeks(52)->format('M Y') }}</span>
                        <hr class="horizontal dark my-3">
                        <h5 class="mb-0">{{ $ytdtrold }}</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 mt-sm-0 mt-4">
                <div class="card overflow-hidden">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-bullseye opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">TR Target</h6>
                        <span class="text-xs">YTD {{ \Carbon\Carbon::now()->format('M Y') }}</span>
                        <hr class="horizontal dark my-3">
                        <h5 class="mb-0">{{ $targettr }}</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 mt-sm-0 mt-4">
                <div class="card overflow-hidden">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-arrow-up-right-dots opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">TR Growth</h6>
                        <span class="text-xs">YTD {{ \Carbon\Carbon::now()->format('M Y') }}</span>
                        <hr class="horizontal dark my-3">
                        <a href="/saleresult?year={{ \Carbon\Carbon::now()->format('Y') }}&month={{ \Carbon\Carbon::now()->format('M') }}&week=no"><h5 class="mb-0">{{ number_format((float)(($ytdtrnew/$ytdtrold)-1)*100, 2, '.', '') }}%</h5></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-5 mt-lg-0 mt-4">
        <div class="row">
            <div class="col-lg-12">
                
                    <div class="overflow-hidden position-relative border-radius-lg bg-cover h-100"
                        style="background-image: url(https://www.mitratel.co.id/wp-content/uploads/2022/03/Solusi-Kami-2c-1-1024x811.jpg);">
                        <span class="mask bg-gradient-dark"></span>
                        <div class="card-body position-relative z-index-1 h-100 p-3">
                            <h6 class="text-white font-weight-bolder mb-3">Hey {{ Auth::User()->name }}!</h6>
                            <p class="text-white mb-3">Welcome to TR dashboard, since you are a/an {{ Auth::User()->jabatan }}, you gained access to {{ Auth::User()->jobdesc }} pages. Feel free to contact the developer if any error occurs!</p>
                            <hr>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-2 col-sm-6">
        <div class="card bg-gradient-danger h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">TR Category</p>
                </div>
                <i class="fa-solid fa-box-archive fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Net Add</p>
                <span class="text-xs text-white">{{ $netadd }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-danger h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">TR Category</p>
                </div>
                <i class="fa-solid fa-box-archive fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Existing</p>
                <span class="text-xs text-white">{{ $existing }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Tenancy Ratio</p>
                </div>
                <i class="fa-solid fa-percent fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Dismantle</p>
                <span class="text-xs text-white">{{ $dismantle }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Tenancy Ratio</p>
                </div>
                <i class="fa-solid fa-percent fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">1 Tenant</p>
                <span class="text-xs text-white">{{ $satutenant }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Tenancy Ratio</p>
                </div>
                <i class="fa-solid fa-percent fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">2 Tenant</p>
                <span class="text-xs text-white">{{ $duatenant }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Tenancy Ratio</p>
                </div>
                <i class="fa-solid fa-percent fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">3 Tenant</p>
                <span class="text-xs text-white">{{ $tigatenant }} PID</span>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-2 col-sm-6">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Tenancy Ratio</p>
                </div>
                <i class="fa-solid fa-percent fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">4 Tenant</p>
                <span class="text-xs text-white">{{ $empattenant }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Tenancy Ratio</p>
                </div>
                <i class="fa-solid fa-percent fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">5 Tenant</p>
                <span class="text-xs text-white">{{ $limatenant }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Tenancy Ratio</p>
                </div>
                <i class="fa-solid fa-percent fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">6 Tenant</p>
                <span class="text-xs text-white">{{ $enamtenant }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-danger h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">TR Demography</p>
                </div>
                <i class="fa-solid fa-mountain-city fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Rural Area</p>
                <span class="text-xs text-white">{{ $rural }} Tower</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-danger h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">TR Demography</p>
                </div>
                <i class="fa-solid fa-mountain-city fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Urban Area</p>
                <span class="text-xs text-white">{{ $urban }} Tower</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-danger h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">TR Demography</p>
                </div>
                <i class="fa-solid fa-mountain-city fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Sub-Urban Area</p>
                <span class="text-xs text-white">{{ $suburban }} Tower</span>
            </div>
        </div>
    </div>
</div>
@endsection