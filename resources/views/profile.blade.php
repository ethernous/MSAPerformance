@extends('layouts.app')
@section('content')
<div class="row mb-5">
    <div class="col-lg-12 mt-lg-0 mt-4">
      <!-- Card Profile -->
      <div class="card card-body" id="profile">
        <div class="row justify-content-center align-items-center">
          <div class="col-sm-auto col-4">
            <div class="avatar avatar-xl position-relative">
              <img src="{{ asset('img/youth.png') }}" alt="bruce" class="w-100 border-radius-lg shadow-sm">
            </div>
          </div>
          <div class="col-sm-auto col-8 my-auto">
            <div class="h-100">
              <h5 class="mb-1 font-weight-bolder">
                {{ Auth::User()->name }}
              </h5>
              <p class="mb-0 font-weight-bold text-sm">
                {{ Auth::User()->jabatan }}
              </p>
            </div>
          </div>
          <div class="col-sm-auto ms-sm-auto mt-sm-0 mt-3 d-flex">
            <div class="form-check-label mb-0">
                <span style="color:green;">●</span>
            </div>
            <label class="form-check-label mb-0">
              <small>
                Online
              </small>
            </label>
          </div>
        </div>
      </div>
      <!-- Card Basic Info -->
      <div class="card mt-4" id="basic-info">
        <div class="card-header">
          <h5>User Informations</h5>
        </div>
        <div class="card-body pt-0">
          <div class="row">
            <form method="POST" action="{{ route('profile.update') }}" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <input type="hidden" name="_method" value="PUT">

                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group focused">
                                <label class="form-control-label" for="name">Name<span class="small text-danger">*</span></label>
                                <input type="text" id="name" class="form-control" name="name" placeholder="Name" value="{{ old('name', Auth::user()->name) }}">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label" for="email">Email Address<span class="small text-danger">*</span></label>
                                <input type="email" id="email" class="form-control" name="email" placeholder="example@example.com" value="{{ old('email', Auth::user()->email) }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group focused">
                                <label class="form-control-label" for="current_password">Current Password</label>
                                <input type="password" id="current_password" class="form-control" name="current_password" placeholder="Current password">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group focused">
                                <label class="form-control-label" for="new_password">New Password</label>
                                <input type="password" id="new_password" class="form-control" name="new_password" placeholder="New password">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group focused">
                                <label class="form-control-label" for="confirm_password">Password Confirmation</label>
                                <input type="password" id="confirm_password" class="form-control" name="password_confirmation" placeholder="Confirm password">
                            </div>
                        </div>
                    </div>
                </div>
                <h5 class="mt-5">Password requirements</h5>
                <p class="text-muted mb-2">
                    Please follow this guide for a strong password:
                </p>
                <ul class="text-muted ps-4 mb-0 float-start">
                    <li>
                    <span class="text-sm">One special characters</span>
                    </li>
                    <li>
                    <span class="text-sm">Min 6 characters</span>
                    </li>
                    <li>
                    <span class="text-sm">One number (2 are recommended)</span>
                    </li>
                    <li>
                    <span class="text-sm">Change it often</span>
                    </li>
                </ul>

                <!-- Button -->
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col text-center">
                            <button type="submit" class="btn bg-gradient-dark btn-sm float-end mt-6 mb-0">Save Data</button>
                        </div>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('scripts')
@if ($message = Session::get('success'))
<script>
Swal.fire(
'Good job!',
'{{ $message }}',
'success'
)
</script>
@endif 
@endpush