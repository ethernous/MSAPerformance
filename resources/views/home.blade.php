<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" href="https://www.mitratel.co.id/wp-content/uploads/2021/01/cropped-Asset-6@4x-180x180.png">
    <link rel="icon" sizes="192x192" type="image/png" href="https://www.mitratel.co.id/wp-content/uploads/2021/01/cropped-Asset-6@4x-192x192.png">
    <title>
        {{ config('app.name', 'Performance Dashboard | Mitratel') }}
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="{{ asset('css/nucleo-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="{{ asset('css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="{{ asset('css/argon-dashboard.css?v=2.0.1') }}" rel="stylesheet" />
</head>

<body class="error-page">
  <main class="main-content  mt-0">
    <div class="page-header min-vh-100" style="background-image: url('{{ asset('img/illustrations/404.svg') }}');">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6 col-md-7 mx-auto text-center">
            <h2 class="text-bolder text-primary">We Are Loading Your Credentials</h2>
            <h2>Please wait for a moment</h2>
            <p class="lead">If it tooks more than 5 seconds please click here!</p>
            <button type="button" class="btn bg-gradient-dark mt-4">Go to Homepage</button>
          </div>
        </div>
      </div>
    </div>
  </main>
  <!-- -------- END FOOTER 3 w/ COMPANY DESCRIPTION WITH LINKS & SOCIAL ICONS & COPYRIGHT ------- -->
  <!--   Core JS Files   -->
  <script src="{{ asset('js/core/popper.min.js') }}"></script>
  <script src="{{ asset('js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/plugins/perfect-scrollbar.min.js') }}"></script>
  <script src="{{ asset('js/plugins/smooth-scrollbar.min.js') }}"></script>
  <!-- Kanban scripts -->
  <script src="{{ asset('js/plugins/dragula/dragula.min.js') }}"></script>
  <script src="{{ asset('js/plugins/jkanban/jkanban.js') }}"></script>
  <script src="{{ asset('js/plugins/chartjs.min.js') }}"></script>
  <script>
      var win = navigator.platform.indexOf('Win') > -1;
      if (win && document.querySelector('#sidenav-scrollbar')) {
          var options = {
              damping: '0.5'
          }
          Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
      }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('js/argon-dashboard.min.js?v=2.0.1') }}"></script>
  @if (Auth::User()->jobdesc == 'all')
  <script>
      window.setTimeout(function () {
          window.location.href = '/sales/dashboard';
      }, 2000);
  </script>
  @elseif(Auth::User()->jobdesc == 'Sales')
  <script>
      window.setTimeout(function () {
          window.location.href = '/sales/dashboard';
      }, 2000);
  </script>
  @elseif(Auth::User()->jobdesc == 'Rev')
  <script>
      window.setTimeout(function () {
          window.location.href = '/rev/dashboard';
      }, 2000);
  </script>
  @elseif(Auth::User()->jobdesc == 'TR')
  <script>
      window.setTimeout(function () {
          window.location.href = '/tratio/dashboard';
      }, 2000);
  </script>
  @endif
</body>

</html>