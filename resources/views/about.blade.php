@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-lg-12 mb-1">
                        <div class="text-center">
                            <h5 class="font-weight-bold">Mitratel Performance Dashboard</h5>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-lg-12">
                        <h5 class="font-weight-bold">Credits</h5>
                        <p>This dashboard is created to fulfill the necessity of performance report for PT. Dayamitra Telekomunikasi (Mitratel) | Marketing Strategy & Analytics at 2022. This dashboard is developed using:</p>
                        <ul>
                            <li><a href="https://laravel.com" target="_blank">Laravel</a> - Open source PHP framework.</li>
                            <li><a href="https://www.creative-tim.com/product/argon-dashboard" target="_blank">Argon Dashboard 2</a> - Argon Dashboard 2 Pro Admin Template.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection