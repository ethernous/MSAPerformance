@extends('layouts.auth')

@section('content')
<div class="page-header min-vh-100">
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger border-left-danger" role="alert">
                <ul class="pl-4 my-2">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
                <div class="card card-plain">
                    <div class="card-header pb-0 text-start">
                        <img src="{{ asset('img/logo-2@4x.png') }}" alt="" width="100%" height="auto">
                        <hr>
                        <h4 class="font-weight-bolder">Performance Dashboard</h4>
                        <p class="mb-0">Enter your email and password to sign in</p>
                    </div>
                    <div class="card-body">
                        <form role="form" method="POST" action="{{ route('login') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="mb-3">
                                <input type="email" class="form-control form-control-lg" placeholder="Email" name="email" aria-label="Email" required>
                            </div>
                            <div class="mb-3">
                                <input type="password" class="form-control form-control-lg" placeholder="Password" name="password" aria-label="Password" required>
                            </div>
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" id="rememberMe">
                                <label class="form-check-label" for="rememberMe">Remember me</label>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-lg btn-danger btn-lg w-100 mt-4 mb-0">Sign In</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer text-center pt-0 px-lg-2 px-1">
                        <p class="mb-4 text-sm mx-auto">
                            Don't have an account?
                            <a href="#" data-bs-toggle="modal" data-bs-target="#modal-contact" class="text-primary text-gradient font-weight-bold">Click Here!</a>
                        </p>
                    </div>
                </div>
            </div>
            <div
                class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column">
                <div class="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center overflow-hidden"
                    style="background-image: url('https://www.mitratel.co.id/wp-content/uploads/2022/03/sebaran-lokasi-menara.png');
                    background-size: cover;">
                    <span class="mask bg-gradient-danger opacity-8"></span>
                    <h4 class="mt-5 text-white font-weight-bolder position-relative">"Potential Everywhere"
                        <hr>
                    </h4>
                    <p class="text-white position-relative">Amanah, Kompeten, Harmonis, Loyal, Adaptif, Kolaboratif</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
