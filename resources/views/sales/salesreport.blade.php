@extends('layouts.app')
@section('content')
@if ($tahun == 'no' && $bulan =='no' && $pekan =='no')
<main class="main-content  mt-0">
  <div class="page-header min-vh-100">
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-lg-6 col-md-7 mx-auto text-center">
                  <h1 class="display-1 text-bolder text-dark">Error 404</h1>
                  <h4>Hello there. You did not input year and month data!</h2>
                  <p class="lead">We suggest you to <b>input year and month data</b></p>
                  <a class="btn btn-danger" href="/salerep">Return</a>
              </div>
          </div>
      </div>
  </div>
</main>
@elseif ($tahun != 'no' && $bulan =='no' && $pekan =='no')
<main class="main-content  mt-0">
  <div class="page-header min-vh-100">
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-lg-6 col-md-7 mx-auto text-center">
                  <h1 class="display-1 text-bolder text-dark">Error 404</h1>
                  <h4>Hello there. You did not input month data!</h2>
                  <p class="lead">We suggest you to <b>input month data</b></p>
                  <a class="btn btn-danger" href="/salerep">Return</a>
              </div>
          </div>
      </div>
  </div>
</main> 
@elseif ($tahun != 'no' && $bulan !='no')
<div class="row">
    <div class="col-lg-12 mb-lg-0 mb-4">
        <div class="card">
            <div class="card-header">
                <h5>Select sales report you want to see!</h5>
            </div>
            <div class="card-body pt-0">
                <form action="#" method="get">
                    <div class="row">
                        <div class="col-lg-2">
                            <label class="form-label">Year : </label>
                            <select name="year" class="form-select input-group">
                                <option value="no">Select Year from the list below</option>
                                {{-- <option value="2020" @if ($tahun==2020) selected @endif>2020</option> --}}
                                <option value="2021" @if ($tahun==2021) selected @endif>2021</option>
                                <option value="2022" @if ($tahun==2022) selected @endif>2022</option>
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label class="form-label">Month : </label>
                            <select name="month" class="form-select input-group">
                                <option value="no">Select month from the list below</option>
                                <option value="Jan" @if ($bulan=='Jan' ) selected @endif>January</option>
                                <option value="Feb" @if ($bulan=='Feb' ) selected @endif>February</option>
                                <option value="Mar" @if ($bulan=='Mar' ) selected @endif>March</option>
                                <option value="Apr" @if ($bulan=='Apr' ) selected @endif>April</option>
                                <option value="May" @if ($bulan=='May' ) selected @endif>May</option>
                                <option value="Jun" @if ($bulan=='Jun' ) selected @endif>June</option>
                                <option value="Jul" @if ($bulan=='Jul' ) selected @endif>July</option>
                                <option value="Aug" @if ($bulan=='Aug' ) selected @endif>August</option>
                                <option value="Sep" @if ($bulan=='Sep' ) selected @endif>September</option>
                                <option value="Oct" @if ($bulan=='Oct' ) selected @endif>October</option>
                                <option value="Nov" @if ($bulan=='Nov' ) selected @endif>November</option>
                                <option value="Dec" @if ($bulan=='Dec' ) selected @endif>December</option>
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label class="form-label">Week : </label>
                            <select name="week" class="form-select input-group">
                                <option value="no">Select week from the list below</option>
                                <option value="W1" @if ($pekan=='W1' ) selected @endif>First Week</option>
                                <option value="W2" @if ($pekan=='W2' ) selected @endif>Second Week</option>
                                <option value="W3" @if ($pekan=='W3' ) selected @endif>Third Week</option>
                                <option value="W4" @if ($pekan=='W4' ) selected @endif>Forth Week</option>
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <label class="form-label">Click to generate report!</label>
                            <div class="input-group">
                                <button class="btn btn-danger" type="submit">Generate</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    @if ($tahun == 2022)
    <div class="row mt-4">
        <div class="col-lg-8 col-md-8">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-danger shadow text-center border-radius-md">
                            <i class="fas fa-clipboard-check text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">MTD Total Sales</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $b2sco + $coloco + $reseco + $b2sns + $colons + $resens}} PID
                            </h5>
                        </div>
                        <div class="progress-wrapper ms-auto w-25">
                          <p class="text-sm mb-0 text-uppercase font-weight-bold">YTD Total Sales</p>
                          <h5 class="font-weight-bolder mb-0">{{ $cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres }} PID
                          </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="total-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 mt-sm-0 mt-4">
            <div class="card h-100">
                <div class="card-header pb-0 p-3">
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="mb-0">Sales Summary</h6>
                        </div>
                        <div class="col-md-6 d-flex justify-content-end align-items-center">
                            <i class="far fa-calendar-alt me-2" aria-hidden="true"></i>
                            <small>{{ $bulan }} {{ $tahun }}</small>
                        </div>
                    </div>
                </div>
                <div class="card-body p-3">
                    <ul class="list-group">
                        <li class="list-group-item border-0 justify-content-between ps-0 pb-0 border-radius-lg">
                            <div class="d-flex">
                                @if ($tarb2s + $tarcolo + $tarres == 0)
                                <div class="d-flex align-items-center">
                                    <button class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-down" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Achievement MTD</h6>
                                        <span class="text-xs">{{ $b2stotal + $colototal + $resetotal }} PID of {{ $tarb2s + $tarcolo + $tarres }} PID</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">0.00 %</div>
                                @elseif (($b2stotal + $colototal + $resetotal)/($tarb2s + $tarcolo + $tarres)*100 < 100)
                                <div class="d-flex align-items-center">
                                    <button class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-down" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Achievement MTD</h6>
                                        <span class="text-xs">{{ $b2stotal + $colototal + $resetotal }} PID of {{ $tarb2s + $tarcolo + $tarres }} PID</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">{{ number_format((float)(($b2stotal + $colototal + $resetotal)/($tarb2s + $tarcolo + $tarres))*100, 2, '.', '') }}%</div>
                                @else
                                <div class="d-flex align-items-center">
                                    <button class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-up" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Achievement MTD</h6>
                                        <span class="text-xs">{{ $b2stotal + $colototal + $resetotal }} PID of {{ $tarb2s + $tarcolo + $tarres }} PID</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-success text-gradient text-sm font-weight-bold ms-auto">{{ number_format((float)(($b2stotal + $colototal + $resetotal)/($tarb2s + $tarcolo + $tarres))*100, 2, '.', '') }}%</div>
                                @endif
                            </div>
                            <hr class="horizontal dark mt-3 mb-2">
                        </li>
                        <li class="list-group-item border-0 justify-content-between ps-0 pb-0 border-radius-lg">
                            <div class="d-flex">
                                @if ($cumtarb2s + $cumtarcolo + $cumtarres == 0)
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-down" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Achievement YTD</h6>
                                        <span class="text-xs">0</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">
                                    0.00 %
                                </div>
                                @elseif (($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($cumtarb2s + $cumtarcolo + $cumtarres)*100 <= 100)
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-down" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Achievement YTD</h6>
                                        <span class="text-xs">{{ $cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres }} PID of {{ $cumtarb2s + $cumtarcolo + $cumtarres }} PID</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">
                                  {{ number_format((float)($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($cumtarb2s + $cumtarcolo + $cumtarres)*100, 2, '.', '') }}%
                                </div>
                                @else
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-up" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Achievement YTD</h6>
                                        <span class="text-xs">{{ $cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres }} PID of {{ $cumtarb2s + $cumtarcolo + $cumtarres }} PID</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-success text-gradient text-sm font-weight-bold ms-auto">
                                  {{ number_format((float)($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($cumtarb2s + $cumtarcolo + $cumtarres)*100, 2, '.', '') }}%
                                </div>
                                @endif
                            </div>
                            <hr class="horizontal dark mt-3 mb-2">
                        </li>
                        <li class="list-group-item border-0 justify-content-between ps-0 mb-2 border-radius-lg">
                            @if ($sumb2s + $sumcolo + $sumrese == 0)
                            <div class="d-flex">
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-down" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Growth</h6>
                                        <span class="text-xs">Year on Year</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">
                                    0.00%
                                </div>
                            </div>
                            @elseif ((($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($sumb2s + $sumcolo + $sumrese)-1)*100 < 0)
                            <div class="d-flex">
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-down" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Growth</h6>
                                        <span class="text-xs">Year on Year</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">
                                  {{ number_format((float)(($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($sumb2s + $sumcolo + $sumrese)-1)*100, 2, '.', '') }}%
                                </div>
                            </div>
                            @else
                            <div class="d-flex">
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-up" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Growth</h6>
                                        <span class="text-xs">Year on Year</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-success text-gradient text-sm font-weight-bold ms-auto">
                                  {{ number_format((float)(($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($sumb2s + $sumcolo + $sumrese)-1)*100, 2, '.', '') }}%
                                </div>
                            </div>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="card">
            <div class="card-header pb-0 p-3">
            <div class="d-flex align-items-center">
                <h6 class="mb-0">Tenant by Customer (Cumulative)</h6>
                <button type="button"
                class="btn btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-auto"
                data-bs-toggle="tooltip" data-bs-placement="left" title="The Amount of Customer Cumulatively by Tenant">
                <i class="fas fa-info"></i>
                </button>
            </div>
            </div>
            <div class="card-body p-3">
            <div class="row">
                <div class="col-5 text-center">
                <div class="chart">
                    <canvas id="chart-cumulative" class="chart-canvas" height="197"></canvas>
                </div>
                <h4 class="font-weight-bold mt-n8">
                    <span>{{ $totalchart }}</span>
                    <span class="d-block text-body text-sm">Total Sales</span>
                </h4>
                </div>
                <div class="col-7">
                <div class="table-responsive">
                    <table class="table align-items-center mb-0">
                    <tbody>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-red me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Telkomsel</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($telkomsel/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-primary me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">XL</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($xl/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-purple me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Smartfren</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($sf/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-red me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Telkom</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($telkom/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-yellow me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Indosat</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($isat/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-default me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Hutchison 3</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($h3i/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-secondary me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Others</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($oth/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
        <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="card">
            <div class="card-header pb-0 p-3">
            <div class="d-flex align-items-center">
                <h6 class="mb-0">Net Add Sales by Customer</h6>
                <button type="button"
                class="btn btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-auto"
                data-bs-toggle="tooltip" data-bs-placement="left" title="The Amount of New Sales Cumulatively by Tenant">
                <i class="fas fa-info"></i>
                </button>
            </div>
            </div>
            <div class="card-body p-3">
            <div class="row">
                <div class="col-5 text-center">
                <div class="chart">
                    <canvas id="chart-netadd" class="chart-canvas" height="197"></canvas>
                </div>
                <h4 class="font-weight-bold mt-n8">
                    <span>{{ $totalcharta }}</span>
                    <span class="d-block text-body text-sm">New Sales</span>
                </h4>
                </div>
                <div class="col-7">
                <div class="table-responsive">
                    <table class="table align-items-center mb-0">
                    <tbody>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-red me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Telkomsel</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($telkomsela/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-primary me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">XL</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($xla/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-purple me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Smartfren</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($sfa/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-red me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Telkom</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($telkoma/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-yellow me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Indosat</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($isata/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-default me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Hutchison 3</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($h3ia/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-secondary me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Others</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($otha/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-danger shadow text-center border-radius-md">
                            <i class="fas fa-people-carry-box text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">MTD Carry Over</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $b2sco + $coloco + $reseco }} PID
                            </h5>
                        </div>
                        <div class="progress-wrapper ms-auto w-25">
                          <p class="text-sm mb-0 text-uppercase font-weight-bold">YTD Carry Over</p>
                          <h5 class="font-weight-bolder mb-0">{{ $cumcob2s + $cumcocolo + $cumcores }} PID
                          </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="co-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-danger shadow text-center border-radius-md">
                            <i class="fas fa-clipboard-list text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">MTD New Sales</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $b2sns + $colons + $resens }} PID
                            </h5>
                        </div>
                        <div class="progress-wrapper ms-auto w-25">
                          <p class="text-sm mb-0 text-uppercase font-weight-bold">YTD New Sales</p>
                          <h5 class="font-weight-bolder mb-0">{{ $cumnsb2s + $cumnscolo + $cumnsres }} PID
                          </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="ns-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-4 col-md-4">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-warning shadow text-center border-radius-md">
                            <i class="fas fa-tower-cell text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">B2S</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $b2snowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="b2s-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-warning shadow text-center border-radius-md">
                            <i class="fas fa-tower-cell text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Colo</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $colonowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="colo-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-warning shadow text-center border-radius-md">
                            <i class="fas fa-tower-cell text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Reseller</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $resenowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="reseller-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                            <i class="fas fa-sign-hanging text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Area 1</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $a1nowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="area1-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                            <i class="fas fa-sign-hanging text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Area 2</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $a2nowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="area2-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                            <i class="fas fa-sign-hanging text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Area 3</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $a3nowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="area3-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                            <i class="fas fa-sign-hanging text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Area 4</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $a4nowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="area4-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="numbers">
                                @if ($pekan == 'no')
                                <p class="text-sm mb-0 text-uppercase font-weight-bold text-dark">Sales Report for {{ $bulan }} {{ $tahun }}</p>
                                @else
                                <p class="text-sm mb-0 text-uppercase font-weight-bold text-dark">Sales Report for {{$pekan}} of {{ $bulan }} {{ $tahun }}</p>    
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <td class="text-center" style="vertical-align:middle;" rowspan="2">SoW</td>
                                            <td class="text-center" style="vertical-align:middle;" rowspan="2">MTD {{ $bulan }} 2021</td>
                                            <td class="text-center" colspan="5">Net Add Sales MTD</td>
                                            <td class="text-center" colspan="5">Cumulative</td>
                                            <td class="text-center" style="vertical-align:middle;" rowspan="2">YoY</td>
                                        </tr>
                                        <tr>
                                            
                                            <td class="text-center">Target</td>
                                            <td class="text-center">CO</td>
                                            <td class="text-center">New Sales</td>
                                            <td class="text-center">Total</td>
                                            <td class="text-center">Ach (%)</td>
                                            <td class="text-center">Target</td>
                                            <td class="text-center">CO</td>
                                            <td class="text-center">New Sales</td>
                                            <td class="text-center">Total</td>
                                            <td class="text-center">Ach (%)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">B2S</td>
                                            <td class="text-center">{{ $sumb2s }}</td>
                                            <td class="text-center">{{ $tarb2s }}</td>
                                            <td class="text-center">{{ $b2sco }}</td>
                                            <td class="text-center">{{ $b2sns }}</td>
                                            <td class="text-center">{{ $b2stotal }}</td>
                                            @if ($tarb2s == 0)
                                            <td class="text-center text-success">100.00 %</td>
                                            @elseif ($b2stotal/$tarb2s*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)($b2stotal/$tarb2s)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($b2stotal/$tarb2s)*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="text-center">{{ $cumtarb2s }}</td>
                                            <td class="text-center">{{ $cumcob2s }}</td>
                                            <td class="text-center">{{ $cumnsb2s }}</td>
                                            <td class="text-center">{{ $cumcob2s + $cumnsb2s }}</td>
                                            @if ($cumtarb2s == 0)
                                            <td class="text-center text-success">100.00 %</td>
                                            @elseif (((($cumcob2s + $cumnsb2s)/$cumtarb2s)*100) <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcob2s + $cumnsb2s)/$cumtarb2s)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcob2s + $cumnsb2s)/$cumtarb2s)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($sumb2s == 0)
                                            <td class="text-center text-success">100.00 %</td>
                                            @elseif ((((($cumcob2s + $cumnsb2s)/$sumb2s)-1)*100) <= 0)
                                            <td class="text-center text-danger">{{ number_format((float)((($cumcob2s + $cumnsb2s)/$sumb2s)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)((($cumcob2s + $cumnsb2s)/$sumb2s)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                            
                                        </tr>
                                        <tr>
                                            <td class="text-center">Colo</td>
                                            <td class="text-center">{{ $sumcolo }}</td>
                                            <td class="text-center">{{ $tarcolo }}</td>
                                            <td class="text-center">{{ $coloco }}</td>
                                            <td class="text-center">{{ $colons }}</td>
                                            <td class="text-center">{{ $colototal }}</td>
                                            @if ($tarcolo == 0)
                                            <td class="text-center text-success">100.00 %</td>
                                            @elseif ($colototal/$tarcolo*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)($colototal/$tarcolo)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($colototal/$tarcolo)*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="text-center">{{ $cumtarcolo }}</td>
                                            <td class="text-center">{{ $cumcocolo }}</td>
                                            <td class="text-center">{{ $cumnscolo }}</td>
                                            <td class="text-center">{{ $cumcocolo + $cumnscolo }}</td>
                                            @if ($cumtarcolo == 0)
                                            <td class="text-center text-success">100.00 %</td>
                                            @elseif (((($cumcocolo + $cumnscolo)/$cumtarcolo)*100) <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcocolo + $cumnscolo)/$cumtarcolo)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcocolo + $cumnscolo)/$cumtarcolo)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($sumcolo == 0)
                                            <td class="text-center text-success">100.00 %</td>
                                            @elseif ((((($cumcocolo + $cumnscolo)/$sumcolo)-1)*100) < 0)
                                            <td class="text-center text-danger">{{ number_format((float)((($cumcocolo + $cumnscolo)/$sumcolo)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)((($cumcocolo + $cumnscolo)/$sumcolo)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="text-center">Reseller</td>
                                            <td class="text-center">{{ $sumrese }}</td>
                                            <td class="text-center">{{ $tarres }}</td>
                                            <td class="text-center">{{ $reseco }}</td>
                                            <td class="text-center">{{ $resens }}</td>
                                            <td class="text-center">{{ $resetotal }}</td>
                                            @if ($tarres == 0)
                                            <td class="text-center text-success">100.00%</td>
                                            @elseif ($resetotal/$tarres*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)($resetotal/$tarres)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($resetotal/$tarres)*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="text-center">{{ $cumtarres }}</td>
                                            <td class="text-center">{{ $cumcores }}</td>
                                            <td class="text-center">{{ $cumnsres }}</td>
                                            <td class="text-center">{{ $cumcores + $cumnsres }}</td>
                                            @if ($cumtarres == 0)
                                            <td class="text-center text-success">100.00%</td>
                                            @elseif (((($cumcores + $cumnsres)/$cumtarres)*100) <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcores + $cumnsres)/$cumtarres)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcores + $cumnsres)/$cumtarres)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($sumrese == 0)
                                            <td class="text-center text-success">100.00%</td>
                                            @elseif ((((($cumcores + $cumnsres)/$sumrese)-1)*100) < 0)
                                            <td class="text-center text-danger">{{ number_format((float)((($cumcores + $cumnsres)/$sumrese)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)((($cumcores + $cumnsres)/$sumrese)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold text-dark text-center">Total</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $sumb2s + $sumcolo + $sumrese }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $tarb2s + $tarcolo + $tarres }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $b2sco + $coloco + $reseco }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $b2sns + $colons + $resens }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $b2stotal + $colototal + $resetotal }}</td>
                                            @if ($tarb2s + $tarcolo + $tarres == 0)
                                            <td class="font-weight-bold text-center text-success">100.00 %</td>
                                            @elseif (($b2stotal + $colototal + $resetotal)/($tarb2s + $tarcolo + $tarres)*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($b2stotal + $colototal + $resetotal)/($tarb2s + $tarcolo + $tarres))*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($b2stotal + $colototal + $resetotal)/($tarb2s + $tarcolo + $tarres))*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="font-weight-bold text-dark text-center">{{ $cumtarb2s + $cumtarcolo + $cumtarres }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $cumcob2s + $cumcocolo + $cumcores }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $cumnsb2s + $cumnscolo + $cumnsres }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres }}</td>
                                            @if ($cumtarb2s + $cumtarcolo + $cumtarres == 0)
                                            <td class="text-center text-success">100.00 %</td>
                                            @elseif (($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($cumtarb2s + $cumtarcolo + $cumtarres)*100 <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($cumtarb2s + $cumtarcolo + $cumtarres)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($cumtarb2s + $cumtarcolo + $cumtarres)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($sumb2s + $sumcolo + $sumrese == 0)
                                            <td class="text-center text-success">100.00%</td>
                                            @elseif ((($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($sumb2s + $sumcolo + $sumrese)-1)*100 < 0)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($sumb2s + $sumcolo + $sumrese)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($sumb2s + $sumcolo + $sumrese)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif($tahun == 2021)
    <div class="row mt-4">
        <div class="col-lg-8 col-md-8">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-danger shadow text-center border-radius-md">
                            <i class="fas fa-clipboard-check text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">MTD Total Sales</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $macroco + $microco + $coloco + $reseco + $macrons + $microns + $colons + $resens}} PID
                            </h5>
                        </div>
                        <div class="progress-wrapper ms-auto w-25">
                          <p class="text-sm mb-0 text-uppercase font-weight-bold">YTD Total Sales</p>
                          <h5 class="font-weight-bolder mb-0">{{ $cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres }} PID
                          </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="total-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 mt-sm-0 mt-4">
            <div class="card h-100">
                <div class="card-header pb-0 p-3">
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="mb-0">Sales Summary</h6>
                        </div>
                        <div class="col-md-6 d-flex justify-content-end align-items-center">
                            <i class="far fa-calendar-alt me-2" aria-hidden="true"></i>
                            <small>{{ $bulan }} {{ $tahun }}</small>
                        </div>
                    </div>
                </div>
                <div class="card-body p-3">
                    <ul class="list-group">
                        <li class="list-group-item border-0 justify-content-between ps-0 pb-0 border-radius-lg">
                            <div class="d-flex">
                                @if ($tarmacro + $tarmicro + $tarcolo + $tarres == 0)
                                <div class="d-flex align-items-center">
                                    <button class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-up" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Achievement MTD</h6>
                                        <span class="text-xs">{{ $macrototal + $micrototal + $colototal + $resetotal }} PID of {{ $tarmacro + $tarmicro  + $tarcolo + $tarres }} PID</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">0.00 %</div>
                                @elseif (($macrototal + $micrototal + $colototal + $resetotal)/($tarmacro + $tarmicro + $tarcolo + $tarres)*100 < 100)
                                <div class="d-flex align-items-center">
                                    <button class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-up" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Achievement MTD</h6>
                                        <span class="text-xs">{{$macrototal + $micrototal + $colototal + $resetotal }} PID of {{ $tarmacro + $tarmicro + $tarcolo + $tarres }} PID</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">{{ number_format((float)(($macrototal + $micrototal + $colototal + $resetotal)/($tarmacro + $tarmicro + $tarcolo + $tarres))*100, 2, '.', '') }}%</div>
                                @else
                                <div class="d-flex align-items-center">
                                    <button class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-up" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Achievement MTD</h6>
                                        <span class="text-xs">{{ $macrototal + $micrototal + $colototal + $resetotal }} PID of {{ $tarmacro  + $tarmicro + $tarcolo + $tarres }} PID</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-success text-gradient text-sm font-weight-bold ms-auto">{{ number_format((float)(($macrototal + $micrototal + $colototal + $resetotal)/($tarmacro + $tarmicro + $tarcolo + $tarres))*100, 2, '.', '') }}%</div>
                                @endif
                            </div>
                            <hr class="horizontal dark mt-3 mb-2">
                        </li>
                        <li class="list-group-item border-0 justify-content-between ps-0 pb-0 border-radius-lg">
                            <div class="d-flex">
                                @if ($cumtarmacro + $cumtarmicro + $cumtarcolo + $cumtarres == 0)
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-up" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Achievement YTD</h6>
                                        <span class="text-xs">0</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">
                                    0.00 %
                                </div>
                                @elseif (($cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($cumtarmacro + $cumtarmicro + $cumtarcolo + $cumtarres)*100 <= 100)
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-up" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Achievement YTD</h6>
                                        <span class="text-xs">{{ $cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres }} PID of {{ $cumtarmacro + $cumtarmicro + $cumtarcolo + $cumtarres }} PID</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">
                                  {{ number_format((float)($cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($cumtarmacro + $cumtarmicro + $cumtarcolo + $cumtarres)*100, 2, '.', '') }}%
                                </div>
                                @else
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-up" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Achievement YTD</h6>
                                        <span class="text-xs">{{ $cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres }} PID of {{ $cumtarmacro + $cumtarmicro + $cumtarcolo + $cumtarres }} PID</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-success text-gradient text-sm font-weight-bold ms-auto">
                                  {{ number_format((float)($cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($cumtarmacro + $cumtarmicro + $cumtarcolo + $cumtarres)*100, 2, '.', '') }}%
                                </div>
                                @endif
                            </div>
                            <hr class="horizontal dark mt-3 mb-2">
                        </li>
                        <li class="list-group-item border-0 justify-content-between ps-0 mb-2 border-radius-lg">
                            @if ($summacro + $summicro + $sumcolo + $sumrese == 0)
                            <div class="d-flex">
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-down" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Growth</h6>
                                        <span class="text-xs">Year on Year</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">
                                    0.00%
                                </div>
                            </div>
                            @elseif ((($cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($summacro + $summicro + $sumcolo + $sumrese)-1)*100 < 0)
                            <div class="d-flex">
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-down" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Growth</h6>
                                        <span class="text-xs">Year on Year</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">
                                  {{ number_format((float)(($cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($summacro + $summicro + $sumcolo + $sumrese)-1)*100, 2, '.', '') }}%
                                </div>
                            </div>
                            @else
                            <div class="d-flex">
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-down" aria-hidden="true"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Sales Growth</h6>
                                        <span class="text-xs">Year on Year</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">
                                  {{ number_format((float)(($cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($summacro + $summicro + $sumcolo + $sumrese)-1)*100, 2, '.', '') }}%
                                </div>
                            </div>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="card">
            <div class="card-header pb-0 p-3">
            <div class="d-flex align-items-center">
                <h6 class="mb-0">Tenant by Customer (Cumulative)</h6>
                <button type="button"
                class="btn btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-auto"
                data-bs-toggle="tooltip" data-bs-placement="bottom" title="The Amount of Customer Cumulatively by Tenant">
                <i class="fas fa-info"></i>
                </button>
            </div>
            </div>
            <div class="card-body p-3">
            <div class="row">
                <div class="col-5 text-center">
                <div class="chart">
                    <canvas id="chart-cumulative" class="chart-canvas" height="197"></canvas>
                </div>
                <h4 class="font-weight-bold mt-n8">
                    <span>{{ $totalchart }}</span>
                    <span class="d-block text-body text-sm">New Sales</span>
                </h4>
                </div>
                <div class="col-7">
                <div class="table-responsive">
                    <table class="table align-items-center mb-0">
                    <tbody>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-red me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Telkomsel</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($telkomsel/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-primary me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">XL</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($xl/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-purple me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Smartfren</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($sf/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-red me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Telkom</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($telkom/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-yellow me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Indosat</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($isat/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-default me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Hutchison 3</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($h3i/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-success me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">STI</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($sti/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-secondary me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Others</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($oth/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
        <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="card">
            <div class="card-header pb-0 p-3">
            <div class="d-flex align-items-center">
                <h6 class="mb-0">Net Add Sales by Customer</h6>
                <button type="button"
                class="btn btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-auto"
                data-bs-toggle="tooltip" data-bs-placement="bottom" title="The Amount of New Sales Cumulatively by Tenant">
                <i class="fas fa-info"></i>
                </button>
            </div>
            </div>
            <div class="card-body p-3">
            <div class="row">
                <div class="col-5 text-center">
                <div class="chart">
                    <canvas id="chart-netadd" class="chart-canvas" height="197"></canvas>
                </div>
                <h4 class="font-weight-bold mt-n8">
                    <span>{{ $totalcharta }}</span>
                    <span class="d-block text-body text-sm">New Sales</span>
                </h4>
                </div>
                <div class="col-7">
                <div class="table-responsive">
                    <table class="table align-items-center mb-0">
                    <tbody>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-red me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Telkomsel</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($telkomsela/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-primary me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">XL</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($xla/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-purple me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Smartfren</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($sfa/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-red me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Telkom</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($telkoma/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-yellow me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Indosat</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($isata/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-default me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Hutchison 3</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($h3ia/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-success me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">STI</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($stia/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-secondary me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Others</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($otha/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-danger shadow text-center border-radius-md">
                            <i class="fas fa-people-carry-box text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">MTD Carry Over</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $macroco + $microco + $coloco + $reseco }} PID
                            </h5>
                        </div>
                        <div class="progress-wrapper ms-auto w-25">
                          <p class="text-sm mb-0 text-uppercase font-weight-bold">YTD Carry Over</p>
                          <h5 class="font-weight-bolder mb-0">{{ $cumcomacro + $cumcomicro + $cumcocolo + $cumcores }} PID
                          </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="co-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-danger shadow text-center border-radius-md">
                            <i class="fas fa-clipboard-list text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">MTD New Sales</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $macrons + $microns + $colons + $resens }} PID
                            </h5>
                        </div>
                        <div class="progress-wrapper ms-auto w-25">
                          <p class="text-sm mb-0 text-uppercase font-weight-bold">YTD New Sales</p>
                          <h5 class="font-weight-bolder mb-0">{{ $cumnsmacro + $cumnsmicro + $cumnscolo + $cumnsres }} PID
                          </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="ns-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-warning shadow text-center border-radius-md">
                            <i class="fas fa-tower-cell text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Macro</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $macronowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="macro-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-warning shadow text-center border-radius-md">
                            <i class="fas fa-tower-cell text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Micro</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $micronowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="micro-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-warning shadow text-center border-radius-md">
                            <i class="fas fa-tower-cell text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Colo</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $colonowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="colo-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-warning shadow text-center border-radius-md">
                            <i class="fas fa-tower-cell text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Reseller</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $resenowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="reseller-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                            <i class="fas fa-sign-hanging text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Area 1</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $a1nowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="area1-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                            <i class="fas fa-sign-hanging text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Area 2</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $a2nowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="area2-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                            <i class="fas fa-sign-hanging text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Area 3</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $a3nowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="area3-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="card overflow-hidden">
                <div class="card-header p-3 pb-0">
                    <div class="d-flex align-items-center">
                        <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                            <i class="fas fa-sign-hanging text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                        <div class="ms-3">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Area 4</p>
                            <h5 class="font-weight-bolder mb-0">
                              {{ $a4nowpro }} PID
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-3 p-0">
                    <div class="chart">
                        <canvas id="area4-chart" class="chart-canvas" height="250"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="numbers">
                                @if ($pekan == 'no')
                                <p class="text-sm mb-0 text-uppercase font-weight-bold text-dark">Sales Report for {{ $bulan }} {{ $tahun }}</p>
                                @else
                                <p class="text-sm mb-0 text-uppercase font-weight-bold text-dark">Sales Report for {{$pekan}} of {{ $bulan }} {{ $tahun }}</p>    
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <td class="text-center" style="vertical-align:middle;" rowspan="2">SoW</td>
                                            <td class="text-center" style="vertical-align:middle;" rowspan="2">MTD {{ $bulan }} 2021</td>
                                            <td class="text-center" colspan="5">Net Add Sales MTD</td>
                                            <td class="text-center" colspan="5">Cumulative</td>
                                            <td class="text-center" style="vertical-align:middle;" rowspan="2">YoY</td>
                                        </tr>
                                        <tr>
                                            
                                            <td class="text-center">Target</td>
                                            <td class="text-center">CO</td>
                                            <td class="text-center">New Sales</td>
                                            <td class="text-center">Total</td>
                                            <td class="text-center">Ach (%)</td>
                                            <td class="text-center">Target</td>
                                            <td class="text-center">CO</td>
                                            <td class="text-center">New Sales</td>
                                            <td class="text-center">Total</td>
                                            <td class="text-center">Ach (%)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">Macro</td>
                                            <td class="text-center">{{ $summacro }}</td>
                                            <td class="text-center">{{ $tarmacro }}</td>
                                            <td class="text-center">{{ $macroco }}</td>
                                            <td class="text-center">{{ $macrons }}</td>
                                            <td class="text-center">{{ $macrototal }}</td>
                                            @if ($tarmacro == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif ($macrototal/$tarmacro*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)($macrototal/$tarmacro)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($macrototal/$tarmacro)*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="text-center">{{ $cumtarmacro }}</td>
                                            <td class="text-center">{{ $cumcomacro }}</td>
                                            <td class="text-center">{{ $cumnsmacro }}</td>
                                            <td class="text-center">{{ $cumcomacro + $cumnsmacro }}</td>
                                            @if ($cumtarmacro == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif (((($cumcomacro + $cumnsmacro)/$cumtarmacro)*100) <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcomacro + $cumnsmacro)/$cumtarmacro)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcomacro + $cumnsmacro)/$cumtarmacro)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($summacro == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif ((((($cumcomacro + $cumnsmacro)/$summacro)-1)*100) <= 0)
                                            <td class="text-center text-danger">{{ number_format((float)((($cumcomacro + $cumnsmacro)/$summacro)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)((($cumcomacro + $cumnsmacro)/$summacro)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                            
                                        </tr>
                                        <tr>
                                            <td class="text-center">Micro</td>
                                            <td class="text-center">{{ $summicro }}</td>
                                            <td class="text-center">{{ $tarmicro }}</td>
                                            <td class="text-center">{{ $microco }}</td>
                                            <td class="text-center">{{ $microns }}</td>
                                            <td class="text-center">{{ $micrototal }}</td>
                                            @if ($tarmicro == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif ($micrototal/$tarmicro*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)($micrototal/$tarmicro)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($micrototal/$tarmicro)*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="text-center">{{ $cumtarmicro }}</td>
                                            <td class="text-center">{{ $cumcomicro }}</td>
                                            <td class="text-center">{{ $cumnsmicro }}</td>
                                            <td class="text-center">{{ $cumcomicro + $cumnsmicro }}</td>
                                            @if ($cumtarmicro == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif (((($cumcomicro + $cumnsmicro)/$cumtarmicro)*100) <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcomicro + $cumnsmicro)/$cumtarmicro)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcomicro + $cumnsmicro)/$cumtarmicro)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($summicro == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif ((((($cumcomicro + $cumnsmicro)/$summicro)-1)*100) <= 0)
                                            <td class="text-center text-danger">{{ number_format((float)((($cumcomicro + $cumnsmicro)/$summicro)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)((($cumcomicro + $cumnsmicro)/$summicro)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                            
                                        </tr>
                                        <tr>
                                            <td class="text-center">Colo</td>
                                            <td class="text-center">{{ $sumcolo }}</td>
                                            <td class="text-center">{{ $tarcolo }}</td>
                                            <td class="text-center">{{ $coloco }}</td>
                                            <td class="text-center">{{ $colons }}</td>
                                            <td class="text-center">{{ $colototal }}</td>
                                            @if ($tarcolo == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif ($colototal/$tarcolo*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)($colototal/$tarcolo)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($colototal/$tarcolo)*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="text-center">{{ $cumtarcolo }}</td>
                                            <td class="text-center">{{ $cumcocolo }}</td>
                                            <td class="text-center">{{ $cumnscolo }}</td>
                                            <td class="text-center">{{ $cumcocolo + $cumnscolo }}</td>
                                            @if ($cumtarcolo == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif (((($cumcocolo + $cumnscolo)/$cumtarcolo)*100) <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcocolo + $cumnscolo)/$cumtarcolo)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcocolo + $cumnscolo)/$cumtarcolo)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($sumcolo == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif ((((($cumcocolo + $cumnscolo)/$sumcolo)-1)*100) < 0)
                                            <td class="text-center text-danger">{{ number_format((float)((($cumcocolo + $cumnscolo)/$sumcolo)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)((($cumcocolo + $cumnscolo)/$sumcolo)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="text-center">Reseller</td>
                                            <td class="text-center">{{ $sumrese }}</td>
                                            <td class="text-center">{{ $tarres }}</td>
                                            <td class="text-center">{{ $reseco }}</td>
                                            <td class="text-center">{{ $resens }}</td>
                                            <td class="text-center">{{ $resetotal }}</td>
                                            @if ($tarres == 0)
                                            <td class="text-center text-danger">0.00%</td>
                                            @elseif ($resetotal/$tarres*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)($resetotal/$tarres)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($resetotal/$tarres)*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="text-center">{{ $cumtarres }}</td>
                                            <td class="text-center">{{ $cumcores }}</td>
                                            <td class="text-center">{{ $cumnsres }}</td>
                                            <td class="text-center">{{ $cumcores + $cumnsres }}</td>
                                            @if ($cumtarres == 0)
                                            <td class="text-center text-danger">0.00%</td>
                                            @elseif (((($cumcores + $cumnsres)/$cumtarres)*100) <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcores + $cumnsres)/$cumtarres)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcores + $cumnsres)/$cumtarres)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($sumrese == 0)
                                            <td class="text-center text-danger">0.00%</td>
                                            @elseif ((((($cumcores + $cumnsres)/$sumrese)-1)*100) < 0)
                                            <td class="text-center text-danger">{{ number_format((float)((($cumcores + $cumnsres)/$sumrese)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)((($cumcores + $cumnsres)/$sumrese)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold text-dark text-center">Total</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $summacro + $summicro + $sumcolo + $sumrese }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $tarmacro + $tarmicro +$tarcolo + $tarres }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $macroco + $microco + $coloco + $reseco }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $macrons + $microns + $colons + $resens }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $macrototal + $micrototal + $colototal + $resetotal }}</td>
                                            @if ($tarmacro + $tarmicro + $tarcolo + $tarres == 0)
                                            <td class="font-weight-bold text-center text-danger">0.00 %</td>
                                            @elseif (($macrototal + $micrototal + $colototal + $resetotal)/($tarmacro + $tarmicro + $tarcolo + $tarres)*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($macrototal + $micrototal + $colototal + $resetotal)/($tarmacro + $tarmicro + $tarcolo + $tarres))*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($macrototal + $micrototal + $colototal + $resetotal)/($tarmacro + $tarmicro + $tarcolo + $tarres))*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="font-weight-bold text-dark text-center">{{ $cumtarmacro + $cumtarmicro + $cumtarcolo + $cumtarres }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $cumcomacro + $cumcomicro + $cumcocolo + $cumcores }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $cumnsmacro + $cumnsmicro + $cumnscolo + $cumnsres }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres }}</td>
                                            @if ($cumtarmacro + $cumtarmicro + $cumtarcolo + $cumtarres == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif (($cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($cumtarmacro + $cumtarmicro + $cumtarcolo + $cumtarres)*100 <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)($cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($cumtarmacro + $cumtarmicro + $cumtarcolo + $cumtarres)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($cumtarmacro + $cumtarmicro + $cumtarcolo + $cumtarres)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($summacro + $summicro + $sumcolo + $sumrese == 0)
                                            <td class="text-center text-danger">0.00%</td>
                                            @elseif ((($cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($summacro + $summicro + $sumcolo + $sumrese)-1)*100 < 0)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($summacro + $summicro + $sumcolo + $sumrese)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcomacro + $cumnsmacro + $cumcomicro + $cumnsmicro + $cumcocolo + $cumnscolo + $cumcores + $cumnsres)/($summacro + $summicro + $sumcolo + $sumrese)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif($tahun == 2020)
    <div class="row mt-4">
        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="numbers">
                                @if ($week == 'no')
                                <p class="text-sm mb-0 text-uppercase font-weight-bold text-dark">Sales Report for {{ $bulan }} {{ $tahun }}</p>
                                @else
                                <p class="text-sm mb-0 text-uppercase font-weight-bold text-dark">Sales Report for {{$week}} of {{ $bulan }} {{ $tahun }}</p>    
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <td class="text-center" style="vertical-align:middle;" rowspan="2">SoW</td>
                                            <td class="text-center" style="vertical-align:middle;" rowspan="2">MTD {{ $bulan }} 2019</td>
                                            <td class="text-center" colspan="5">Net Add Sales MTD</td>
                                            <td class="text-center" colspan="5">Cumulative</td>
                                            <td class="text-center" style="vertical-align:middle;" rowspan="2">YoY</td>
                                        </tr>
                                        <tr>
                                            
                                            <td class="text-center">Target</td>
                                            <td class="text-center">CO</td>
                                            <td class="text-center">New Sales</td>
                                            <td class="text-center">Total</td>
                                            <td class="text-center">Ach (%)</td>
                                            <td class="text-center">Target</td>
                                            <td class="text-center">CO</td>
                                            <td class="text-center">New Sales</td>
                                            <td class="text-center">Total</td>
                                            <td class="text-center">Ach (%)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">B2S</td>
                                            <td class="text-center">{{ $sumb2s }}</td>
                                            <td class="text-center">{{ $tarb2s }}</td>
                                            <td class="text-center">{{ $b2sco }}</td>
                                            <td class="text-center">{{ $b2sns }}</td>
                                            <td class="text-center">{{ $b2stotal }}</td>
                                            @if ($tarb2s == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif ($b2stotal/$tarb2s*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)($b2stotal/$tarb2s)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($b2stotal/$tarb2s)*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="text-center">{{ $cumtarb2s }}</td>
                                            <td class="text-center">{{ $cumcob2s }}</td>
                                            <td class="text-center">{{ $cumnsb2s }}</td>
                                            <td class="text-center">{{ $cumcob2s + $cumnsb2s }}</td>
                                            @if ($cumtarb2s == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif (((($cumcob2s + $cumnsb2s)/$cumtarb2s)*100) <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcob2s + $cumnsb2s)/$cumtarb2s)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcob2s + $cumnsb2s)/$cumtarb2s)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($sumb2s == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif ((((($cumcob2s + $cumnsb2s)/$sumb2s)-1)*100) <= 0)
                                            <td class="text-center text-danger">{{ number_format((float)((($cumcob2s + $cumnsb2s)/$sumb2s)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)((($cumcob2s + $cumnsb2s)/$sumb2s)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                            
                                        </tr>
                                        <tr>
                                            <td class="text-center">Colo</td>
                                            <td class="text-center">{{ $sumcolo }}</td>
                                            <td class="text-center">{{ $tarcolo }}</td>
                                            <td class="text-center">{{ $coloco }}</td>
                                            <td class="text-center">{{ $colons }}</td>
                                            <td class="text-center">{{ $colototal }}</td>
                                            @if ($tarcolo == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif ($colototal/$tarcolo*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)($colototal/$tarcolo)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($colototal/$tarcolo)*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="text-center">{{ $cumtarcolo }}</td>
                                            <td class="text-center">{{ $cumcocolo }}</td>
                                            <td class="text-center">{{ $cumnscolo }}</td>
                                            <td class="text-center">{{ $cumcocolo + $cumnscolo }}</td>
                                            @if ($cumtarcolo == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif (((($cumcocolo + $cumnscolo)/$cumtarcolo)*100) <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcocolo + $cumnscolo)/$cumtarcolo)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcocolo + $cumnscolo)/$cumtarcolo)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($sumcolo == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif ((((($cumcocolo + $cumnscolo)/$sumcolo)-1)*100) < 0)
                                            <td class="text-center text-danger">{{ number_format((float)((($cumcocolo + $cumnscolo)/$sumcolo)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)((($cumcocolo + $cumnscolo)/$sumcolo)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="text-center">Reseller</td>
                                            <td class="text-center">{{ $sumrese }}</td>
                                            <td class="text-center">{{ $tarres }}</td>
                                            <td class="text-center">{{ $reseco }}</td>
                                            <td class="text-center">{{ $resens }}</td>
                                            <td class="text-center">{{ $resetotal }}</td>
                                            @if ($tarres == 0)
                                            <td class="text-center text-danger">0.00%</td>
                                            @elseif ($resetotal/$tarres*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)($resetotal/$tarres)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($resetotal/$tarres)*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="text-center">{{ $cumtarres }}</td>
                                            <td class="text-center">{{ $cumcores }}</td>
                                            <td class="text-center">{{ $cumnsres }}</td>
                                            <td class="text-center">{{ $cumcores + $cumnsres }}</td>
                                            @if ($cumtarres == 0)
                                            <td class="text-center text-danger">0.00%</td>
                                            @elseif (((($cumcores + $cumnsres)/$cumtarres)*100) <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcores + $cumnsres)/$cumtarres)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcores + $cumnsres)/$cumtarres)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($sumrese == 0)
                                            <td class="text-center text-danger">0.00%</td>
                                            @elseif ((((($cumcores + $cumnsres)/$sumrese)-1)*100) < 0)
                                            <td class="text-center text-danger">{{ number_format((float)((($cumcores + $cumnsres)/$sumrese)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)((($cumcores + $cumnsres)/$sumrese)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="text-center">Solution</td>
                                            <td class="text-center">{{ $sumsol }}</td>
                                            <td class="text-center">{{ $tarsol }}</td>
                                            <td class="text-center">{{ $solco }}</td>
                                            <td class="text-center">{{ $solns }}</td>
                                            <td class="text-center">{{ $soltotal }}</td>
                                            @if ($tarsol == 0)
                                            <td class="text-center text-danger">0.00%</td>
                                            @elseif ($soltotal/$tarsol*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)($soltotal/$tarsol)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($soltotal/$tarsol)*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="text-center">{{ $cumtarsol }}</td>
                                            <td class="text-center">{{ $cumcosol }}</td>
                                            <td class="text-center">{{ $cumnssol }}</td>
                                            <td class="text-center">{{ $cumcosol + $cumnssol }}</td>
                                            @if ($cumtarsol == 0)
                                            <td class="text-center text-danger">0.00%</td>
                                            @elseif (((($cumcosol + $cumnssol)/$cumtarsol)*100) <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcosol + $cumnssol)/$cumtarsol)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcosol + $cumnssol)/$cumtarsol)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($sumsol == 0)
                                            <td class="text-center text-danger">0.00%</td>
                                            @elseif ((((($cumcosol + $cumnssol)/$sumsol)-1)*100) < 0)
                                            <td class="text-center text-danger">{{ number_format((float)((($cumcosol + $cumnssol)/$sumsol)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)((($cumcosol + $cumnssol)/$sumsol)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold text-dark text-center">Total</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $sumb2s + $sumcolo + $sumrese + $sumsol }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $tarb2s + $tarcolo + $tarres + $tarsol }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $b2sco + $coloco + $reseco + $solco }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $b2sns + $colons + $resens + $solns }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $b2stotal + $colototal + $resetotal + $soltotal }}</td>
                                            @if ($tarb2s + $tarcolo + $tarres + $tarsol == 0)
                                            <td class="font-weight-bold text-center text-danger">0.00 %</td>
                                            @elseif (($b2stotal + $colototal + $resetotal + $soltotal)/($tarb2s + $tarcolo + $tarres + $tarsol)*100 < 100)
                                            <td class="text-center text-danger">{{ number_format((float)(($b2stotal + $colototal + $resetotal + $soltotal)/($tarb2s + $tarcolo + $tarres + $tarsol))*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($b2stotal + $colototal + $resetotal + $soltotal)/($tarb2s + $tarcolo + $tarres + $tarsol))*100, 2, '.', '') }}%</td>
                                            @endif
                                            <td class="font-weight-bold text-dark text-center">{{ $cumtarb2s + $cumtarcolo + $cumtarres + $cumtarsol }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $cumcob2s + $cumcocolo + $cumcores + $cumcosol }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $cumnsb2s + $cumnscolo + $cumnsres + $cumnssol }}</td>
                                            <td class="font-weight-bold text-dark text-center">{{ $cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres + $cumcosol + $cumnssol }}</td>
                                            @if ($cumtarb2s + $cumtarcolo + $cumtarres + $cumtarsol == 0)
                                            <td class="text-center text-danger">0.00 %</td>
                                            @elseif (($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres + $cumcosol + $cumnssol)/($cumtarb2s + $cumtarcolo + $cumtarres + $cumtarsol)*100 <= 100)
                                            <td class="text-center text-danger">{{ number_format((float)($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres + $cumcosol + $cumnssol)/($cumtarb2s + $cumtarcolo + $cumtarres + $cumtarsol)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres + $cumcosol + $cumnssol)/($cumtarb2s + $cumtarcolo + $cumtarres + $cumtarsol)*100, 2, '.', '') }}%</td>
                                            @endif
                                            @if ($sumb2s + $sumcolo + $sumrese + $sumsol == 0)
                                            <td class="text-center text-danger">0.00%</td>
                                            @elseif ((($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres + $cumcosol + $cumnssol)/($sumb2s + $sumcolo + $sumrese + $sumsol)-1)*100 < 0)
                                            <td class="text-center text-danger">{{ number_format((float)(($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres + $cumcosol + $cumnssol)/($sumb2s + $sumcolo + $sumrese + $sumsol)-1)*100, 2, '.', '') }}%</td>
                                            @else
                                            <td class="text-center text-success">{{ number_format((float)(($cumcob2s + $cumnsb2s + $cumcocolo + $cumnscolo + $cumcores + $cumnsres + $cumcosol + $cumnssol)/($sumb2s + $sumcolo + $sumrese + $sumsol)-1)*100, 2, '.', '') }}%</td>
                                            @endif
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="card">
            <div class="card-header pb-0 p-3">
            <div class="d-flex align-items-center">
                <h6 class="mb-0">Tenant by Customer (Cumulative)</h6>
                <button type="button"
                class="btn btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-auto"
                data-bs-toggle="tooltip" data-bs-placement="bottom" title="The Amount of Customer Cumulatively by Tenant">
                <i class="fas fa-info"></i>
                </button>
            </div>
            </div>
            <div class="card-body p-3">
            <div class="row">
                <div class="col-5 text-center">
                <div class="chart">
                    <canvas id="chart-cumulative" class="chart-canvas" height="197"></canvas>
                </div>
                <h4 class="font-weight-bold mt-n8">
                    <span>{{ $totalchart }}</span>
                    <span class="d-block text-body text-sm">New Sales</span>
                </h4>
                </div>
                <div class="col-7">
                <div class="table-responsive">
                    <table class="table align-items-center mb-0">
                    <tbody>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-red me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Telkomsel</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($telkomsel/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-primary me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">XL</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($xl/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-purple me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Smartfren</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($sf/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-red me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Telkom</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($telkom/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-yellow me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Indosat</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($isat/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-default me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Hutchison 3</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($h3i/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-success me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">HCPT</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($hcpt/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-secondary me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Others</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($oth/$totalchart)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
        <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="card">
            <div class="card-header pb-0 p-3">
            <div class="d-flex align-items-center">
                <h6 class="mb-0">Net Add Sales by Customer</h6>
                <button type="button"
                class="btn btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-auto"
                data-bs-toggle="tooltip" data-bs-placement="bottom" title="The Amount of New Sales Cumulatively by Tenant">
                <i class="fas fa-info"></i>
                </button>
            </div>
            </div>
            <div class="card-body p-3">
            <div class="row">
                <div class="col-5 text-center">
                <div class="chart">
                    <canvas id="chart-netadd" class="chart-canvas" height="197"></canvas>
                </div>
                <h4 class="font-weight-bold mt-n8">
                    <span>{{ $totalcharta }}</span>
                    <span class="d-block text-body text-sm">New Sales</span>
                </h4>
                </div>
                <div class="col-7">
                <div class="table-responsive">
                    <table class="table align-items-center mb-0">
                    <tbody>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-red me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Telkomsel</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($telkomsela/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-primary me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">XL</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($xla/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-purple me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Smartfren</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($sfa/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-red me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Telkom</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($telkoma/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-yellow me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Indosat</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($isata/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-default me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Hutchison 3</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($h3ia/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-success me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">HCPT</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($hcpta/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex px-2 py-0">
                            <span class="badge bg-secondary me-3"> </span>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">Others</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                            <span class="text-xs font-weight-bold"> {{ number_format((float)($otha/$totalcharta)*100, 2, '.', '') }}% </span>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    @endif

@else
<main class="main-content  mt-0">
  <div class="page-header min-vh-100">
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-lg-6 col-md-7 mx-auto text-center">
                  <h1 class="display-1 text-bolder text-dark">Error 404</h1>
                  <h4>Hello there. Your data is not found!</h2>
                  <p class="lead">We suggest you to <b>re-check your input</b></p>
                  <a class="btn btn-danger" href="/salerep">Return</a>
              </div>
          </div>
      </div>
  </div>
</main>
@endif
@endsection
@push('scripts')
@if ($tahun == 2022)
<script>
    // Bar chart
    var ctx1 = document.getElementById("chart-cumulative").getContext("2d");
    new Chart(ctx1, {
      type: "doughnut",
      data: {
        labels: ['H3I', 'Others', 'Telkom', 'SF', 'XL', 'ISAT', 'TSEL'],
        datasets: [{
          label: "Net Add Sales Share",
          weight: 9,
          cutout: 90,
          tension: 0.9,
          pointRadius: 2,
          borderWidth: 2,
          backgroundColor: ["black", "brown", "orange", "purple", "blue", "yellow", "red"],
          data: [{{ $h3i }}, {{ $oth }}, {{ $telkom }}, {{ $sf }}, {{ $xl }}, {{ $isat }}, {{ $telkomsel }}],
          fill: false
        }],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: false,
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
            },
            ticks: {
              display: false
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
            },
            ticks: {
              display: false,
            }
          },
        },
      },
    });
    var ctx2 = document.getElementById("chart-netadd").getContext("2d");

    var gradientStroke1 = ctx2.createLinearGradient(0, 230, 0, 50);

    gradientStroke1.addColorStop(1, 'rgba(203,12,159,0.2)');
    gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
    gradientStroke1.addColorStop(0, 'rgba(203,12,159,0)'); //purple colors

    new Chart(ctx2, {
      type: "doughnut",
      data: {
        labels: ['H3I', 'Others', 'HCPT', 'Telkom', 'SF', 'XL', 'ISAT', 'TSEL'],
        datasets: [{
          label: "Net Add Sales Share",
          weight: 9,
          cutout: 90,
          tension: 0.9,
          pointRadius: 2,
          borderWidth: 2,
          backgroundColor: ["black", "brown", "orange", "purple", "blue", "yellow", "red"],
          data: [{{ $h3ia }}, {{ $otha }}, {{ $telkoma }}, {{ $sfa }}, {{ $xla }}, {{ $isata }}, {{ $telkomsela }}],
          fill: false
        }],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: false,
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
            },
            ticks: {
              display: false
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
            },
            ticks: {
              display: false,
            }
          },
        },
      },
    });

    var ctx3 = document.getElementById("co-chart").getContext("2d");
    new Chart(ctx3, {
      data: {
        labels: {!! $labelbulan !!},
        datasets: [{
            type: "bar",
            label: "C0 2022",
            weight: 5,
            tension: 0.4,
            borderWidth: 0,
            pointBackgroundColor: "#800000",
            borderColor: "#800000",
            backgroundColor: '#800000',
            borderRadius: 4,
            borderSkipped: false,
            data: {{ $hasilconow }},
            maxBarThickness: 10,
          },
          {
            type: "line",
            label: "CO 2021",
            tension: 0.4,
            borderWidth: 0,
            pointRadius: 0,
            pointBackgroundColor: "#DC143C",
            borderColor: "#DC143C",
            borderWidth: 3,
            backgroundColor: gradientStroke1,
            data: {{ $hasilcobef }},
            fill: true,
          }
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#b2b9bf',
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: true,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              color: '#b2b9bf',
              padding: 10,
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
        },
      },
    });
    var ctx4 = document.getElementById("ns-chart").getContext("2d");
    new Chart(ctx4, {
      data: {
        labels: {!! $labelbulan !!},
        datasets: [{
            type: "bar",
            label: "NS 2022",
            weight: 5,
            tension: 0.4,
            borderWidth: 0,
            pointBackgroundColor: "#800000",
            borderColor: "#800000",
            backgroundColor: '#800000',
            borderRadius: 4,
            borderSkipped: false,
            data: {{ $hasilnsnow }},
            maxBarThickness: 10,
          },
          {
            type: "line",
            label: "NS 2021",
            tension: 0.4,
            borderWidth: 0,
            pointRadius: 0,
            pointBackgroundColor: "#DC143C",
            borderColor: "#DC143C",
            borderWidth: 3,
            backgroundColor: gradientStroke1,
            data: {{ $hasilnsbef }},
            fill: true,
          }
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#b2b9bf',
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: true,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              color: '#b2b9bf',
              padding: 10,
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
        },
      },
    });
    var ctx5 = document.getElementById("b2s-chart").getContext("2d");
    new Chart(ctx5, {
      data: {
        labels: {!! $labelbulan !!},
        datasets: [{
            type: "bar",
            label: "B2S 2022",
            weight: 5,
            tension: 0.4,
            borderWidth: 0,
            pointBackgroundColor: "#DAA520",
            borderColor: "#DAA520",
            backgroundColor: '#DAA520',
            borderRadius: 4,
            borderSkipped: false,
            data: {{ $hasilb2snow }},
            maxBarThickness: 10,
          },
          {
            type: "line",
            label: "B2S 2021",
            tension: 0.4,
            borderWidth: 0,
            pointRadius: 0,
            pointBackgroundColor: "#FFD700",
            borderColor: "#FFD700",
            borderWidth: 3,
            backgroundColor: gradientStroke1,
            data: {{ $hasilb2sbef }},
            fill: true,
          }
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#b2b9bf',
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: true,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              color: '#b2b9bf',
              padding: 10,
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
        },
      },
    });
    var ctx6 = document.getElementById("colo-chart").getContext("2d");
    new Chart(ctx6, {
      data: {
        labels: {!! $labelbulan !!},
        datasets: [{
            type: "bar",
            label: "Colo 2022",
            weight: 5,
            tension: 0.4,
            borderWidth: 0,
            pointBackgroundColor: "#DAA520",
            borderColor: "#DAA520",
            backgroundColor: '#DAA520',
            borderRadius: 4,
            borderSkipped: false,
            data: {{ $hasilcolonow }},
            maxBarThickness: 10,
          },
          {
            type: "line",
            label: "Colo 2021",
            tension: 0.4,
            borderWidth: 0,
            pointRadius: 0,
            pointBackgroundColor: "#FFD700",
            borderColor: "#FFD700",
            borderWidth: 3,
            backgroundColor: gradientStroke1,
            data: {{ $hasilcolobef }},
            fill: true,
          }
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#b2b9bf',
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: true,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              color: '#b2b9bf',
              padding: 10,
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
        },
      },
    });
    var ctx7 = document.getElementById("reseller-chart").getContext("2d");
    new Chart(ctx7, {
      data: {
        labels: {!! $labelbulan !!},
        datasets: [{
            type: "bar",
            label: "Rese 2022",
            weight: 5,
            tension: 0.4,
            borderWidth: 0,
            pointBackgroundColor: "#DAA520",
            borderColor: "#DAA520",
            backgroundColor: '#DAA520',
            borderRadius: 4,
            borderSkipped: false,
            data: {{ $hasilresenow }},
            maxBarThickness: 10,
          },
          {
            type: "line",
            label: "Rese 2021",
            tension: 0.4,
            borderWidth: 0,
            pointRadius: 0,
            pointBackgroundColor: "#FFD700",
            borderColor: "#FFD700",
            borderWidth: 3,
            backgroundColor: gradientStroke1,
            data: {{ $hasilresebef }},
            fill: true,
          }
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#b2b9bf',
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: true,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              color: '#b2b9bf',
              padding: 10,
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
        },
      },
    });
    var ctx8 = document.getElementById("area1-chart").getContext("2d");
    new Chart(ctx8, {
      data: {
        labels: {!! $labelbulan !!},
        datasets: [{
            type: "bar",
            label: "A1 2022",
            weight: 5,
            tension: 0.4,
            borderWidth: 0,
            pointBackgroundColor: "#008000",
            borderColor: "#008000",
            backgroundColor: '#008000',
            borderRadius: 4,
            borderSkipped: false,
            data: {{ $hasila1now }},
            maxBarThickness: 10,
          },
          {
            type: "line",
            label: "A1 2021",
            tension: 0.4,
            borderWidth: 0,
            pointRadius: 0,
            pointBackgroundColor: "#00FF00",
            borderColor: "#00FF00",
            borderWidth: 3,
            backgroundColor: gradientStroke1,
            data: {{ $hasila1bef }},
            fill: true,
          }
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#b2b9bf',
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: true,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              color: '#b2b9bf',
              padding: 10,
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
        },
      },
    });
    var ctx9 = document.getElementById("area2-chart").getContext("2d");
    new Chart(ctx9, {
      data: {
        labels: {!! $labelbulan !!},
        datasets: [{
            type: "bar",
            label: "A2 2022",
            weight: 5,
            tension: 0.4,
            borderWidth: 0,
            pointBackgroundColor: "#008000",
            borderColor: "#008000",
            backgroundColor: '#008000',
            borderRadius: 4,
            borderSkipped: false,
            data: {{ $hasila2now }},
            maxBarThickness: 10,
          },
          {
            type: "line",
            label: "A2 2021",
            tension: 0.4,
            borderWidth: 0,
            pointRadius: 0,
            pointBackgroundColor: "#00FF00",
            borderColor: "#00FF00",
            borderWidth: 3,
            backgroundColor: gradientStroke1,
            data: {{ $hasila2bef }},
            fill: true,
          }
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#b2b9bf',
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: true,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              color: '#b2b9bf',
              padding: 10,
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
        },
      },
    });
    var ctx10 = document.getElementById("area3-chart").getContext("2d");
    new Chart(ctx10, {
      data: {
        labels: {!! $labelbulan !!},
        datasets: [{
            type: "bar",
            label: "A3 2022",
            weight: 5,
            tension: 0.4,
            borderWidth: 0,
            pointBackgroundColor: "#008000",
            borderColor: "#008000",
            backgroundColor: '#008000',
            borderRadius: 4,
            borderSkipped: false,
            data: {{ $hasila3now }},
            maxBarThickness: 10,
          },
          {
            type: "line",
            label: "A3 2021",
            tension: 0.4,
            borderWidth: 0,
            pointRadius: 0,
            pointBackgroundColor: "#00FF00",
            borderColor: "#00FF00",
            borderWidth: 3,
            backgroundColor: gradientStroke1,
            data: {{ $hasila3bef }},
            fill: true,
          }
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#b2b9bf',
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: true,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              color: '#b2b9bf',
              padding: 10,
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
        },
      },
    });
    var ctx11 = document.getElementById("area4-chart").getContext("2d");
    new Chart(ctx11, {
      data: {
        labels: {!! $labelbulan !!},
        datasets: [{
            type: "bar",
            label: "A4 2022",
            weight: 5,
            tension: 0.4,
            borderWidth: 0,
            pointBackgroundColor: "#008000",
            borderColor: "#008000",
            backgroundColor: '#008000',
            borderRadius: 4,
            borderSkipped: false,
            data: {{ $hasila4now }},
            maxBarThickness: 10,
          },
          {
            type: "line",
            label: "A4 2021",
            tension: 0.4,
            borderWidth: 0,
            pointRadius: 0,
            pointBackgroundColor: "#00FF00",
            borderColor: "#00FF00",
            borderWidth: 3,
            backgroundColor: gradientStroke1,
            data: {{ $hasila4bef }},
            fill: true,
          }
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#b2b9bf',
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: true,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              color: '#b2b9bf',
              padding: 10,
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
        },
      },
    });
    var ctx11 = document.getElementById("total-chart").getContext("2d");
    new Chart(ctx11, {
      data: {
        labels: {!! $labelbulan !!},
        datasets: [{
            type: "bar",
            label: "Total Sales 2022",
            weight: 5,
            tension: 0.4,
            borderWidth: 0,
            pointBackgroundColor: "#800000",
            borderColor: "#800000",
            backgroundColor: '#800000',
            borderRadius: 4,
            borderSkipped: false,
            data: {{ $hasiltotsale }},
            maxBarThickness: 10,
          },
          {
            type: "line",
            label: "Target Sales 2022",
            tension: 0.4,
            borderWidth: 0,
            pointRadius: 0,
            pointBackgroundColor: "#808080",
            borderColor: "#808080",
            borderWidth: 3,
            backgroundColor: gradientStroke1,
            data: {{ $hasiltarsale }},
            fill: true,
          },
          {
            type: "line",
            label: "Total Sales 2021",
            tension: 0.4,
            borderWidth: 0,
            pointRadius: 2,
            pointBackgroundColor: "#DC143C",
            borderColor: "#DC143C",
            borderWidth: 3,
            data: {{ $hasilreasale }},
            maxBarThickness: 6
          }
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#b2b9bf',
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: true,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              color: '#b2b9bf',
              padding: 10,
              font: {
                size: 11,
                family: "Open Sans",
                style: 'normal',
                lineHeight: 2
              },
            }
          },
        },
      },
    });
</script>
@elseif($tahun == 2021)
<script>
    // Bar chart
    var ctx1 = document.getElementById("chart-cumulative").getContext("2d");
    new Chart(ctx1, {
      type: "doughnut",
      data: {
        labels: ['H3I', 'Others', 'HCPT', 'Telkom', 'SF', 'XL', 'ISAT', 'TSEL'],
        datasets: [{
          label: "Net Add Sales Share",
          weight: 9,
          cutout: 90,
          tension: 0.9,
          pointRadius: 2,
          borderWidth: 2,
          backgroundColor: ["black", "brown", "orange", "red", "purple", "blue", "yellow", "red"],
          data: [{{ $h3i }}, {{ $oth }}, {{ $sti }}, {{ $telkom }}, {{ $sf }}, {{ $xl }}, {{ $isat }}, {{ $telkomsel }}],
          fill: false
        }],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: false,
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
            },
            ticks: {
              display: false
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
            },
            ticks: {
              display: false,
            }
          },
        },
      },
    });
    var ctx2 = document.getElementById("chart-netadd").getContext("2d");

    var gradientStroke1 = ctx2.createLinearGradient(0, 230, 0, 50);

    gradientStroke1.addColorStop(1, 'rgba(203,12,159,0.2)');
    gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
    gradientStroke1.addColorStop(0, 'rgba(203,12,159,0)'); //purple colors

    new Chart(ctx2, {
      type: "doughnut",
      data: {
        labels: ['H3I', 'Others', 'HCPT', 'Telkom', 'SF', 'XL', 'ISAT', 'TSEL'],
        datasets: [{
          label: "Net Add Sales Share",
          weight: 9,
          cutout: 90,
          tension: 0.9,
          pointRadius: 2,
          borderWidth: 2,
          backgroundColor: ["black", "brown", "orange", "red", "purple", "blue", "yellow", "red"],
          data: [{{ $h3ia }}, {{ $otha }}, {{ $stia }}, {{ $telkoma }}, {{ $sfa }}, {{ $xla }}, {{ $isata }}, {{ $telkomsela }}],
          fill: false
        }],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: false,
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
            },
            ticks: {
              display: false
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
            },
            ticks: {
              display: false,
            }
          },
        },
      },
    });

  var ctx3 = document.getElementById("co-chart").getContext("2d");
  new Chart(ctx3, {
    data: {
      labels: {!! $labelbulan !!},
      datasets: [{
          type: "bar",
          label: "C0 2022",
          weight: 5,
          tension: 0.4,
          borderWidth: 0,
          pointBackgroundColor: "#800000",
          borderColor: "#800000",
          backgroundColor: '#800000',
          borderRadius: 4,
          borderSkipped: false,
          data: {{ $hasilconow }},
          maxBarThickness: 10,
        },
        {
          type: "line",
          label: "CO 2021",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          pointBackgroundColor: "#DC143C",
          borderColor: "#DC143C",
          borderWidth: 3,
          backgroundColor: gradientStroke1,
          data: {{ $hasilcobef }},
          fill: true,
        }
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'bottom',
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#b2b9bf',
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: true,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#b2b9bf',
            padding: 10,
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });
  var ctx4 = document.getElementById("ns-chart").getContext("2d");
  new Chart(ctx4, {
    data: {
      labels: {!! $labelbulan !!},
      datasets: [{
          type: "bar",
          label: "NS 2022",
          weight: 5,
          tension: 0.4,
          borderWidth: 0,
          pointBackgroundColor: "#800000",
          borderColor: "#800000",
          backgroundColor: '#800000',
          borderRadius: 4,
          borderSkipped: false,
          data: {{ $hasilnsnow }},
          maxBarThickness: 10,
        },
        {
          type: "line",
          label: "NS 2021",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          pointBackgroundColor: "#DC143C",
          borderColor: "#DC143C",
          borderWidth: 3,
          backgroundColor: gradientStroke1,
          data: {{ $hasilnsbef }},
          fill: true,
        }
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'bottom',
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#b2b9bf',
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: true,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#b2b9bf',
            padding: 10,
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });
  var ctx5 = document.getElementById("macro-chart").getContext("2d");
  new Chart(ctx5, {
    data: {
      labels: {!! $labelbulan !!},
      datasets: [{
          type: "bar",
          label: "Macro 2021",
          weight: 5,
          tension: 0.4,
          borderWidth: 0,
          pointBackgroundColor: "#DAA520",
          borderColor: "#DAA520",
          backgroundColor: '#DAA520',
          borderRadius: 4,
          borderSkipped: false,
          data: {{ $hasilmacronow }},
          maxBarThickness: 10,
        },
        {
          type: "line",
          label: "Macro 2020",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          pointBackgroundColor: "#FFD700",
          borderColor: "#FFD700",
          borderWidth: 3,
          backgroundColor: gradientStroke1,
          data: {{ $hasilmacrobef }},
          fill: true,
        }
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'bottom',
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#b2b9bf',
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: true,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#b2b9bf',
            padding: 10,
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });
  var ctx6 = document.getElementById("colo-chart").getContext("2d");
  new Chart(ctx6, {
    data: {
      labels: {!! $labelbulan !!},
      datasets: [{
          type: "bar",
          label: "Colo 2022",
          weight: 5,
          tension: 0.4,
          borderWidth: 0,
          pointBackgroundColor: "#DAA520",
          borderColor: "#DAA520",
          backgroundColor: '#DAA520',
          borderRadius: 4,
          borderSkipped: false,
          data: {{ $hasilcolonow }},
          maxBarThickness: 10,
        },
        {
          type: "line",
          label: "Colo 2021",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          pointBackgroundColor: "#FFD700",
          borderColor: "#FFD700",
          borderWidth: 3,
          backgroundColor: gradientStroke1,
          data: {{ $hasilcolobef }},
          fill: true,
        }
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'bottom',
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#b2b9bf',
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: true,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#b2b9bf',
            padding: 10,
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });
  var ctx7 = document.getElementById("reseller-chart").getContext("2d");
  new Chart(ctx7, {
    data: {
      labels: {!! $labelbulan !!},
      datasets: [{
          type: "bar",
          label: "Rese 2022",
          weight: 5,
          tension: 0.4,
          borderWidth: 0,
          pointBackgroundColor: "#DAA520",
          borderColor: "#DAA520",
          backgroundColor: '#DAA520',
          borderRadius: 4,
          borderSkipped: false,
          data: {{ $hasilresenow }},
          maxBarThickness: 10,
        },
        {
          type: "line",
          label: "Rese 2021",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          pointBackgroundColor: "#FFD700",
          borderColor: "#FFD700",
          borderWidth: 3,
          backgroundColor: gradientStroke1,
          data: {{ $hasilresebef }},
          fill: true,
        }
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'bottom',
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#b2b9bf',
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: true,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#b2b9bf',
            padding: 10,
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });
  var ctx8 = document.getElementById("area1-chart").getContext("2d");
  new Chart(ctx8, {
    data: {
      labels: {!! $labelbulan !!},
      datasets: [{
          type: "bar",
          label: "A1 2022",
          weight: 5,
          tension: 0.4,
          borderWidth: 0,
          pointBackgroundColor: "#008000",
          borderColor: "#008000",
          backgroundColor: '#008000',
          borderRadius: 4,
          borderSkipped: false,
          data: {{ $hasila1now }},
          maxBarThickness: 10,
        },
        {
          type: "line",
          label: "A1 2021",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          pointBackgroundColor: "#00FF00",
          borderColor: "#00FF00",
          borderWidth: 3,
          backgroundColor: gradientStroke1,
          data: {{ $hasila1bef }},
          fill: true,
        }
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'bottom',
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#b2b9bf',
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: true,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#b2b9bf',
            padding: 10,
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });
  var ctx9 = document.getElementById("area2-chart").getContext("2d");
  new Chart(ctx9, {
    data: {
      labels: {!! $labelbulan !!},
      datasets: [{
          type: "bar",
          label: "A2 2022",
          weight: 5,
          tension: 0.4,
          borderWidth: 0,
          pointBackgroundColor: "#008000",
          borderColor: "#008000",
          backgroundColor: '#008000',
          borderRadius: 4,
          borderSkipped: false,
          data: {{ $hasila2now }},
          maxBarThickness: 10,
        },
        {
          type: "line",
          label: "A2 2021",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          pointBackgroundColor: "#00FF00",
          borderColor: "#00FF00",
          borderWidth: 3,
          backgroundColor: gradientStroke1,
          data: {{ $hasila2bef }},
          fill: true,
        }
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'bottom',
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#b2b9bf',
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: true,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#b2b9bf',
            padding: 10,
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });
  var ctx10 = document.getElementById("area3-chart").getContext("2d");
  new Chart(ctx10, {
    data: {
      labels: {!! $labelbulan !!},
      datasets: [{
          type: "bar",
          label: "A3 2022",
          weight: 5,
          tension: 0.4,
          borderWidth: 0,
          pointBackgroundColor: "#008000",
          borderColor: "#008000",
          backgroundColor: '#008000',
          borderRadius: 4,
          borderSkipped: false,
          data: {{ $hasila3now }},
          maxBarThickness: 10,
        },
        {
          type: "line",
          label: "Referral",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          pointBackgroundColor: "#00FF00",
          borderColor: "#00FF00",
          borderWidth: 3,
          backgroundColor: gradientStroke1,
          data: {{ $hasila3bef }},
          fill: true,
        }
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'bottom',
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#b2b9bf',
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: true,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#b2b9bf',
            padding: 10,
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });
  var ctx11 = document.getElementById("area4-chart").getContext("2d");
  new Chart(ctx11, {
    data: {
      labels: {!! $labelbulan !!},
      datasets: [{
          type: "bar",
          label: "A4 2022",
          weight: 5,
          tension: 0.4,
          borderWidth: 0,
          pointBackgroundColor: "#008000",
          borderColor: "#008000",
          backgroundColor: '#008000',
          borderRadius: 4,
          borderSkipped: false,
          data: {{ $hasila4now }},
          maxBarThickness: 10,
        },
        {
          type: "line",
          label: "A4 2021",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          pointBackgroundColor: "#00FF00",
          borderColor: "#00FF00",
          borderWidth: 3,
          backgroundColor: gradientStroke1,
          data: {{ $hasila4bef }},
          fill: true,
        }
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'bottom',
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#b2b9bf',
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: true,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#b2b9bf',
            padding: 10,
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });
  var ctx11 = document.getElementById("total-chart").getContext("2d");
  new Chart(ctx11, {
    data: {
      labels: {!! $labelbulan !!},
      datasets: [{
          type: "bar",
          label: "Total Sales 2022",
          weight: 5,
          tension: 0.4,
          borderWidth: 0,
          pointBackgroundColor: "#800000",
          borderColor: "#800000",
          backgroundColor: '#800000',
          borderRadius: 4,
          borderSkipped: false,
          data: {{ $hasiltotsale }},
          maxBarThickness: 10,
        },
        {
          type: "line",
          label: "Target Sales 2022",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          pointBackgroundColor: "#808080",
          borderColor: "#808080",
          borderWidth: 3,
          backgroundColor: gradientStroke1,
          data: {{ $hasiltarsale }},
          fill: true,
        },
        {
          type: "line",
          label: "Total Sales 2021",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 2,
          pointBackgroundColor: "#DC143C",
          borderColor: "#DC143C",
          borderWidth: 3,
          data: {{ $hasilreasale }},
          maxBarThickness: 6
        }
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'bottom',
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#b2b9bf',
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: true,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#b2b9bf',
            padding: 10,
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });
  var ctx12 = document.getElementById("micro-chart").getContext("2d");
  new Chart(ctx12, {
    data: {
      labels: {!! $labelbulan !!},
      datasets: [{
          type: "bar",
          label: "Micro 2021",
          weight: 5,
          tension: 0.4,
          borderWidth: 0,
          pointBackgroundColor: "#DAA520",
          borderColor: "#DAA520",
          backgroundColor: '#DAA520',
          borderRadius: 4,
          borderSkipped: false,
          data: {{ $hasilmicronow }},
          maxBarThickness: 10,
        },
        {
          type: "line",
          label: "Micro 2020",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          pointBackgroundColor: "#FFD700",
          borderColor: "#FFD700",
          borderWidth: 3,
          backgroundColor: gradientStroke1,
          data: {{ $hasilmicrobef }},
          fill: true,
        }
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'bottom',
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#b2b9bf',
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: true,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#b2b9bf',
            padding: 10,
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });

</script>
{{-- @elseif($tahun == 2020)
<script>
    // Bar chart
    var ctx5 = document.getElementById("chart-cumulative").getContext("2d");
    new Chart(ctx5, {
      type: "doughnut",
      data: {
        labels: ['H3I', 'Others', 'HCPT', 'Telkom', 'SF', 'XL', 'ISAT', 'TSEL'],
        datasets: [{
          label: "Net Add Sales Share",
          weight: 9,
          cutout: 90,
          tension: 0.9,
          pointRadius: 2,
          borderWidth: 2,
          backgroundColor: ["black", "brown", "orange", "red", "purple", "blue", "yellow", "red"],
          data: [{{ $h3i }}, {{ $oth }}, {{ $sti }}, {{ $telkom }}, {{ $sf }}, {{ $xl }}, {{ $isat }}, {{ $telkomsel }}],
          fill: false
        }],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: false,
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
            },
            ticks: {
              display: false
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
            },
            ticks: {
              display: false,
            }
          },
        },
      },
    });
    var ctx1 = document.getElementById("chart-netadd").getContext("2d");

    var gradientStroke1 = ctx1.createLinearGradient(0, 230, 0, 50);

    gradientStroke1.addColorStop(1, 'rgba(203,12,159,0.2)');
    gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
    gradientStroke1.addColorStop(0, 'rgba(203,12,159,0)'); //purple colors

    new Chart(ctx1, {
      type: "doughnut",
      data: {
        labels: ['H3I', 'Others', 'HCPT', 'Telkom', 'SF', 'XL', 'ISAT', 'TSEL'],
        datasets: [{
          label: "Net Add Sales Share",
          weight: 9,
          cutout: 90,
          tension: 0.9,
          pointRadius: 2,
          borderWidth: 2,
          backgroundColor: ["black", "brown", "orange", "red", "purple", "blue", "yellow", "red"],
          data: [{{ $h3ia }}, {{ $otha }}, {{ $stia }}, {{ $telkoma }}, {{ $sfa }}, {{ $xla }}, {{ $isata }}, {{ $telkomsela }}],
          fill: false
        }],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: false,
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
            },
            ticks: {
              display: false
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: false,
              drawTicks: false,
            },
            ticks: {
              display: false,
            }
          },
        },
      },
    });

</script> --}}
@endif
@endpush
