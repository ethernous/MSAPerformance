@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-8">
        <div class="row">
            <div class="col-xl-6 mb-xl-0 mb-4">
                <div class="card bg-transparent shadow-xl">
                    <div class="overflow-hidden position-relative border-radius-xl"
                        style="background-image: url('https://raw.githubusercontent.com/creativetimofficial/public-assets/master/argon-dashboard-pro/assets/img/card-visa.jpg');">
                        <span class="mask bg-gradient-dark"></span>
                        <div class="card-body position-relative z-index-1 p-3">
                            <i class="fas fa-tower-cell text-white p-2"></i>
                            <h5 class="text-white mt-4 mb-5 pb-2">
                                @if ($detail[0]->piddmt == null)
                                    No Data
                                @else
                                    {{ $detail[0]->piddmt }}
                                @endif
                            </h5>
                            <div class="d-flex">
                                <div class="d-flex">
                                    <div class="me-4">
                                        <p class="text-white text-sm opacity-8 mb-0">Tenant</p>
                                        <h6 class="text-white mb-0">
                                            @if ($detail[0]->tenant == null)
                                                No Data
                                            @else
                                                {{ $detail[0]->tenant }}
                                            @endif
                                        </h6>
                                    </div>
                                    <div>
                                        <p class="text-white text-sm opacity-8 mb-0">Ach Sales</p>
                                        <h6 class="text-white mb-0">
                                            @if ($detail[0]->achsales == null)
                                                No Data
                                            @else
                                                {{ $detail[0]->achsales }}
                                            @endif
                                        </h6>
                                    </div>
                                </div>
                                <div class="ms-auto w-20 d-flex align-items-end justify-content-end">
                                    <img class="w-60 mt-2" src="{{ asset('img/logo-ct.png') }}" alt="logo">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header mx-4 p-3 text-center">
                                <div
                                    class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                                    <i class="fas fa-list opacity-10"></i>
                                </div>
                            </div>
                            <div class="card-body pt-0 p-3 text-center">
                                <h6 class="text-center mb-0">Category</h6>
                                <span class="text-xs">Sales Category</span>
                                <hr class="horizontal dark my-3">
                                <p class="mb-0">
                                    @if ($detail[0]->kategori == null)
                                        No Data
                                    @else
                                        {{ $detail[0]->kategori }}
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mt-md-0 mt-4">
                        <div class="card">
                            <div class="card-header mx-4 p-3 text-center">
                                <div
                                    class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                                    <i class="fas fa-location-dot opacity-10"></i>
                                </div>
                            </div>
                            <div class="card-body pt-0 p-3 text-center">
                                @if ($detail[0]->latitude == null && $detail[0]->longitude == null)
                                    <h6 class="text-center mb-0">Location</h6>
                                    <span class="text-xs">Not Available</span>
                                    <hr class="horizontal dark my-3">
                                    <p>Location Error</p>
                                @else
                                <h6 class="text-center mb-0">Location</h6>
                                <span class="text-xs">Available</span>
                                <hr class="horizontal dark my-3">
                                <a href="https://www.google.com/maps/search/{{ $detail[0]->latitude }},{{ $detail[0]->longitude }}" target="blank"><i class="fas fa-location-arrow opacity-10"></i> Go to Location </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mb-lg-0 mb-4">
                <div class="card mt-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h6 class="mb-0">Site Name</h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-3">
                        <div class="row">
                            <div class="col-md-12 mb-md-0 mb-4">
                                <div
                                    class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">
                                    <img class="w-5 me-3 mb-0" src="{{ asset('img/logo-ct.png') }}" alt="logo">
                                    <h6 class="mb-0">
                                        @if ($detail[0]->sitename == null)
                                            No Data
                                        @else
                                            {{ $detail[0]->sitename }}
                                        @endif
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card h-100">
            <div class="card-header pb-0 p-3">
                <div class="row">
                    <div class="col-6 d-flex align-items-center">
                        <h6 class="mb-0">Sales Data</h6>
                    </div>
                </div>
            </div>
            <div class="card-body p-3 pb-0">
                <ul class="list-group">
                    <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                        <div class="d-flex flex-column">
                            <h6 class="mb-1 text-dark font-weight-bold text-sm">Area Name</h6>
                            <span class="text-xs">
                                @if ($detail[0]->area == null)
                                    No Data
                                @else
                                    {{ $detail[0]->area }}
                                @endif
                            </span>
                        </div>
                        <div class="d-flex flex-column">
                            <h6 class="mb-1 text-dark font-weight-bold text-sm">Scope of Work</h6>
                            <span class="text-xs">
                                @if ($detail[0]->sow == null)
                                    No Data
                                @else
                                    {{ $detail[0]->sow }}
                                @endif
                            </span>
                        </div>
                        <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4"><i class="fas fa-circle-check text-success text-lg me-1"></i></button>
                    </li>
                    <li
                        class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                        <div class="d-flex flex-column">
                            <h6 class="text-dark mb-1 font-weight-bold text-sm">Date of WO</h6>
                            <span class="text-xs">
                                @if ($detail[0]->tanggalwo == null)
                                    No Data
                                @else
                                    {{ \Carbon\Carbon::Parse($detail[0]->tanggalwo)->format('d F Y') }}
                                @endif
                            </span>
                        </div>
                        <div class="d-flex flex-column">
                            <h6 class="text-dark mb-1 font-weight-bold text-sm">Date of RFI</h6>
                            <span class="text-xs">
                                @if ($detail[0]->daterfi == null)
                                    No Data
                                @else
                                {{ \Carbon\Carbon::Parse($detail[0]->daterfi)->format('d F Y') }}
                                @endif
                            </span>
                        </div>
                        <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4"><i class="fas fa-circle-check text-success text-lg me-1"></i></button>
                    </li>
                    <li
                        class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                        <div class="d-flex flex-column">
                            <h6 class="text-dark mb-1 font-weight-bold text-sm">Sales Status</h6>
                            <span class="text-xs">
                                @if ($detail[0]->status == null)
                                    No Data
                                @else
                                    {{ $detail[0]->status }}
                                @endif
                            </span>
                        </div>
                        <div class="d-flex flex-column">
                            <h6 class="text-dark mb-1 font-weight-bold text-sm">Sales Details</h6>
                            <span class="text-xs">
                                @if ($detail[0]->detstatus == null)
                                    No Data
                                @else
                                    {{ $detail[0]->detstatus }}
                                @endif
                            </span>
                        </div>
                        <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4"><i class="fas fa-circle-check text-success text-lg me-1"></i></button>
                    </li>
                    <li
                        class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                        <div class="d-flex flex-column">
                            <h6 class="text-dark mb-1 font-weight-bold text-sm">BAUF Status</h6>
                            <span class="text-xs">
                                @if ($detail[0]->bauf == null)
                                    No Data
                                @else
                                {{ $detail[0]->bauf }}
                                @endif
                            </span>
                        </div>
                        <div class="d-flex flex-column">
                            <h6 class="text-dark font-weight-bold text-sm">BAPS Status</h6>
                            <span class="text-xs">
                                @if ($detail[0]->baps == null)
                                    No Data
                                @else
                                {{ $detail[0]->baps }}
                                @endif
                            </span>
                        </div>
                        <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4"><i class="fas fa-circle-check text-success text-lg me-1"></i></button>
                    </li>
                    <li class="list-group-item border-0 d-flex justify-content-between ps-0 border-radius-lg">
                        <div class="d-flex flex-column">
                            <h6 class="text-dark mb-1 font-weight-bold text-sm">Milestone Data</h6>
                            <span class="text-xs">
                                @if ($detail[0]->milestone == null)
                                    No Data
                                @else
                                    {{ $detail[0]->milestone }}
                                @endif
                            </span>
                        </div>
                        <div class="d-flex flex-column">
                            <h6 class="text-dark font-weight-bold text-sm">PO RFI Status</h6>
                            <span class="text-xs">
                                @if ($detail[0]->cekporfi == null)
                                    No Data
                                @else
                                {{ $detail[0]->cekporfi }}
                                @endif
                            </span>
                        </div>
                        <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4"><i class="fas fa-circle-check text-success text-lg me-1"></i></button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-7 mt-4">
        <div class="card">
            <div class="card-header pb-0 px-3">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="mb-0">Sales Issue</h6>
                    </div>
                    <div class="col-md-6 d-flex justify-content-end align-items-center">
                        <i class="far fa-calendar-alt me-2"></i>
                        <small>{{ \Carbon\Carbon::now()->format('d F Y') }}</small>
                    </div>
                </div>
            </div>
            <div class="card-body pt-4 p-3">
                <ul class="list-group">
                    <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                        <div class="d-flex align-items-center">
                            <button class="btn btn-icon-only btn-rounded btn-outline-dark mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-right"></i></button>
                            <div class="d-flex flex-column">
                                <h6 class="mb-1 text-dark text-sm">General Blocking</h6>
                                <span class="text-xs">
                                    @if ($detail[0]->general == null)
                                        No Data
                                    @else
                                    {{ $detail[0]->general }}
                                    @endif
                                </span>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                        <div class="d-flex align-items-center">
                            <button class="btn btn-icon-only btn-rounded btn-outline-dark mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-right"></i></button>
                            <div class="d-flex flex-column">
                                <h6 class="mb-1 text-dark text-sm">Drop Issue</h6>
                                <span class="text-xs">
                                    @if ($detail[0]->dropisu == null)
                                        No Data
                                    @else
                                    {{ $detail[0]->dropisu }}
                                    @endif
                                </span>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                        <div class="d-flex align-items-center">
                            <button class="btn btn-icon-only btn-rounded btn-outline-dark mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-right"></i></button>
                            <div class="d-flex flex-column">
                                <h6 class="mb-1 text-dark text-sm">Batch Product</h6>
                                <span class="text-xs">
                                    @if ($detail[0]->batch == null)
                                        No Data
                                    @else
                                    {{ $detail[0]->batch }}
                                    @endif
                                </span>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                        <div class="d-flex align-items-center">
                            <button class="btn btn-icon-only btn-rounded btn-outline-dark mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-right"></i></button>
                            <div class="d-flex flex-column">
                                <h6 class="mb-1 text-dark text-sm">Notes</h6>
                                <span class="text-xs">
                                    @if ($detail[0]->notes == null)
                                        No Data
                                    @else
                                    {{ $detail[0]->notes }}
                                    @endif
                                </span>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                        <div class="d-flex align-items-center">
                            <button class="btn btn-icon-only btn-rounded btn-outline-dark mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-right"></i></button>
                            <div class="d-flex flex-column">
                                <h6 class="mb-1 text-dark text-sm">Block OG</h6>
                                <span class="text-xs">
                                    @if ($detail[0]->blockog == null)
                                        No Data
                                    @else
                                    {{ $detail[0]->blockog }}
                                    @endif
                                </span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-5 mt-4">
        <div class="card h-100 mb-4">
            <div class="card-header pb-0 px-3">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="mb-0">Aging Issue</h6>
                    </div>
                    <div class="col-md-6 d-flex justify-content-end align-items-center">
                        <i class="far fa-calendar-alt me-2"></i>
                        <small>{{ \Carbon\Carbon::now()->format('d F Y') }}</small>
                    </div>
                </div>
            </div>
            <div class="card-body pt-4 p-3">
                <ul class="list-group">
                    <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                        <div class="d-flex align-items-center">
                            <button class="btn btn-icon-only btn-rounded btn-outline-dark mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-right"></i></button>
                            <div class="d-flex flex-column">
                                <h6 class="mb-1 text-dark text-sm">Aging RFI</h6>
                                <span class="text-xs">
                                    @if (\Carbon\Carbon::parse($detail[0]->tanggalwo)->diffInDays(\Carbon\Carbon::parse($detail[0]->daterfi)) <= 30)
                                        < 1 Month
                                    @elseif(\Carbon\Carbon::parse($detail[0]->tanggalwo)->diffInDays(\Carbon\Carbon::parse($detail[0]->daterfi)) <= 60 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 89)
                                        > 2 Months
                                    @elseif (\Carbon\Carbon::parse($detail[0]->tanggalwo)->diffInDays(\Carbon\Carbon::parse($detail[0]->daterfi)) <= 90 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 3650)
                                        > 3 Months
                                    @else
                                     No Data
                                    @endif
                                </span>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                        <div class="d-flex align-items-center">
                            <button class="btn btn-icon-only btn-rounded btn-outline-dark mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-right"></i></button>
                            <div class="d-flex flex-column">
                                <h6 class="mb-1 text-dark text-sm">Aging WO to OG</h6>
                                <span class="text-xs">
                                    @if (\Carbon\Carbon::parse($detail[0]->tanggalwo)->diffInDays(\Carbon\Carbon::parse($detail[0]->daterfi)) <= 30)
                                        < 1 Month
                                    @elseif(\Carbon\Carbon::parse($detail[0]->tanggalwo)->diffInDays(\Carbon\Carbon::parse($detail[0]->daterfi)) <= 60 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 89)
                                        > 2 Months
                                    @elseif (\Carbon\Carbon::parse($detail[0]->tanggalwo)->diffInDays(\Carbon\Carbon::parse($detail[0]->daterfi)) <= 90 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 3650)
                                        > 3 Months
                                    @else
                                     No Data
                                    @endif
                                </span>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                        <div class="d-flex align-items-center">
                            <button class="btn btn-icon-only btn-rounded btn-outline-dark mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-arrow-right"></i></button>
                            <div class="d-flex flex-column">
                                <h6 class="mb-1 text-dark text-sm">SLA</h6>
                                <span class="text-xs">
                                    @if ($detail[0]->sow == "Colo" && \Carbon\Carbon::parse($detail[0]->tanggalwo)->diffInDays(\Carbon\Carbon::parse($detail[0]->daterfi)) <= 60)
                                        In SLA
                                    @elseif ($detail[0]->sow == "Reseller" && \Carbon\Carbon::parse($detail[0]->tanggalwo)->diffInDays(\Carbon\Carbon::parse($detail[0]->daterfi)) <= 60)
                                        In SLA
                                    @elseif ($detail[0]->sow == "New Site" && \Carbon\Carbon::parse($detail[0]->tanggalwo)->diffInDays(\Carbon\Carbon::parse($detail[0]->daterfi)) <= 120)
                                        In SLA
                                    @else
                                        Out SLA
                                    @endif
                                </span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection