@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold"> Sales Status & Administration Progress</p>
                            <h5 class="font-weight-bolder">
                                Details of Sales Status & Administration Progress by PID
                            </h5>
                            <p class="mb-0 text-xs">
                                General Blocking, Aging Issue, BAUF, BAPS, Milestone, PO RFI
                            </p>
                            <a class="mb-0 text-primary text-sm font-weight-bolder" href="/multisales/{{ $year }}/level3/{{ $category }}/{{ $segmen }}/{{ $segval }}/status">Click for Details!</a>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div
                            class="icon icon-shape bg-gradient-secondary shadow-secondary text-center rounded-circle">
                            <i class="ni ni-tag text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-12 mb-lg-0 mb-4">
        <div class="card z-index-2 h-100">
            <div class="card-header pb-0 pt-3 bg-transparent">
                <h6 class="text-capitalize">Scope of Work Data list</h6>
            </div>
            <div class="card-body p-3">
                @if ($segmen == 'sow')
                <div class="table-responsive p-0">
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">PID</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Site Name</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">SoW</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Sales Status</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aging RFI</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aging WO to OG</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">SLA</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $d)
                            <tr>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->piddmt }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->sitename }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->sow }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->status }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                    @if (\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 30)
                                        < 1 Month(s)
                                    @elseif(\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 60 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 89)
                                        > 2 Month(s)
                                    @elseif (\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 90 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 3650)
                                        > 3 Month(s)
                                    @else
                                     No Data
                                    @endif</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                    @if (\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 30)
                                        < 1 Month(s)
                                    @elseif(\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 60 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 89)
                                        > 2 Month(s)
                                    @elseif (\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 90 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 3650)
                                        > 3 Month(s)
                                    @else
                                     No Data
                                    @endif</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                    @if ($d->sow == "Colo" && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 60)
                                        In SLA
                                    @elseif ($d->sow == "Reseller" && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 60)
                                    In SLA
                                    @elseif ($d->sow == "New Site" && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 120)
                                    In SLA
                                    @else
                                    Out SLA
                                    @endif</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @elseif($segmen == 'tenant')
                <div class="table-responsive p-0">
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">PID</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Site Name</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tenant</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Sales Status</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aging RFI</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aging WO to OG</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">SLA</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $d)
                            <tr>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->piddmt }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->sitename }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->tenant }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->status }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                    @if (\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 30)
                                        < 1 Month(s)
                                    @elseif(\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 60 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 89)
                                        > 2 Month(s)
                                    @elseif (\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 90 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 3650)
                                        > 3 Month(s)
                                    @else
                                     No Data
                                    @endif</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                    @if (\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 30)
                                        < 1 Month(s)
                                    @elseif(\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 60 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 89)
                                        > 2 Month(s)
                                    @elseif (\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 90 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 3650)
                                        > 3 Month(s)
                                    @else
                                     No Data
                                    @endif</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                    @if ($d->sow == "Colo" && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 60)
                                        In SLA
                                    @elseif ($d->sow == "Reseller" && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 60)
                                    In SLA
                                    @elseif ($d->sow == "New Site" && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 120)
                                    In SLA
                                    @else
                                    Out SLA
                                    @endif</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @elseif($segmen == 'area')
                <div class="table-responsive p-0">
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">PID</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Site Name</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Area</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Sales Status</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aging RFI</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aging WO to OG</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">SLA</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $d)
                            <tr>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->piddmt }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->sitename }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->area }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->status }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                    @if (\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 30)
                                        < 1 Month(s)
                                    @elseif(\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 60 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 89)
                                        > 2 Month(s)
                                    @elseif (\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 90 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 3650)
                                        > 3 Month(s)
                                    @else
                                     No Data
                                    @endif</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                    @if (\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 30)
                                        < 1 Month(s)
                                    @elseif(\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 60 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 89)
                                        > 2 Month(s)
                                    @elseif (\Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 90 && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 3650)
                                        > 3 Month(s)
                                    @else
                                     No Data
                                    @endif</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                    @if ($d->sow == "Colo" && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 60)
                                        In SLA
                                    @elseif ($d->sow == "Reseller" && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 60)
                                    In SLA
                                    @elseif ($d->sow == "New Site" && \Carbon\Carbon::parse($d->tanggalwo)->diffInDays(\Carbon\Carbon::parse($d->daterfi)) <= 120)
                                    In SLA
                                    @else
                                    Out SLA
                                    @endif</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
                
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function () {
    // Setup - add a text input to each footer cell
    $('#example thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#example thead');
 
    var table = $('#example').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        initComplete: function () {
            var api = this.api();
 
            // For each column
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    $(cell).html('<input type="text" placeholder="' + title + '" />');
 
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('keyup change', function (e) {
                            e.stopPropagation();
 
                            // Get the search value
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();
 
                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();
 
                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });
});
</script>
@endpush