@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12 mb-lg-0 mb-4">
        <div class="card z-index-2 h-100">
            <div class="card-header pb-0 pt-3 bg-transparent">
                <h6 class="text-capitalize">{{ $segval }} Data of {{ $segmen }} List</h6>
            </div>
            <div class="card-body p-3">
                <div class="table-responsive p-0">
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">PID</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">{{ $segmen }}</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Site Name</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Ach Sales</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($hasil as $h)
                            <tr>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $h->piddmt }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $h->$segmen }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $h->sitename }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $h->achsales }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                    <a href="/mainsales/2022/detail/{{ $h->piddmt }}" class="btn btn-icon btn-2 btn-success btn-link text-success text-gradient px-3 mb-0" data-bs-toggle="tooltip" data-bs-placement="top" title="See Details"><span class="btn-inner--icon"><i class="fas fa-info"></i></span></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <script>
                Swal.fire(
            'Error!',
            '{{ $error }}',
            'error'
            )
            </script>
        @endforeach
    @endif
    @if ($message = Session::get('success'))
        <script>
        Swal.fire(
        'Good job!',
        '{{ $message }}',
        'success'
        )
        </script>
    @endif
    <script>
        function edart(url){
            console.log(url);
            Swal.fire({
            title: 'Are you sure?',
            text: "Do you want to see "+url+"'s data?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, i do'
            }).then((result) => {
            if (result.value) {
                Swal.fire(
                'Okay!',
                'You have seen '+url+"'s record",
                'success',
                window.location = 'main/detail/'+url
                )
            }
            })
        };
    </script>
    <script>
        $(document).ready(function () {
        // Setup - add a text input to each footer cell
        $('#example thead tr')
            .clone(true)
            .addClass('filters')
            .appendTo('#example thead');
    
        var table = $('#example').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            initComplete: function () {
                var api = this.api();
    
                // For each column
                api
                    .columns()
                    .eq(0)
                    .each(function (colIdx) {
                        // Set the header cell to contain the input element
                        var cell = $('.filters th').eq(
                            $(api.column(colIdx).header()).index()
                        );
                        var title = $(cell).text();
                        $(cell).html('<input type="text" placeholder="' + title + '" />');
    
                        // On every keypress in this input
                        $(
                            'input',
                            $('.filters th').eq($(api.column(colIdx).header()).index())
                        )
                            .off('keyup change')
                            .on('keyup change', function (e) {
                                e.stopPropagation();
    
                                // Get the search value
                                $(this).attr('title', $(this).val());
                                var regexr = '({search})'; //$(this).parents('th').find('select').val();
    
                                var cursorPosition = this.selectionStart;
                                // Search the column for that value
                                api
                                    .column(colIdx)
                                    .search(
                                        this.value != ''
                                            ? regexr.replace('{search}', '(((' + this.value + ')))')
                                            : '',
                                        this.value != '',
                                        this.value == ''
                                    )
                                    .draw();
    
                                $(this)
                                    .focus()[0]
                                    .setSelectionRange(cursorPosition, cursorPosition);
                            });
                    });
            },
        });
    });
    </script>
@endpush