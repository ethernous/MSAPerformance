@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold"> Scope of Work</p>
                            <h5 class="font-weight-bolder">
                                {{ $sow }}
                            </h5>
                            <p class="mb-0 text-xs">
                                Current number of scope of work
                            </p>
                            <a class="mb-0 text-primary text-sm font-weight-bolder" href="#" data-bs-toggle="modal" data-bs-target="#sowModal">Click for Details!</a>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div
                            class="icon icon-shape bg-gradient-primary shadow-primary text-center rounded-circle">
                            <i class="ni ni-building text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Tenants</p>
                            <h5 class="font-weight-bolder">
                                {{ $tenant }}
                            </h5>
                            <p class="mb-0 text-xs">
                                Current number of tenants
                            </p>
                            <a class="mb-0 text-primary text-sm font-weight-bolder" href="#" data-bs-toggle="modal" data-bs-target="#tenantModal">Click for Details!</a>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div
                            class="icon icon-shape bg-gradient-danger shadow-danger text-center rounded-circle">
                            <i class="ni ni-books text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Area</p>
                            <h5 class="font-weight-bolder">
                                {{ $area }}
                            </h5>
                            <p class="mb-0 text-xs">
                                Current number of area
                            </p>
                            <a class="mb-0 text-primary text-sm font-weight-bolder" href="#" data-bs-toggle="modal" data-bs-target="#areaModal">Click for Details!</a>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div
                            class="icon icon-shape bg-gradient-success shadow-success text-center rounded-circle">
                            <i class="ni ni-square-pin text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-12 mb-lg-0 mb-4">
        <div class="card z-index-2 h-100">
            <div class="card-header pb-0 pt-3 bg-transparent">
                <h6 class="text-capitalize">Level 2 Data list</h6>
            </div>
            <div class="card-body p-3">
                <div class="table-responsive p-0">
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">PID</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Site Name</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tenant</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Area</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Scope of Work</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $d)
                            <tr>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->piddmt }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->sitename }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->tenant }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->area }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->sow }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="sowModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Select a Scope of Work you want to see!</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                @foreach ($datasow as $item)
                <div class="col-xl-4">
                    <a class="btn btn-icon btn-3 btn-success" type="button" href="/multisales/{{ $year }}/level2/{{ $category }}/sow/{{ $item->jenissow }}">
                        <span class="btn-inner--text">{{ $item->jenissow }}</span><hr>
                        <span class="btn-inner--icon"><i class="ni ni-curved-next"></i></span>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="tenantModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Select a Tenant you want to see!</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                @foreach ($datatenant as $item)
                <div class="col-xl-4">
                    <a class="btn btn-icon btn-3 btn-success" type="button" href="/multisales/{{ $year }}/level2/{{ $category }}/tenant/{{ $item->namatenant }}">
                        <span class="btn-inner--text">{{ $item->deskripsi }}</span><hr>
                        <span class="btn-inner--icon"><i class="ni ni-curved-next"></i></span>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="areaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Select an Area you want to see!</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                @foreach ($dataarea as $item)
                <div class="col-xl-3">
                    <a class="btn btn-icon btn-3 btn-success" type="button" href="/multisales/{{ $year }}/level2/{{ $category }}/area/{{ $item->namaarea }}">
                        <span class="btn-inner--text">{{ $item->namaarea }}</span><hr>
                        <span class="btn-inner--icon"><i class="ni ni-curved-next"></i></span>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function () {
    // Setup - add a text input to each footer cell
    $('#example thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#example thead');
 
    var table = $('#example').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        initComplete: function () {
            var api = this.api();
 
            // For each column
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    $(cell).html('<input type="text" placeholder="' + title + '" />');
 
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('keyup change', function (e) {
                            e.stopPropagation();
 
                            // Get the search value
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();
 
                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();
 
                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });
});
</script>
@endpush