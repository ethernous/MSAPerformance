@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12 mb-lg-0 mb-4">
        <div class="card z-index-2 h-100">
            <div class="card-header pb-0 pt-3 bg-transparent">
                <h6 class="text-capitalize">Data list</h6>
            </div>
            <div class="card-body p-3">
                <div class="table-responsive p-0">
                    <table id="example" class="table" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">PID</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Site Name</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Sales Status</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">BAUF</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">BAPS</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Milestone</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Cek PO RFI</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $d)
                            <tr>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->piddmt }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->sitename }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->status }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                @if ($d->bauf == null)
                                    No Data
                                @else
                                    {{ $d->bauf }}
                                @endif
                                </td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                @if ($d->baps == null)
                                    No Data
                                @else
                                    {{ $d->baps }}
                                @endif    
                                </td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->milestone }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->cekporfi }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">
                                    <a href="/mainsales/{{ $year }}/detail/{{ $d->piddmt }}" class="btn btn-icon btn-sm btn-success"><span class="btn-inner--icon"><i class="ni ni-folder-17"></i></span> | See Details</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function () {
    // Setup - add a text input to each footer cell
    $('#example thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#example thead');
 
    var table = $('#example').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        initComplete: function () {
            var api = this.api();
 
            // For each column
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    $(cell).html('<input type="text" placeholder="' + title + '" />');
 
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('keyup change', function (e) {
                            e.stopPropagation();
 
                            // Get the search value
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();
 
                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();
 
                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });
});
</script>
@endpush