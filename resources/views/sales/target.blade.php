@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12 mb-lg-0 mb-4">
        <div class="card z-index-2 h-100">
            <div class="card-header pb-0 pt-3 bg-transparent">
                <h6 class="text-capitalize">Sales Target of {{ $year }} list</h6>
            </div>
            <div class="card-body p-3">
                <div class="table-responsive p-0">
                    <table class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">SoW</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Jan</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Feb</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Mar</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Apr</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">May</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Jun</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Jul</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aug</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Sep</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Oct</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nov</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Des</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $d)
                            <tr>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->sow }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->jan }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->feb }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->mar }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->apr }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->may }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->jun }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->jul }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->aug }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->sep }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->oct }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->nov }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->des }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection