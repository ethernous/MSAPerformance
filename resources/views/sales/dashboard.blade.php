@extends('layouts.app')

@section('content')
<div class="row  mt-n2">
    <div class="col-xl-8 col-lg-7">
        <div class="row">
            <div class="col-sm-3">
                <div class="card overflow-hidden">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-tower-cell opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">Sales Achievement</h6>
                        <span class="text-xs">YTD {{ \Carbon\Carbon::now()->format('M Y') }}</span>
                        <hr class="horizontal dark my-3">
                        <h5 class="mb-0">{{ $sumbef }}</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 mt-sm-0 mt-4">
                <div class="card overflow-hidden">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-tower-cell opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">Sales Achievement</h6>
                        <span class="text-xs">YTD {{ \Carbon\Carbon::now()->subWeeks(52)->format('M Y') }}</span>
                        <hr class="horizontal dark my-3">
                        <h5 class="mb-0">{{ $sumaft }}</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 mt-sm-0 mt-4">
                <div class="card overflow-hidden">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-bullseye opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">Sales Target</h6>
                        <span class="text-xs">YTD {{ \Carbon\Carbon::now()->format('M Y') }}</span>
                        <hr class="horizontal dark my-3">
                        <h5 class="mb-0">{{ $target }}</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 mt-sm-0 mt-4">
                <div class="card overflow-hidden">
                    <div class="card-header mx-4 p-3 text-center">
                        <div class="icon icon-shape icon-lg bg-gradient-danger shadow text-center border-radius-lg">
                            <i class="fas fa-arrow-up-right-dots opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="card-body pt-0 p-3 text-center">
                        <h6 class="text-center mb-0">Sales Growth</h6>
                        <span class="text-xs">YTD {{ \Carbon\Carbon::now()->format('M Y') }}</span>
                        <hr class="horizontal dark my-3">
                        <a href="/saleresult?year={{ \Carbon\Carbon::now()->format('Y') }}&month={{ \Carbon\Carbon::now()->format('M') }}&week=no"><h5 class="mb-0">{{ number_format((float)(($sumbef/$sumaft)-1)*100, 2, '.', '') }}%</h5></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-5 mt-lg-0 mt-4">
        <div class="row">
            <div class="col-lg-12">
                
                    <div class="overflow-hidden position-relative border-radius-lg bg-cover h-100"
                        style="background-image: url(https://www.mitratel.co.id/wp-content/uploads/2022/03/Solusi-Kami-2c-1-1024x811.jpg);">
                        <span class="mask bg-gradient-dark"></span>
                        <div class="card-body position-relative z-index-1 h-100 p-3">
                            <h6 class="text-white font-weight-bolder mb-3">Hey {{ Auth::User()->name }}!</h6>
                            <p class="text-white mb-3">Welcome to Sales Dashboard, since you are a/an {{ Auth::User()->jabatan }}, you gained access to {{ Auth::User()->jobdesc }} pages. Feel free to contact the developer if any error occurs!</p>
                            <hr>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-2 col-sm-6">
        <div class="card bg-gradient-danger h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Sales Category</p>
                </div>
                <i class="fa-solid fa-box-archive fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Carry Over</p>
                <span class="text-xs text-white">{{ $co }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-danger h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Sales Category</p>
                </div>
                <i class="fa-solid fa-box-archive fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">New Sales</p>
                <span class="text-xs text-white">{{ $ns }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Sales Tenant</p>
                </div>
                <i class="fa-solid fa-tower-cell fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Telkomsel</p>
                <span class="text-xs text-white">{{ $tsel }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Sales Tenant</p>
                </div>
                <i class="fa-solid fa-tower-cell fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Telkom</p>
                <span class="text-xs text-white">{{ $tlkm }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Sales Tenant</p>
                </div>
                <i class="fa-solid fa-tower-cell fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">XL</p>
                <span class="text-xs text-white">{{ $xl }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Sales Tenant</p>
                </div>
                <i class="fa-solid fa-tower-cell fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Indosat</p>
                <span class="text-xs text-white">{{ $isat }} PID</span>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-2 col-sm-6">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Sales Tenant</p>
                </div>
                <i class="fa-solid fa-tower-cell fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">H3I</p>
                <span class="text-xs text-white">{{ $h3i }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Sales Tenant</p>
                </div>
                <i class="fa-solid fa-tower-cell fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Smartfren</p>
                <span class="text-xs text-white">{{ $smart }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-warning h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Sales Tenant</p>
                </div>
                <i class="fa-solid fa-tower-cell fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Others</p>
                <span class="text-xs text-white">{{ $others }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-danger h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Sales SoW</p>
                </div>
                <i class="fa-solid fa-file-lines fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">B2S</p>
                <span class="text-xs text-white">{{ $b2s }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-danger h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Sales SoW</p>
                </div>
                <i class="fa-solid fa-file-lines fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Colo</p>
                <span class="text-xs text-white">{{ $colo }} PID</span>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-6 mt-lg-0 mt-4">
        <div class="card bg-gradient-danger h-100">
            <div class="card-body">
                <div class="d-flex mb-4">
                    <p class="mb-0 text-white">Sales SoW</p>
                </div>
                <i class="fa-solid fa-file-lines fa-2xl text-white"></i>
                <p class="mt-4 mb-0 font-weight-bold text-white">Reseller</p>
                <span class="text-xs text-white">{{ $rese }} PID</span>
            </div>
        </div>
    </div>
</div>
@endsection
