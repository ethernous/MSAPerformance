@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-xl-6 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold"> Carry Over</p>
                            <h5 class="font-weight-bolder">
                                {{ $carry }}
                            </h5>
                            <p class="mb-0 text-xs">
                                Current number of carry over sales
                            </p>
                            <a class="mb-0 text-primary text-sm font-weight-bolder" href="/multisales/{{ $year }}/level1/Carry Over">Click for Details!</a>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div
                            class="icon icon-shape bg-gradient-primary shadow-primary text-center rounded-circle">
                            <i class="ni ni-building text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">New Sales</p>
                            <h5 class="font-weight-bolder">
                                {{ $new }}
                            </h5>
                            <p class="mb-0 text-xs">
                                Current number of new sales
                            </p>
                            <a class="mb-0 text-primary text-sm font-weight-bolder" href="/multisales/{{$year}}/level1/New Sales">Click for Details!</a>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div
                            class="icon icon-shape bg-gradient-success shadow-success text-center rounded-circle">
                            <i class="ni ni-money-coins text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-lg-12 mb-lg-0 mb-4">
        <div class="card z-index-2 h-100">
            <div class="card-header pb-0 pt-3 bg-transparent">
                <h6 class="text-capitalize">Main Data list</h6>
            </div>
            <div class="card-body p-3">
                <div class="table-responsive p-0">
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead class="thead-light">
                            <tr>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">PID</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Category</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tenant</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Site Name</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Ach Sales</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $d)
                            <tr>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->piddmt }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->kategori }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->tenant }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->sitename }}</td>
                                <td class="text-center text-uppercase text-dark text-xxs font-weight-bolder">{{ $d->achsales }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function () {
        // Setup - add a text input to each footer cell
        $('#example thead tr')
            .clone(true)
            .addClass('filters')
            .appendTo('#example thead');

        var table = $('#example').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            initComplete: function () {
                var api = this.api();

                // For each column
                api
                    .columns()
                    .eq(0)
                    .each(function (colIdx) {
                        // Set the header cell to contain the input element
                        var cell = $('.filters th').eq(
                            $(api.column(colIdx).header()).index()
                        );
                        var title = $(cell).text();
                        $(cell).html('<input type="text" placeholder="' + title + '" />');

                        // On every keypress in this input
                        $(
                                'input',
                                $('.filters th').eq($(api.column(colIdx).header()).index())
                            )
                            .off('keyup change')
                            .on('keyup change', function (e) {
                                e.stopPropagation();

                                // Get the search value
                                $(this).attr('title', $(this).val());
                                var regexr = '({search})'; //$(this).parents('th').find('select').val();

                                var cursorPosition = this.selectionStart;
                                // Search the column for that value
                                api
                                    .column(colIdx)
                                    .search(
                                        this.value != '' ?
                                        regexr.replace('{search}', '(((' + this.value + ')))') :
                                        '',
                                        this.value != '',
                                        this.value == ''
                                    )
                                    .draw();

                                $(this)
                                    .focus()[0]
                                    .setSelectionRange(cursorPosition, cursorPosition);
                            });
                    });
            },
        });
    });
</script>
@endpush